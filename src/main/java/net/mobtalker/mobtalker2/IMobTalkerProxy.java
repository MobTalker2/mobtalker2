/*
 * SPDX-FileCopyrightText: 2013-2019 Chimaine, MobTalker2 contributors
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */
package net.mobtalker.mobtalker2;

import net.minecraftforge.fml.common.event.*;
import net.minecraftforge.fml.common.network.IGuiHandler;

public interface IMobTalkerProxy extends IGuiHandler
{
    void onPreInit( FMLPreInitializationEvent event );
    
    void onInit( FMLInitializationEvent event );
    
    void onPostInit( FMLPostInitializationEvent event );
    
    // ========================================
    
    void onServerAboutToStart( FMLServerAboutToStartEvent event );
    
    void onServerStarting( FMLServerStartingEvent event );
    
    void onServerStarted( FMLServerStartedEvent event );
    
    void onServerStopping( FMLServerStoppingEvent event );
    
    void onServerStopped( FMLServerStoppedEvent event );
    
    // ========================================
    
    void onFingerprintViolation( FMLFingerprintViolationEvent event );
}
