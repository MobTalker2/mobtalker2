/*
 * SPDX-FileCopyrightText: 2013-2019 Chimaine, MobTalker2 contributors
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */
package net.mobtalker.mobtalker2.util.fp;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

import java.util.NoSuchElementException;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.function.Supplier;

import static org.apache.commons.lang3.Validate.notNull;

public class Option<T>
{
    private final T _value;
    
    private static Option<?> EMPTY;
    
    // ========================================
    
    // private to prevent inheritance outside this file
    private Option( T value )
    {
        _value = value;
    }
    
    // ========================================
    
    public boolean isPresent()
    {
        return true;
    }
    
    // ========================================
    
    public T get()
    {
        return getOrThrow();
    }
    
    public T getOrThrow()
    {
        return getOrThrow( "Option is empty" );
    }
    
    public T getOrThrow( String errorMessage )
    {
        return getOrThrow( () -> errorMessage );
    }
    
    public T getOrThrow( Supplier<String> errorMessage )
    {
        notNull( errorMessage );
        if ( !isPresent() )
        {
            throw new NoSuchElementException( errorMessage.get() );
        }
        return _value;
    }
    
    // ========================================
    
    public T getOrElse( T fallback )
    {
        return getOrElse( () -> fallback );
    }
    
    public T getOrElse( Supplier<? extends T> fallback )
    {
        notNull( fallback );
        if ( !isPresent() )
        {
            return fallback.get();
        }
        return _value;
    }
    
    // ========================================
    
    public <R> Option<R> map( Function<? super T, ? extends R> mapping )
    {
        notNull( mapping );
        if ( !isPresent() )
        {
            return Option.empty();
        }
        return Option.of( mapping.apply( get() ) );
    }
    
    public <R> Option<R> flatMap( Function<? super T, Option<R>> mapping )
    {
        notNull( mapping );
        if ( !isPresent() )
        {
            return Option.empty();
        }
        return notNull( mapping.apply( get() ) );
    }
    
    public boolean exists( Predicate<? super T> condition )
    {
        notNull( condition );
        return isPresent() && condition.test( get() );
    }
    
    public Option<T> filter( Predicate<? super T> condition )
    {
        notNull( condition );
        if ( !isPresent() || condition.test( get() ) )
        {
            return this;
        }
        return Option.empty();
    }
    
    public void ifPresent( Consumer<? super T> action )
    {
        notNull( action );
        if ( isPresent() )
        {
            action.accept( get() );
        }
    }
    
    // ========================================
    
    public <L> Either<L, T> toEither( L fallback )
    {
        return toEither( () -> fallback );
    }
    
    public <L> Either<L, T> toEither( Supplier<L> fallback )
    {
        notNull( fallback );
        if ( !isPresent() )
        {
            return Either.left( fallback.get() );
        }
        return Either.right( get() );
    }
    
    // ========================================
    
    @Override
    public String toString()
    {
        return new ToStringBuilder( this )
                       .append( "_value", _value )
                       .toString();
    }
    
    @Override
    public boolean equals( Object o )
    {
        if ( this == o )
        {
            return true;
        }
        if ( o == null )
        {
            return false;
        }
        if ( !o.getClass().equals( getClass() ) )
        {
            return false;
        }
        
        Option<?> other = (Option<?>) o;
        return new EqualsBuilder()
                       .append( _value, other._value )
                       .isEquals();
    }
    
    @Override
    public int hashCode()
    {
        return new HashCodeBuilder()
                       .append( getClass() )
                       .append( _value )
                       .toHashCode();
    }
    
    // ========================================
    
    public static <T> Option<T> of( T value )
    {
        return new Option<>( value );
    }
    
    public static <T> Option<T> empty()
    {
        if ( EMPTY == null )
        {
            EMPTY = new EmptyOption<>();
        }
        
        @SuppressWarnings( "unchecked" )
        Option<T> o = (Option<T>) EMPTY;
        return o;
    }
    
    // ========================================
    
    private static final class EmptyOption<T> extends Option<T>
    {
        private EmptyOption()
        {
            super( null );
        }
        
        // ========================================
        
        @Override
        public boolean isPresent()
        {
            return false;
        }
    }
}
