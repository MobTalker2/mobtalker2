/*
 * SPDX-FileCopyrightText: 2013-2019 Chimaine, MobTalker2 contributors
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */
package net.mobtalker.mobtalker2.util.json;

public class MissingMemberException extends RuntimeException
{
    private final String _name;
    
    public MissingMemberException( String name )
    {
        super( "Missing member: " + name );
        _name = name;
    }
    
    public String getName()
    {
        return _name;
    }
}
