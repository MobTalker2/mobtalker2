/*
 * SPDX-FileCopyrightText: 2013-2019 Chimaine, MobTalker2 contributors
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */
package net.mobtalker.mobtalker2.util;

import net.minecraft.command.ICommandManager;
import net.minecraft.server.MinecraftServer;

// Created on Apr 4, 2016 4:12:51 PM
public final class CommandUtil
{
    private CommandUtil()
    {}
    
    /**
     * @return the current server's command manager
     */
    public static ICommandManager getServerCommandManager()
    {
        return MinecraftServer.getServer().getCommandManager();
    }
    
    /**
     * Checks if a given command exists in the server's command manager.
     * 
     * @param command command to check
     * @return {@code true} if the command exists in the server's command manager, {@code false} otherwise
     * @see #getServerCommandManager()
     * @see #commandExists(ICommandManager, String)
     */
    public static boolean commandExists( String command )
    {
        return commandExists( getServerCommandManager(), command );
    }
    
    /**
     * Checks if a given command exists in the given command manager.
     * 
     * @param manager command manager which should be used
     * @param command command to check
     * @return {@code true} if the command exists in the given command manager, {@code false} otherwise
     * @see #commandExists(String)
     */
    public static boolean commandExists( ICommandManager manager, String command )
    {
        return manager.getCommands().containsKey( command );
    }
}
