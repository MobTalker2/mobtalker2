/*
 * SPDX-FileCopyrightText: 2013-2019 Chimaine, MobTalker2 contributors
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */
package net.mobtalker.mobtalker2.util;

import static org.apache.commons.lang3.Validate.notNull;

import net.minecraft.scoreboard.ScorePlayerTeam;

@Deprecated
public class ScoreboardTeamInfo
{
    public final String Name;
    public final String Color;
    public final boolean FriendlyFire;
    public final boolean CanSeeInvisibleMembers;
    
    // ========================================
    
    public ScoreboardTeamInfo( String name, String color, boolean friendlyFire, boolean canSeeInvisibleMembers )
    {
        Name = name;
        Color = color;
        FriendlyFire = friendlyFire;
        CanSeeInvisibleMembers = canSeeInvisibleMembers;
    }
    
    public ScoreboardTeamInfo( ScorePlayerTeam team )
    {
        notNull( team );
        Name = team.getRegisteredName();
        Color = ChatFormattingUtil.fromPrefix( team.getColorPrefix() ).getFriendlyName();
        FriendlyFire = team.getAllowFriendlyFire();
        CanSeeInvisibleMembers = team.getSeeFriendlyInvisiblesEnabled();
    }
}
