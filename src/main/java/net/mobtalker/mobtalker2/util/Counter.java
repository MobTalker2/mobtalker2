/*
 * SPDX-FileCopyrightText: 2013-2019 Chimaine, MobTalker2 contributors
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */
package net.mobtalker.mobtalker2.util;

/**
 * Class which wraps an int in order to allow counting across multiple methods.
 */
public final class Counter implements Comparable<Counter>
{
    private int count;

    // ========================================

    public Counter()
    {
        this( 0 );
    }

    public Counter( int startCount )
    {
        count = startCount;
    }

    // ========================================

    /**
     * @return current value of this counter
     */
    public int value()
    {
        return count;
    }

    // ========================================

    /**
     * Increments the counter by 1 and returns the result.
     *
     * @see #preIncrement()
     */
    public int increment()
    {
        return preIncrement();
    }

    /**
     * Increments the counter by 1 and returns the result.
     *
     * @see #increment()
     */
    public int preIncrement()
    {
        return ++count;
    }

    /**
     * Returns the counter's value and increments it by 1.
     */
    public int postIncrement()
    {
        return count++;
    }

    // ========================================

    /**
     * Decrements the counter by 1 and returns the result.
     *
     * @see #preDecrement()
     */
    public int decrement()
    {
        return preDecrement();
    }

    /**
     * Decrements the counter by 1 and returns the result.
     *
     * @see #decrement()
     */
    public int preDecrement()
    {
        return --count;
    }

    /**
     * Returns the counter's value and decrements it by 1.
     */
    public int postDecrement()
    {
        return count--;
    }

    // ========================================

    @Override
    @SuppressWarnings( "NullableProblems" )
    public int compareTo( Counter other )
    {
        if ( other.value() < value() ) return 1;
        if ( other.value() == value() ) return 0;
        return -1;
    }

    @Override
    public boolean equals( Object o )
    {
        if ( o == null ) return false;
        if ( o == this ) return true;
        if ( !( o.getClass() == getClass() ) ) return false;
        Counter c = (Counter) o;
        return compareTo( c ) == 0;
    }

    @Override
    public int hashCode()
    {
        return Integer.hashCode( value() );
    }

    @Override
    public String toString()
    {
        return String.format( "%s[%d]", getClass().getName(), value() );
    }
}
