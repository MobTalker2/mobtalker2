/*
 * SPDX-FileCopyrightText: 2013-2019 Chimaine, MobTalker2 contributors
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */
package net.mobtalker.mobtalker2.util;

import java.io.InputStream;

import net.mobtalker.mobtalker2.MobTalker2;

public class AssetUtil
{
    public static InputStream getAsset( String path )
    {
        return MobTalker2.class.getResourceAsStream( "/assets/" + MobTalker2.DOMAIN + "/" + path );
    }
}
