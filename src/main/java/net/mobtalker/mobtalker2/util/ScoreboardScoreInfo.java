/*
 * SPDX-FileCopyrightText: 2013-2019 Chimaine, MobTalker2 contributors
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */
package net.mobtalker.mobtalker2.util;

import static org.apache.commons.lang3.Validate.notNull;

@Deprecated
public class ScoreboardScoreInfo
{
    public final String Player;
    public final ScoreboardObjectiveInfo Objective;
    public final int Score;
    
    // ========================================
    
    public ScoreboardScoreInfo( String player, ScoreboardObjectiveInfo objective, int score )
    {
        Player = player;
        Objective = objective;
        Score = score;
    }
    
    public ScoreboardScoreInfo( net.minecraft.scoreboard.Score score )
    {
        notNull( score );
        Player = score.getPlayerName();
        Objective = new ScoreboardObjectiveInfo( score.getObjective() );
        Score = score.getScorePoints();
    }
}
