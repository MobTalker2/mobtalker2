/*
 * SPDX-FileCopyrightText: 2013-2019 Chimaine, MobTalker2 contributors
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */
package net.mobtalker.mobtalker2.util;

import net.minecraft.entity.EntityLivingBase;
import net.minecraft.util.EntityDamageSource;

public class MobTalkerDamageSource extends EntityDamageSource
{
    /**
     * @param name <code>mob</code> or <code>player</code>
     */
    public MobTalkerDamageSource( EntityLivingBase source, String name, boolean isMagic, boolean bypassArmor )
    {
        super( name, source );
        
        if ( bypassArmor )
            setDamageBypassesArmor();
        if ( isMagic )
            setMagicDamage();
    }
    
//    @Override
//    public ChatMessageComponent getDeathMessage( EntityLivingBase entity )
//    {
//        String msg = "death.attack." + this.damageType;
//
//        String entityName = entity.getTranslatedEntityName();
//        String sourceName = _sourceEntity.getTranslatedEntityName();
//
//        return ChatMessageComponent.createFromTranslationWithSubstitutions( msg, new Object[] { entityName, sourceName } );
//    }
}
