/*
 * SPDX-FileCopyrightText: 2013-2019 Chimaine, MobTalker2 contributors
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */
package net.mobtalker.mobtalker2.util.logging;

import java.util.Objects;

import net.minecraftforge.fml.common.FMLLog;
import net.mobtalker.mobtalker2.*;
import net.mobtalker.mobtalker2.common.MobTalkerConfig;

import org.apache.logging.log4j.Level;

import com.google.common.base.Strings;

public class MobTalkerLog
{
    private final String _name;
    
    // ========================================
    
    public MobTalkerLog()
    {
        this( null );
    }
    
    public MobTalkerLog( String tag )
    {
        if ( Strings.isNullOrEmpty( tag ) )
        {
            _name = MobTalker2.NAME;
        }
        else
        {
            _name = MobTalker2.NAME + "." + tag;
        }
    }
    
    // ========================================
    
    public void log( Level level, Object obj )
    {
        FMLLog.log( _name, level, Objects.toString( obj ) );
    }
    
    public void log( Level level, String msg, Object... data )
    {
        FMLLog.log( _name, level, msg, data );
    }
    
    public void log( Level level, Throwable ex, Object obj )
    {
        FMLLog.log( _name, level, ex, Objects.toString( obj ) );
    }
    
    public void log( Level level, Throwable ex, String msg, Object... data )
    {
        FMLLog.log( _name, level, ex, msg, data );
    }
    
    // ========================================
    
    public void bigWarning( Object obj )
    {
        FMLLog.bigWarning( Objects.toString( obj ) );
    }
    
    public void bigWarning( String msg, Object... data )
    {
        FMLLog.bigWarning( msg, data );
    }
    
    // ========================================
    
    public void fatal( Object obj )
    {
        log( Level.FATAL, obj );
    }
    
    public void fatal( String msg, Object... data )
    {
        log( Level.FATAL, msg, data );
    }
    
    public void fatal( Throwable ex, Object obj )
    {
        log( Level.FATAL, ex, obj );
    }
    
    public void fatal( Throwable ex, String msg, Object... data )
    {
        log( Level.FATAL, ex, msg, data );
    }
    
    public void error( Object obj )
    {
        log( Level.ERROR, obj );
    }
    
    public void error( String msg, Object... data )
    {
        log( Level.ERROR, msg, data );
    }
    
    public void error( Throwable ex, Object obj )
    {
        log( Level.ERROR, ex, obj );
    }
    
    public void error( Throwable ex, String msg, Object... data )
    {
        log( Level.ERROR, ex, msg, data );
    }
    
    public void warn( Object obj )
    {
        log( Level.WARN, obj );
    }
    
    public void warn( String msg, Object... data )
    {
        log( Level.WARN, msg, data );
    }
    
    public void warn( Throwable ex, Object obj )
    {
        log( Level.WARN, ex, obj );
    }
    
    public void warn( Throwable ex, String msg, Object... data )
    {
        log( Level.WARN, ex, msg, data );
    }
    
    public void info( Object obj )
    {
        log( Level.INFO, obj );
    }
    
    public void info( String msg, Object... data )
    {
        log( Level.INFO, msg, data );
    }
    
    public void info( Throwable ex, Object obj )
    {
        log( Level.INFO, ex, obj );
    }
    
    public void info( Throwable ex, String msg, Object... data )
    {
        log( Level.INFO, ex, msg, data );
    }
    
    public void debug( Object obj )
    {
        log( MobTalkerConfig.isDebugEnabled() ? Level.INFO : Level.DEBUG, obj );
    }
    
    public void debug( String msg, Object... data )
    {
        log( MobTalkerConfig.isDebugEnabled() ? Level.INFO : Level.DEBUG, msg, data );
    }
    
    public void debug( Throwable ex, Object obj )
    {
        log( MobTalkerConfig.isDebugEnabled() ? Level.INFO : Level.DEBUG, ex, obj );
    }
    
    public void debug( Throwable ex, String msg, Object... data )
    {
        log( MobTalkerConfig.isDebugEnabled() ? Level.INFO : Level.DEBUG, ex, msg, data );
    }
    
    public void trace( Object obj )
    {
        if ( !MobTalkerConfig.isDebugEnabled() )
            return;
        
        log( Level.TRACE, obj );
    }
    
    public void trace( String msg, Object... data )
    {
        if ( !MobTalkerConfig.isDebugEnabled() )
            return;
        
        log( Level.TRACE, msg, data );
    }
    
    public void trace( Throwable ex, Object obj )
    {
        if ( !MobTalkerConfig.isDebugEnabled() )
            return;
        
        log( Level.TRACE, ex, obj );
    }
    
    public void trace( Throwable ex, String msg, Object... data )
    {
        if ( !MobTalkerConfig.isDebugEnabled() )
            return;
        
        log( Level.TRACE, ex, msg, data );
    }
}
