/*
 * SPDX-FileCopyrightText: 2013-2019 Chimaine, MobTalker2 contributors
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */
package net.mobtalker.mobtalker2.util;

import static net.mobtalker.mobtalker2.util.ItemUtil.*;

import java.util.*;
import java.util.Map.Entry;

import net.minecraft.item.ItemStack;

import com.google.common.collect.Maps;

public class InventoryUtil
{
    public static int getItemCount( ItemStack[] stacks, String name, int meta )
    {
        int count = 0;
        for ( ItemStack stack : stacks )
        {
            if ( ( stack != null ) && matchItem( stack, name, meta ) )
            {
                count += stack.stackSize;
            }
        }
        
        return count;
    }
    
    public static Map<ItemStack, Integer> getTotalItemCounts( ItemStack[] stacks )
    {
        Map<ItemStack, Integer> counts = Maps.newHashMap();
        
        for ( ItemStack stack : stacks )
        {
            if ( stack == null )
                continue;
            
            addItemCount( counts, stack );
        }
        
        return counts;
    }
    
    private static void addItemCount( Map<ItemStack, Integer> counts, ItemStack stack )
    {
        for ( Entry<ItemStack, Integer> entry : counts.entrySet() )
        {
            if ( entry.getKey().isItemEqual( stack ) )
            {
                entry.setValue( entry.getValue() + stack.stackSize );
                return;
            }
        }
        
        counts.put( stack, stack.stackSize );
    }
}
