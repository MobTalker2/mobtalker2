/*
 * SPDX-FileCopyrightText: 2013-2019 Chimaine, MobTalker2 contributors
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */
package net.mobtalker.mobtalker2.util;

import static org.apache.commons.lang3.Validate.notNull;

import java.util.function.*;

public final class LambdaUtil
{
    private LambdaUtil()
    {
    }
    
    public static <T, V> Consumer<T> combine( Function<? super T, ? extends V> converter, Consumer<? super V> action )
    {
        notNull( converter );
        notNull( action );
        return ( t ) -> action.accept( converter.apply( t ) );
    }
    
    public static <T, V, R> Function<T, R> combineFunctions( Function<? super T, ? extends V> func1,
                                                             Function<? super V, ? extends R> func2 )
    {
        notNull( func1 );
        notNull( func2 );
        return ( t ) -> func2.apply( func1.apply( t ) );
    }
}
