/*
 * SPDX-FileCopyrightText: 2013-2019 Chimaine, MobTalker2 contributors
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */
package net.mobtalker.mobtalker2.util;

public final class MathUtil
{
    public static final boolean isInteger( double d )
    {
        return !Double.isInfinite( d ) && ( d == Math.rint( d ) );
    }
}
