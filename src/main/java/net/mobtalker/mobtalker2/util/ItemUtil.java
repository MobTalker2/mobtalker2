/*
 * SPDX-FileCopyrightText: 2013-2019 Chimaine, MobTalker2 contributors
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */
package net.mobtalker.mobtalker2.util;

import net.minecraft.item.*;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.fml.common.registry.GameData;

public class ItemUtil
{
    public static boolean isValidItem( String name )
    {
        return GameData.getItemRegistry().containsKey( new ResourceLocation( name ) );
    }
    
    public static Item getItemForName( String name )
    {
        return GameData.getItemRegistry().getObject( new ResourceLocation( name ) );
    }
    
    public static ItemStack getItemStackForName( String name )
    {
        return new ItemStack( getItemForName( name ) );
    }
    
    public static String getNameForItem( ItemStack itemStack )
    {
        return getNameForItem( itemStack.getItem() );
    }
    
    public static String getNameForItem( Item item )
    {
        return GameData.getItemRegistry().getNameForObject( item ).toString();
    }
    
    // ========================================
    
    /**
     * Attempts to match an item name and meta value with an {@link ItemStack}.
     * <p>
     * If <code>meta</code> is <code>-1</code>, the comparison is meta data insensitive.
     * 
     * @param stack The {@link ItemStack} to match.
     * @param name The item name.
     * @param meta The item meta value.
     * @return If <code>stack</code> matches the given name and meta value.
     */
    public static boolean matchItem( ItemStack stack, String name, int meta )
    {
        if ( meta > -1 )
        {
            if ( stack.getItemDamage() != meta )
                return false;
            
            return name.equals( getNameForItem( stack ) );
        }
        else
        {
            return name.equals( getNameForItem( stack ) );
        }
    }
    
    /**
     * Meta insensitive version of {@link #matchItem(ItemStack, String, int)}.
     */
    public static boolean matchItem( ItemStack stack, String name )
    {
        return name.equals( getNameForItem( stack ) );
    }
}
