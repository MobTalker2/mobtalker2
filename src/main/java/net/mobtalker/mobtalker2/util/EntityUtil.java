/*
 * SPDX-FileCopyrightText: 2013-2019 Chimaine, MobTalker2 contributors
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */
package net.mobtalker.mobtalker2.util;

import java.util.*;

import net.minecraft.entity.*;
import net.minecraft.entity.player.*;
import net.minecraft.world.*;
import net.minecraftforge.fml.server.FMLServerHandler;
import net.mobtalker.mobtalker2.MobTalker2;
import net.mobtalker.mobtalker2.common.entity.EntityType;
import net.mobtalker.mobtalker2.common.entity.ai.PredicateUuid;

public class EntityUtil
{
    /**
     * Returns the name of the entity as defined by it's {@link EntityType}, excluding custom name tags.
     */
    public static String getName( EntityLivingBase entity )
    {
        if ( entity instanceof EntityPlayer )
            return getName( (EntityPlayer) entity );
        
        return EntityType.of( entity ).getDisplayName();
    }
    
    /**
     * Returns the name of the player as per {@link EntityPlayer#getDisplayNameString()}.
     */
    public static String getName( EntityPlayer player )
    {
        try
        {
            return player.getDisplayNameString();
        }
        catch(Throwable t)
        {
            MobTalker2.LOG.debug( t, "Caught exception from EntityPlayer.getDisplayNameString" );
            return player.hasCustomName() ? player.getCustomNameTag() : player.getClass().getName();
        }
    }
    
    /**
     * Returns the name of the entity as defined by it's {@link EntityType} or the custom name tag if defined.
     * 
     * @see Entity#hasCustomName()
     */
    public static String getDisplayName( EntityLivingBase entity )
    {
        if ( entity instanceof EntityPlayer )
            return getName( (EntityPlayer) entity );
        
        return entity.hasCustomName() ? entity.getCustomNameTag() : getName( entity );
    }
    
    // ========================================
    
    public static double getDistance( Entity a, Entity b )
    {
        double dx = a.posX - b.posX;
        double dy = a.posY - b.posY;
        double dz = a.posZ - b.posZ;
        return Math.sqrt( ( dx * dx ) + ( dy * dy ) + ( dz * dz ) );
    }
    
    public static boolean checkDistance( Entity a, Entity b, double max )
    {
        return getDistance( a, b ) <= max;
    }
    
    // ========================================
    
    public static EntityPlayerMP findPlayerByUUID( UUID id )
    {
        for ( WorldServer world : FMLServerHandler.instance().getServer().worldServers )
        {
            EntityPlayer player = world.getPlayerEntityByUUID( id );
            
            if ( player != null )
                return (EntityPlayerMP) player;
        }
        
        return null;
    }
    
    public static EntityPlayerMP findPlayerByName( String name )
    {
        for ( WorldServer world : FMLServerHandler.instance().getServer().worldServers )
        {
            EntityPlayer player = world.getPlayerEntityByName( name );
            
            if ( player != null )
                return (EntityPlayerMP) player;
        }
        
        return null;
    }
    
    public static EntityLivingBase findEntityByUuid( World world, UUID id )
    {
        @SuppressWarnings( "unchecked" )
        List<EntityLivingBase> canidates = world.getEntities( EntityLivingBase.class, new PredicateUuid( id ) );
        if ( canidates.isEmpty() )
            return null;
        
        assert canidates.size() == 1;
        return canidates.get( 0 );
    }
}
