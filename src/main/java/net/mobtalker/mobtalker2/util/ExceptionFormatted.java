/*
 * SPDX-FileCopyrightText: 2013-2019 Chimaine, MobTalker2 contributors
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */
package net.mobtalker.mobtalker2.util;

public class ExceptionFormatted extends Exception
{
    public ExceptionFormatted()
    {
        super();
    }
    
    public ExceptionFormatted( Throwable cause )
    {
        super( cause );
    }
    
    public ExceptionFormatted( String message )
    {
        super( message );
    }
    
    public ExceptionFormatted( String message, Throwable cause )
    {
        super( message, cause );
    }
    
    public ExceptionFormatted( String message, Object... args )
    {
        super( String.format( message, args ) );
    }
    
    public ExceptionFormatted( String message, Throwable cause, Object... args )
    {
        super( String.format( message, args ), cause );
    }
}
