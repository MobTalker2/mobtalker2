/*
 * SPDX-FileCopyrightText: 2013-2019 Chimaine, MobTalker2 contributors
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */
package net.mobtalker.mobtalker2.client.renderer.texture;

import static net.minecraftforge.fml.relauncher.Side.*;

import java.awt.image.BufferedImage;
import java.io.*;

import javax.imageio.ImageIO;

import net.minecraft.client.renderer.texture.*;
import net.minecraft.client.resources.*;
import net.minecraft.client.resources.data.TextureMetadataSection;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.fml.relauncher.SideOnly;

import org.apache.logging.log4j.LogManager;

import de.rummeltsoftware.mc.mgt.Dimensions;

/**
 * Custom texture implementation to support large, re-scalable, non-square textures.
 */
@SideOnly( CLIENT )
public class MobTalkerTexture extends AbstractTexture
{
    private final ResourceLocation _textureLocation;
    
    private Dimensions _size;
    
    // ========================================
    
    public MobTalkerTexture( ResourceLocation resourceLocation )
    {
        _textureLocation = resourceLocation;
    }
    
    // ========================================
    
    @Override
    public void loadTexture( IResourceManager resourceManager )
        throws IOException
    {
        IResource resource = resourceManager.getResource( _textureLocation );
        
        BufferedImage image;
        try (
            InputStream stream = resource.getInputStream() )
        {
            image = ImageIO.read( stream );
        }
        
        boolean blur = false;
        boolean clamp = false;
        
        if ( resource.hasMetadata() )
        {
            try
            {
                TextureMetadataSection meta = (TextureMetadataSection) resource.getMetadata( "texture" );
                
                if ( meta != null )
                {
                    blur = meta.getTextureBlur();
                    clamp = meta.getTextureClamp();
                }
            }
            catch ( RuntimeException ex )
            {
                LogManager.getLogger().warn( "Failed reading metadata of: " + _textureLocation, ex );
            }
        }
        
        _size = scaleToBase( image.getWidth(), image.getHeight(), 256.0F );
        
        int textureID = getGlTextureId();
        
        TextureUtil.allocateTexture( textureID, image.getWidth(), image.getHeight() );
        TextureUtil.uploadTextureImageSub( textureID, image, 0, 0, blur, clamp );
    }
    
    // ========================================
    
    private static Dimensions scaleToBase( int width, int height, float base )
    {
        int max = Math.max( width, height );
        float scale = base / max;
        
        width = (int) ( width * scale );
        height = (int) ( height * scale );
        
        return new Dimensions( width, height );
    }
    
    // ========================================
    
    /**
     * Will return <code>null</code> until {@link #loadTexture(IResourceManager)} has been called.
     */
    public Dimensions getSize()
    {
        return _size;
    }
}
