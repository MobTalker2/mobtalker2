/*
 * SPDX-FileCopyrightText: 2013-2019 Chimaine, MobTalker2 contributors
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */
package net.mobtalker.mobtalker2.client.resources;

import static com.google.common.base.Preconditions.*;
import static net.minecraftforge.fml.relauncher.Side.*;

import java.io.*;
import java.nio.file.*;
import java.util.zip.*;

import net.minecraftforge.fml.relauncher.SideOnly;

@SideOnly( CLIENT )
public class ScriptFileResources extends ScriptResources
{
    private final ZipFile _archiveFile;
    private final String _archiveBasePath;
    
    // ========================================
    
    public ScriptFileResources( String name, Path path, String archiveBasePath )
        throws IOException
    {
        super( name, path );
        checkArgument( Files.isRegularFile( path ) );
        
        _archiveFile = new ZipFile( path.toFile() );
        _archiveBasePath = archiveBasePath;
    }
    
    // ========================================
    
    @Override
    public boolean hasResource( String path )
    {
        return _archiveFile.getEntry( _archiveBasePath + path ) != null;
    }
    
    @Override
    public InputStream getResourceAsStream( String path )
        throws IOException
    {
        ZipEntry entry = _archiveFile.getEntry( _archiveBasePath + path );
        return _archiveFile.getInputStream( entry );
    }
    
    @Override
    public void close()
    {
        try
        {
            _archiveFile.close();
        }
        catch ( IOException ex )
        {}
    }
}
