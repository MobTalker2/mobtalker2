/*
 * SPDX-FileCopyrightText: 2013-2019 Chimaine, MobTalker2 contributors
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */
package net.mobtalker.mobtalker2.client.resources;

import net.minecraftforge.fml.relauncher.SideOnly;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;

import static com.google.common.base.Preconditions.checkArgument;
import static net.minecraftforge.fml.relauncher.Side.CLIENT;

/**
 * Implementation of {@linkplain ScriptResources} which should be used for folder art packs outside a script pack.
 *
 * <p>Not to be confused with {@linkplain ScriptPackResources}.</p>
 */
@SideOnly( CLIENT )
public class ScriptFolderResources extends ScriptResources
{
    public ScriptFolderResources( String name, Path path )
    {
        super( name, path );
        
        checkArgument( Files.exists( _path ) && Files.isDirectory( _path ) );
    }
    
    // ========================================
    
    @Override
    public boolean hasResource( String location )
    {
        return Files.isRegularFile( _path.resolve( location ) );
    }
    
    @Override
    public InputStream getResourceAsStream( String location )
            throws IOException
    {
        Path path = _path.resolve( location );
        checkArgument( Files.isRegularFile( path ) );
        
        return Files.newInputStream( path );
    }
    
    @Override
    public void close()
    {}
}
