/*
 * SPDX-FileCopyrightText: 2013-2019 Chimaine, MobTalker2 contributors
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */
package net.mobtalker.mobtalker2.client.util;

import static net.mobtalker.mobtalker2.util.ChatMessageHelper.*;
import net.minecraft.client.Minecraft;
import net.minecraft.util.EnumChatFormatting;

public class ClientChatMessageUtil
{
    public static void printToChat( String msg, EnumChatFormatting formatting )
    {
        Minecraft.getMinecraft().ingameGUI.getChatGUI().printChatMessage( coloredText( msg, formatting ) );
    }
}
