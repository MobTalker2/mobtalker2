/*
 * SPDX-FileCopyrightText: 2013-2019 Chimaine, MobTalker2 contributors
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */
package net.mobtalker.mobtalker2.client.util;

import static net.minecraftforge.fml.relauncher.Side.*;

import java.util.Random;

import net.minecraft.client.Minecraft;
import net.minecraft.client.multiplayer.WorldClient;
import net.minecraft.entity.EntityLiving;
import net.minecraft.pathfinding.*;
import net.minecraft.util.EnumParticleTypes;
import net.minecraft.world.World;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.event.entity.living.LivingEvent;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.relauncher.SideOnly;

/**
 * Original code by RWTema (http://pastebin.com/zezduLxb)
 */
@SideOnly( CLIENT )
public class EntityPathDebugger
{
    private static final Object SyncLock = new Object();
    private static EntityPathDebugger _activeDebugger;
    
    public static void enable()
    {
        synchronized ( SyncLock )
        {
            if ( _activeDebugger != null )
                return;
            
            _activeDebugger = new EntityPathDebugger();
            MinecraftForge.EVENT_BUS.register( _activeDebugger );
        }
    }
    
    public static void disable()
    {
        synchronized ( SyncLock )
        {
            if ( _activeDebugger == null )
                return;
            
            MinecraftForge.EVENT_BUS.unregister( _activeDebugger );
            _activeDebugger = null;
        }
    }
    
    // ========================================
    
    private EntityPathDebugger()
    {}
    
    @SubscribeEvent
    public void showPath( LivingEvent.LivingUpdateEvent event )
    {
        World world = event.entity.worldObj;
        if ( ( world == null ) || world.isRemote )
            return;
        
        if ( ( world.getTotalWorldTime() % 2 ) != 0 )
            return;
        
        WorldClient theWorld = Minecraft.getMinecraft().theWorld;
        if ( ( theWorld == null ) ||
             ( theWorld.provider == null ) ||
             ( theWorld.provider.getDimensionId() != world.provider.getDimensionId() ) )
            return;
        
        if ( event.entityLiving instanceof EntityLiving )
        {
            EntityLiving entity = (EntityLiving) event.entityLiving;
            
            PathNavigate navigate = entity.getNavigator();
            if ( navigate == null )
                return;
            
            PathEntity path = navigate.getPath();
            if ( ( path == null ) || path.isFinished() )
                return;
            
            Random rand = new Random( entity.getEntityId() );
            double r = rand.nextDouble();
            double g = rand.nextDouble();
            double b = rand.nextDouble();
            
            rand.setSeed( world.getTotalWorldTime() );
            
            for ( int i = 0; ( i + 1 ) < path.getCurrentPathLength(); i++ )
            {
                PathPoint p1 = path.getPathPointFromIndex( i );
                PathPoint p2 = path.getPathPointFromIndex( i + 1 );
                
                float p = rand.nextFloat();
                
                theWorld.spawnParticle( EnumParticleTypes.REDSTONE,
                                        ( p1.xCoord * p ) + ( p2.xCoord * ( 1 - p ) ) + 0.5,
                                        ( p1.yCoord * p ) + ( p2.yCoord * ( 1 - p ) ) + 0.1,
                                        ( p1.zCoord * p ) + ( p2.zCoord * ( 1 - p ) ) + 0.5,
                                        r, g, b );
            }
        }
    }
}
