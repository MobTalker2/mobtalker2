/*
 * SPDX-FileCopyrightText: 2013-2019 Chimaine, MobTalker2 contributors
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */
package net.mobtalker.mobtalker2.client.util;

import static net.minecraftforge.fml.relauncher.Side.*;
import net.minecraft.client.Minecraft;
import net.minecraft.client.audio.*;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.fml.relauncher.SideOnly;

@SideOnly( CLIENT )
public class SoundHelper
{
    public static void playSoundFX( String resource, float volume )
    {
        playSoundFX( new ResourceLocation( resource ), volume );
    }
    
    public static void playSoundFX( ResourceLocation resource, float volume )
    {
        getSoundHandler().playSound( PositionedSoundRecord.create( resource, volume ) );
    }
    
    public static SoundHandler getSoundHandler()
    {
        return Minecraft.getMinecraft().getSoundHandler();
    }
}
