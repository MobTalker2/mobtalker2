/*
 * SPDX-FileCopyrightText: 2013-2019 Chimaine, MobTalker2 contributors
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */
package net.mobtalker.mobtalker2.client.config.gui;

import static net.minecraftforge.fml.relauncher.Side.*;

import java.util.Set;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiScreen;
import net.minecraftforge.fml.client.IModGuiFactory;
import net.minecraftforge.fml.relauncher.SideOnly;

@SideOnly( CLIENT )
public class MobTalkerGuiConfigFactory implements IModGuiFactory
{
    @Override
    public void initialize( Minecraft minecraftInstance )
    {
        
    }
    
    @Override
    public Class<? extends GuiScreen> mainConfigGuiClass()
    {
        return MobTalkerGuiConfig.class;
    }
    
    @Override
    public Set<RuntimeOptionCategoryElement> runtimeGuiCategories()
    {
        return null;
    }
    
    @Override
    public RuntimeOptionGuiHandler getHandlerFor( RuntimeOptionCategoryElement element )
    {
        return null;
    }
}
