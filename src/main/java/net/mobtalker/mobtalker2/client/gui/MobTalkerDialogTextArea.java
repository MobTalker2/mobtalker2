/*
 * SPDX-FileCopyrightText: 2013-2019 Chimaine, MobTalker2 contributors
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */
package net.mobtalker.mobtalker2.client.gui;

import static net.minecraftforge.fml.relauncher.Side.*;
import net.minecraft.client.Minecraft;
import net.minecraftforge.fml.relauncher.SideOnly;
import net.mobtalker.mobtalker2.common.MobTalkerConfig;
import de.rummeltsoftware.mc.mgt.MGTextArea;

@SideOnly( CLIENT )
public class MobTalkerDialogTextArea extends MGTextArea
{
    private static final String CONTINUE_INDICATOR = "\u203A" /* "\u2023" *//* "\u25B6" */;
    private static final String CONVERSATION_END_INDICATOR = "\u220E"; // "\u25A0"; // "\u25FC";
    
    // ========================================
    
    private IEndOfTextReachedListener _listener;
    private boolean _isConclusion;
    
    // ========================================
    
    public MobTalkerDialogTextArea()
    {
        enableTextScrolling( true );
        setTextScrollingSpeed( MobTalkerConfig.getTextScrollingSpeed() );
    }
    
    // ========================================
    
    @Override
    protected void doDraw()
    {
        super.doDraw();
        
        int posX = getBounds().getRight() + 3;
        int posY = ( getBounds().getBottom() - getFontRenderer().FONT_HEIGHT );
        
        if ( _isConclusion && isEndOfText() )
        {
            getFontRenderer().drawString( CONVERSATION_END_INDICATOR, posX, posY, 0xFFFFFFFF, true );
        }
        else
        {
            // Blinking effect
            long elapsed = Minecraft.getSystemTime();
            if ( ( elapsed % 1250 ) > 625 )
                return;
            
            getFontRenderer().drawString( CONTINUE_INDICATOR, posX, posY, 0xFFFFFFFF, true );
        }
    }
    
    // ========================================
    
    @Override
    public void setText( String text )
    {
        setText( text, false );
    }
    
    public void setText( String text, boolean isConclusion )
    {
        super.setText( text );
        _isConclusion = isConclusion;
    }
    
    public void clearText()
    {
        setText( null );
    }
    
    public boolean isEndOfText()
    {
        return _lines.isEmpty() || ( _lines.size() <= getLineCount() );
    }
    
    public void advanceText()
    {
        if ( isEndOfText() )
        {
            assert _listener != null : "Reached end of text, but no listener is set!";
            _listener.onEndOfTextReached();
        }
        else
        {
            for ( int i = 0, linesToRemove = Math.min( getLineCount(), _lines.size() ); i < linesToRemove; i++ )
            {
                _lines.remove( 0 );
            }
            
            resetTextScrolling();
        }
    }
    
    // ========================================
    
    public void setEndOfTextListener( IEndOfTextReachedListener listener )
    {
        _listener = listener;
    }
    
    // ========================================
    
    @FunctionalInterface
    public interface IEndOfTextReachedListener
    {
        void onEndOfTextReached();
    }
}
