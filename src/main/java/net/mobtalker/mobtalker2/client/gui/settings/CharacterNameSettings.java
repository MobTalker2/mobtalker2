/*
 * SPDX-FileCopyrightText: 2013-2019 Chimaine, MobTalker2 contributors
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */
package net.mobtalker.mobtalker2.client.gui.settings;

import java.util.List;

import org.simpleframework.xml.*;

import de.rummeltsoftware.mc.mgt.Alignment;

public class CharacterNameSettings
{
    @Attribute( name = "parent" )
    public String Parent;
    
    @Attribute( name = "width", required = false )
    public int Width;
    
    @ElementList( name = "Anchors" )
    public List<AnchorSettings> Anchors;
    
    @Element( name = "Text" )
    public TextSettings Text;
    
    // ========================================
    
    public static class TextSettings
    {
        @Attribute( name = "align" )
        public Alignment Align;
        
        @Attribute( name = "color" )
        public String Color;
        
        @Attribute( name = "dropShadow" )
        public boolean DropShadow;
    }
}
