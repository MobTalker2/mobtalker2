/*
 * SPDX-FileCopyrightText: 2013-2019 Chimaine, MobTalker2 contributors
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */
package net.mobtalker.mobtalker2.client.gui;

import net.minecraft.util.ResourceLocation;
import de.rummeltsoftware.mc.mgt.MGTexture;

public class CharacterSprite extends MGTexture
{
    public CharacterSprite()
    {}
    
    // ========================================
    
    @Override
    public void setTexture( ResourceLocation location )
    {
        super.setTexture( location );
        if ( _texture == null )
            return;
        
        setSize( _texture.getSize() );
    }
}
