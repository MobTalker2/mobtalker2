/*
 * SPDX-FileCopyrightText: 2013-2019 Chimaine, MobTalker2 contributors
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */
package net.mobtalker.mobtalker2.client.gui.settings;

import org.apache.commons.lang3.StringUtils;

public class Margin
{
    private final int _left;
    private final int _top;
    private final int _right;
    private final int _bottom;
    
    // ========================================
    
    public Margin( int left, int top, int right, int bottom )
    {
        _left = left;
        _top = top;
        _right = right;
        _bottom = bottom;
    }
    
    public static Margin parse( String s )
    {
        String[] parts = StringUtils.split( s, ',' );
        if ( ( parts == null ) || ( parts.length != 4 ) )
            throw new IllegalArgumentException();
        
        return new Margin( Integer.valueOf( parts[0] ),
                           Integer.valueOf( parts[1] ),
                           Integer.valueOf( parts[2] ),
                           Integer.valueOf( parts[3] ) );
    }
    
    // ========================================
    
    public int getLeft()
    {
        return _left;
    }
    
    public int getTop()
    {
        return _top;
    }
    
    public int getRight()
    {
        return _right;
    }
    
    public int getBottom()
    {
        return _bottom;
    }
}
