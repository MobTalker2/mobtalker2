/*
 * SPDX-FileCopyrightText: 2013-2019 Chimaine, MobTalker2 contributors
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */
package net.mobtalker.mobtalker2.client.network;

import static net.minecraftforge.fml.relauncher.Side.*;
import net.minecraft.client.Minecraft;
import net.minecraft.client.entity.EntityPlayerSP;
import net.minecraft.util.EnumChatFormatting;
import net.minecraftforge.fml.client.FMLClientHandler;
import net.minecraftforge.fml.common.network.simpleimpl.*;
import net.minecraftforge.fml.relauncher.SideOnly;
import net.mobtalker.mobtalker2.client.gui.settings.ClientUiSettings;
import net.mobtalker.mobtalker2.client.interaction.ClientInteractionHandler;
import net.mobtalker.mobtalker2.client.util.ClientChatMessageUtil;
import net.mobtalker.mobtalker2.common.network.*;
import de.rummeltsoftware.mc.mgt.Point;
import net.mobtalker.mobtalker2.common.network.message.*;

@SideOnly( CLIENT )
public class ClientMessageHandler
{
    private static ClientMessageHandler instance = new ClientMessageHandler();
    
    public static ClientMessageHandler instance()
    {
        return instance;
    }
    
    // ========================================
    
    public void sendCancelInteraction()
    {
        NetworkManager.sendToServer( new MessageCancelInteraction() );
    }
    
    public void sendTextFinished()
    {
        NetworkManager.sendToServer( new MessageShowTextResponse() );
    }
    
    public void sendChoiceMade( int choice )
    {
        NetworkManager.sendToServer( new MessageShowMenuResponse().setChoice( choice ) );
    }
    
    // ========================================
    
    public void onShowText( MessageShowText msg )
    {
        ClientInteractionHandler handler = getInteractionHandler();
        
        handler.displayName( msg.getName() );
        handler.displayText( msg.getText(), msg.isLast() );
    }
    
    public void onShowMenu( MessageShowMenu msg )
    {
        ClientInteractionHandler handler = getInteractionHandler();
        
        handler.displayChoice( msg.getCaption(),
                               msg.getChoices() );
    }
    
    public void onShowSprite( MessageShowSprite msg )
    {
        ClientInteractionHandler handler = getInteractionHandler();
        
        handler.showSprite( msg.getGroup(),
                            msg.getPath(),
                            msg.getPosition(),
                            new Point( msg.getOffsetX(), msg.getOffsetY() ) );
    }
    
    public void onShowScene( MessageShowScene msg )
    {
        ClientInteractionHandler handler = getInteractionHandler();
        
        handler.showScene( msg.getPath(),
                           msg.getMode() );
    }
    
    public void onHideTexture( MessageHideTexture msg )
    {
        ClientInteractionHandler handler = getInteractionHandler();
        
        handler.hideTexture( msg.getGroup() );
    }
    
    public void onGuiConfig( MessageGuiConfig message )
    {
        try
        {
            ClientUiSettings.set( message.getConfig() );
        }
        catch ( Exception ex )
        {
            ClientChatMessageUtil.printToChat( "[MobTalker2] Recieved an invalid GUI config!", EnumChatFormatting.RED );
        }
    }
    
    // ========================================
    
    public static ClientInteractionHandler getInteractionHandler()
    {
        EntityPlayerSP player = FMLClientHandler.instance().getClientPlayerEntity();
        
        if ( player.openContainer instanceof ClientInteractionHandler )
        {
            return (ClientInteractionHandler) player.openContainer;
        }
        else
        {
            throw new IllegalStateException( "open container is not " + ClientInteractionHandler.class.getName()
                                             + " but " + player.openContainer.getClass().getName() );
        }
    }
    
    // ========================================
    
    public static abstract class Delegator<REQ extends ServerToClientMessage>
        implements IMessageHandler<REQ, ClientToServerMessage>
    {
        public boolean checkThread( final REQ message, final MessageContext ctx )
        {
            if ( Minecraft.getMinecraft().isCallingFromMinecraftThread() )
                return true;
            
            Minecraft.getMinecraft().addScheduledTask( () -> onMessage( message, ctx ) );
            return false;
        }
    }
}
