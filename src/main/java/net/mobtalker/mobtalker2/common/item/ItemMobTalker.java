/*
 * SPDX-FileCopyrightText: 2013-2019 Chimaine, MobTalker2 contributors
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */
package net.mobtalker.mobtalker2.common.item;

import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;

public class ItemMobTalker extends Item
{
    public ItemMobTalker()
    {
        super();
        
        setMaxStackSize( 1 );
        setUnlocalizedName( "mobtalker2:mobtalker" );
        
        setCreativeTab( CreativeTabs.tabTools );
        setMaxDamage( 20 );
        setNoRepair();
    }
}
