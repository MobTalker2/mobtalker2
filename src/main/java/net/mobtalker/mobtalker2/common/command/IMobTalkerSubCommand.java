/*
 * SPDX-FileCopyrightText: 2013-2019 Chimaine, MobTalker2 contributors
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */
package net.mobtalker.mobtalker2.common.command;

import net.minecraft.command.CommandException;
import net.minecraft.command.ICommandSender;
import net.minecraft.util.IChatComponent;

/** @since 0.7.5 */
public interface IMobTalkerSubCommand
{
    /**
     * Handles a subcommand forwarded by the {@linkplain MobTalkerCommand {@code mobtalker} command}.
     *
     * @param sender {@linkplain ICommandSender Sender} who executed the command (not necessarily a player)
     * @param args   All arguments given after the initial {@code /mobtalker}
     *
     * @return {@code true} if the command was handled by this sub-command, {@code false} if not.
     * @throws CommandException thrown if an error occurs while handling the command
     */
    boolean handle( ICommandSender sender, String[] args )
            throws CommandException;
    
    IChatComponent getCommandUsage();
}
