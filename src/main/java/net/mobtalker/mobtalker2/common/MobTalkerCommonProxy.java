/*
 * SPDX-FileCopyrightText: 2013-2019 Chimaine, MobTalker2 contributors
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */
package net.mobtalker.mobtalker2.common;

import net.minecraft.command.ServerCommandManager;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Blocks;
import net.minecraft.init.Items;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;
import net.minecraftforge.fml.common.event.*;
import net.minecraftforge.fml.common.network.NetworkRegistry;
import net.minecraftforge.fml.common.registry.GameRegistry;
import net.mobtalker.mobtalker2.IMobTalkerProxy;
import net.mobtalker.mobtalker2.MobTalker2;
import net.mobtalker.mobtalker2.common.command.MobTalkerCommand;
import net.mobtalker.mobtalker2.common.network.NetworkManager;
import net.mobtalker.mobtalker2.common.network.ServerConnectionHandler;
import net.mobtalker.mobtalker2.common.network.ServerMessageHandler;
import net.mobtalker.mobtalker2.server.interaction.IInteractionAdapter;
import net.mobtalker.mobtalker2.server.interaction.InteractionRegistry;
import net.mobtalker.mobtalker2.server.interaction.ServerInteractionHandler;
import net.mobtalker.mobtalker2.server.resources.scriptpack.ScriptPackRepository;
import net.mobtalker.mobtalker2.server.script.ScriptExecutor;
import net.mobtalker.mobtalker2.server.script.ScriptFactory;
import net.mobtalker.mobtalker2.server.script.lib.metatables.Metatables;
import net.mobtalker.mobtalker2.server.storage.VariableStorageRegistry;
import net.mobtalker.mobtalker2.util.EntityUtil;
import net.mobtalker.mobtalker2.util.logging.MobTalkerLog;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.util.List;

import static org.apache.commons.lang3.Validate.validState;

public abstract class MobTalkerCommonProxy implements IMobTalkerProxy
{
    protected static final MobTalkerLog LOG = new MobTalkerLog( "Proxy" );
    
    // ========================================
    
    @Override
    public void onPreInit( FMLPreInitializationEvent event )
    {
        File configFile = new File( event.getModConfigurationDirectory(), MobTalker2.NAME + ".cfg" );
        MobTalkerConfig.init( configFile );
        
        NetworkManager.load();
        
        MobTalkerItems.register();
    }
    
    @Override
    public void onInit( FMLInitializationEvent event )
    {
        registerRecipes();
        
        NetworkRegistry.INSTANCE.registerGuiHandler( MobTalker2.instance, this );
    }
    
    @Override
    public void onPostInit( FMLPostInitializationEvent event )
    {}
    
    // ========================================
    
    @Override
    public void onServerAboutToStart( FMLServerAboutToStartEvent event )
    {
        ScriptPackRepository.load( getScriptPackPaths() );
        
        Metatables.build();
        
        ScriptFactory.load();
        ScriptFactory.instance().beginPrecompile();
        
        InteractionRegistry.load();
        
        VariableStorageRegistry.load();
        
        ServerConnectionHandler.register();
    }
    
    protected abstract List<Path> getScriptPackPaths();
    
    @Override
    public void onServerStarting( FMLServerStartingEvent event )
    {
        // Register commands
        ServerCommandManager cmdMngr = (ServerCommandManager) event.getServer().getCommandManager();
        
        MobTalkerCommand cmd;
        if ( MobTalkerCommand.isInitialized() )
        {
            cmd = MobTalkerCommand.instance();
        }
        else
        {
            cmd = new MobTalkerCommand();
        }
        cmdMngr.registerCommand( cmd );
    }
    
    @Override
    public void onServerStarted( FMLServerStartedEvent event )
    {
        try
        {
            ScriptFactory.instance().finishPrecompile();
        }
        catch ( InterruptedException ex )
        {
            LOG.error( "Interrupted while waiting for precompilation of scripts" );
        }
        
        ScriptExecutor.load();
    }
    
    @Override
    public void onServerStopping( FMLServerStoppingEvent event )
    {
        InteractionRegistry.unload();
        VariableStorageRegistry.unload();
        ScriptExecutor.unload();
        ScriptFactory.unload();
        ScriptPackRepository.unload();
        ServerMessageHandler.instance().cancelAllCalls();
    }
    
    @Override
    public void onServerStopped( FMLServerStoppedEvent event )
    {}
    
    // ========================================
    // Recipes
    
    private void registerRecipes()
    {
        ItemStack stickStack = new ItemStack( Items.stick );
        ItemStack glowStoneStack = new ItemStack( Blocks.glowstone );
        ItemStack mobTalkerStack = new ItemStack( MobTalkerItems.talker );
        
        GameRegistry.addRecipe( mobTalkerStack, "x", "y", 'x', glowStoneStack, 'y', stickStack );
    }
    
    // ========================================
    
    @Override
    public Object getClientGuiElement( int ID, EntityPlayer player, World world, int x, int y, int z )
    {
        throw new AssertionError();
    }
    
    @Override
    public Object getServerGuiElement( int id, EntityPlayer player, World world, int x, int y, int z )
    {
        IInteractionAdapter adapter = InteractionRegistry.instance().getAdapter( player );
        
        validState( adapter != null,
                    "Tried to open gui for '%s', who does not have a partner",
                    EntityUtil.getName( player ) );
    
        if ( id == 1 )
        {
            return new ServerInteractionHandler( adapter );
        }
        
        throw new AssertionError( "Tried to open unknown gui id '" + id
                                  + "' for '" + EntityUtil.getName( player ) + "'" );
    }
    
    // ========================================
    
    protected abstract Path getWorldSavePath()
            throws IOException;
    
}
