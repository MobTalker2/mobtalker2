/*
 * SPDX-FileCopyrightText: 2013-2019 Chimaine, MobTalker2 contributors
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */
package net.mobtalker.mobtalker2.common.launch;

import java.awt.GraphicsEnvironment;
import java.util.Map;

import javax.swing.JOptionPane;

import net.minecraftforge.fml.relauncher.IFMLLoadingPlugin;

import org.apache.logging.log4j.*;

public class JavaVersionCheckPlugin implements IFMLLoadingPlugin
{
    public static final int MINIMUM_JAVA_VERSION = 8;
    
    // ========================================
    
    public JavaVersionCheckPlugin()
    {
        checkJavaVersion();
    }
    
    // ========================================
    
    private static void checkJavaVersion()
    {
        Logger log = LogManager.getLogger( "MobTalker2" );
        
        log.debug( "Checking Java version" );
        
        int version = Integer.parseInt( System.getProperty( "java.version" ).split( "\\." )[1] );
        if ( MINIMUM_JAVA_VERSION <= version )
        {
            log.debug( "Found sufficient Java version: " + version );
            return;
        }
        
        log.fatal( "Insufficient Java version: " + version + " (required: " + MINIMUM_JAVA_VERSION + ")" );
        
        if ( !GraphicsEnvironment.isHeadless() )
        {
            String msg = "MobTalker2 requires Java " + MINIMUM_JAVA_VERSION + " to run. "
                         + "Your current Java version is Java " + version + ". "
                         + "Please update to or run Minecraft with at least Java " + MINIMUM_JAVA_VERSION + ", or remove MobTalker2.";
            JOptionPane.showMessageDialog( null, msg, "Insufficient Java version", JOptionPane.ERROR_MESSAGE );
        }
        
        System.exit( 1 );
    }
    
    // ========================================
    
    @Override
    public String[] getASMTransformerClass()
    {
        return null;
    }
    
    @Override
    public String getModContainerClass()
    {
        return null;
    }
    
    @Override
    public String getSetupClass()
    {
        return null;
    }
    
    @Override
    public void injectData( Map<String, Object> data )
    {}
    
    @Override
    public String getAccessTransformerClass()
    {
        return null;
    }
}
