/*
 * SPDX-FileCopyrightText: 2013-2019 Chimaine, MobTalker2 contributors
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */
package net.mobtalker.mobtalker2.common.launch;

import static net.minecraftforge.fml.relauncher.Side.*;
import net.minecraft.client.gui.*;
import net.minecraftforge.fml.client.CustomModLoadingErrorDisplayException;
import net.minecraftforge.fml.relauncher.SideOnly;

@SideOnly( CLIENT )
public class FingerprintViolationException extends CustomModLoadingErrorDisplayException
{
    @Override
    public void initGui( GuiErrorScreen errorScreen, FontRenderer fontRenderer )
    {}
    
    @Override
    public void drawScreen( GuiErrorScreen errorScreen,
                            FontRenderer fontRenderer,
                            int mouseRelX,
                            int mouseRelY,
                            float tickTime )
    {
        int centerX = errorScreen.width / 2;
        
        int offset = 40;
        errorScreen.drawCenteredString( fontRenderer,
                                        "MobTalker2 has detected an invalid fingerprint for its mod file.",
                                        centerX, offset, 0xFFFFFF );
        
        offset += 20;
        errorScreen.drawCenteredString( fontRenderer,
                                        "!!! THIS COULD MEAN THAT SOMEBODY INJECTED MALICIOUS CODE INTO IT !!!",
                                        centerX, offset, 0xFFFFFF );
        
        offset += 20;
        errorScreen.drawCenteredString( fontRenderer,
                                        "To protect your security Minecraft has been stopped.",
                                        centerX, offset, 0xFFFFFF );
        
        offset += 20;
        errorScreen.drawCenteredString( fontRenderer,
                                        "Please download MobTalker2 only from",
                                        centerX, offset, 0xFFFFFF );
        
        offset += 10;
        errorScreen.drawCenteredString( fontRenderer,
                                        "the official download location at",
                                        centerX, offset, 0xFFFFFF );
        
        offset += 10;
        errorScreen.drawCenteredString( fontRenderer,
                                        "www.mobtalker.net",
                                        centerX, offset, 0xFFFFFF );
    }
    
    // ========================================
    
    @Override
    public String getMessage()
    {
        return "MobTalker2 has detected an invalid fingerprint for its mod file. "
               + "THIS COULD MEAN THAT SOMEBODY INJECTED MALICIOUS CODE INTO IT! "
               + "To protect your security Minecraft has been stopped. "
               + "Please download MobTalker2 only from the official download location at "
               + "www.mobtalker.net";
    }
}