/*
 * SPDX-FileCopyrightText: 2013-2019 Chimaine, MobTalker2 contributors
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */
package net.mobtalker.mobtalker2.common.entity.monster;

import static net.mobtalker.mobtalker2.common.entity.ai.EntityAiTaskUtil.*;
import net.minecraft.entity.ai.*;
import net.minecraft.entity.monster.EntityPigZombie;
import net.mobtalker.mobtalker2.common.entity.EntityType;
import net.mobtalker.mobtalker2.common.entity.ai.*;
import net.mobtalker.mobtalker2.server.interaction.*;
import net.mobtalker.mobtalker2.server.script.lib.EntityReaction;

public class PigZombieAdapterInitializer implements IInteractionAdapterInitializer<EntityPigZombie>
{
    @Override
    public void initialize( IInteractionAdapter adapter, EntityPigZombie entity,
                            EntityAITasks tasks, EntityAITasks targetTasks )
    {
        // Tasks
        replaceIndex( tasks, 2,
                      EntityAIAvoidEntity.class,
                      new MobTalkerAiAvoidEntity( adapter, 1.5D, 1.2D, 0.8D, 4.0D,
                                                  PredicateExplodingCreeper.Instance ) );
        replaceIndex( tasks, 7,
                      EntityAIAttackOnCollide.class,
                      new EntityAIAttackOnCollide( entity, null, 1.0D, true ) );
        
        removeIndex( tasks, 8, EntityAIAttackOnCollide.class );
        
        addLast( tasks, 1, new MobTalkerAiInteractPlayer( adapter ) );
        addLast( tasks, 2, new MobTalkerAiAvoidEntity( adapter, 1.5D, 1.2D, 0.8D, 6.0D,
                                                       new PredicateReactionScared( adapter ) ) );
        
        // Target tasks
        replaceIndex( targetTasks, 0,
                      EntityAIHurtByTarget.class,
                      new MobTalkerAiZombiePigHurtByTarget( adapter ) );
        replaceIndex( targetTasks, 1,
                      EntityAINearestAttackableTarget.class,
                      new MobTalkerAiZombiePigTarget( adapter ) );
        
        removeIndex( targetTasks, 3, EntityAINearestAttackableTarget.class );
        removeIndex( targetTasks, 2, EntityAINearestAttackableTarget.class );
        
        // Reactions
        adapter.setReaction( EntityReaction.HOSTILE, EntityType.Player );
        adapter.setReaction( EntityReaction.HOSTILE, EntityType.IronGolem );
        adapter.setReaction( EntityReaction.HOSTILE, EntityType.Villager );
    }
}
