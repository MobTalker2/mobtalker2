/*
 * SPDX-FileCopyrightText: 2013-2019 Chimaine, MobTalker2 contributors
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */
package net.mobtalker.mobtalker2.common.entity.ai;

import net.minecraft.entity.EntityLivingBase;
import net.mobtalker.mobtalker2.server.interaction.IInteractionAdapter;
import net.mobtalker.mobtalker2.server.script.lib.EntityReaction;

import com.google.common.base.Predicate;

public class PredicateReactionScared implements Predicate<EntityLivingBase>
{
    private final IInteractionAdapter _adapter;
    
    public PredicateReactionScared( IInteractionAdapter adapter )
    {
        _adapter = adapter;
    }
    
    @Override
    public boolean apply( EntityLivingBase entity )
    {
        return _adapter.getReaction( entity ) == EntityReaction.SCARED;
    }
}
