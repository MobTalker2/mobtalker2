/*
 * SPDX-FileCopyrightText: 2013-2019 Chimaine, MobTalker2 contributors
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */
package net.mobtalker.mobtalker2.common.entity;

import static com.google.common.base.Preconditions.*;

import java.util.*;

public class VillagerUtil
{
    private static final List<VillagerProfession> _professions;
    
    static
    {
        _professions = new ArrayList<>();
        _professions.add( new VillagerProfession( "Farmer", "Farmer", "Fisherman", "Shepherd", "Fletcher" ) );
        _professions.add( new VillagerProfession( "Librarian", "Librarian" ) );
        _professions.add( new VillagerProfession( "Priest", "Cleric" ) );
        _professions.add( new VillagerProfession( "Blacksmith", "Armorer", "Weapon Smith", "Tool Smith" ) );
        _professions.add( new VillagerProfession( "Butcher", "Butcher", "Leatherworker" ) );
    }
    
    // ========================================
    
    public static String getProfessionName( int professionId )
    {
        checkElementIndex( professionId, _professions.size() );
        return _professions.get( professionId ).Name;
    }
    
    public static String getCareerName( int professionId, int careerId )
    {
        checkElementIndex( professionId, _professions.size() );
        VillagerProfession profession = _professions.get( professionId );
        
        checkElementIndex( careerId - 1, profession.Careers.length );
        
        return profession.Careers[careerId - 1];
    }
    
    // ========================================
    
    private static class VillagerProfession
    {
        public final String Name;
        public final String[] Careers;
        
        public VillagerProfession( String name, String... careers )
        {
            Name = name;
            Careers = careers;
        }
    }
}
