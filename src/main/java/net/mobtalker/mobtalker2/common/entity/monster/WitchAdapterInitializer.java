/*
 * SPDX-FileCopyrightText: 2013-2019 Chimaine, MobTalker2 contributors
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */
package net.mobtalker.mobtalker2.common.entity.monster;

import static net.mobtalker.mobtalker2.common.entity.ai.EntityAiTaskUtil.*;
import net.minecraft.entity.ai.*;
import net.minecraft.entity.monster.EntityWitch;
import net.mobtalker.mobtalker2.common.entity.EntityType;
import net.mobtalker.mobtalker2.common.entity.ai.*;
import net.mobtalker.mobtalker2.server.interaction.*;
import net.mobtalker.mobtalker2.server.script.lib.EntityReaction;

public class WitchAdapterInitializer implements IInteractionAdapterInitializer<EntityWitch>
{
    @Override
    public void initialize( IInteractionAdapter adapter, EntityWitch entity,
                            EntityAITasks tasks, EntityAITasks targetTasks )
    {
        // Tasks
        replaceIndex( tasks, 3,
                      EntityAIAvoidEntity.class,
                      new MobTalkerAiAvoidEntity( adapter, 1.5D, 1.2D, 0.8D, 4.0D,
                                                  PredicateExplodingCreeper.Instance ) );
        
        addLast( tasks, 1,
                 new MobTalkerAiInteractPlayer( adapter ) );
        addLast( tasks, 2,
                 new MobTalkerAiAvoidEntity( adapter, 1.5D, 1.2D, 0.8D, 6.0D,
                                             new PredicateReactionScared( adapter ) ) );
        
        // Target Tasks
        replaceIndex( targetTasks, 0,
                      EntityAIHurtByTarget.class,
                      new MobTalkerAiHurtByTarget( adapter, false ) );
        replaceIndex( targetTasks, 1,
                      EntityAINearestAttackableTarget.class,
                      new MobTalkerAiNearestAttackableTarget<>( EntityWitch.class, adapter ) );
        
        // Reactions
        adapter.setReaction( EntityReaction.HOSTILE, EntityType.Player );
    }
}
