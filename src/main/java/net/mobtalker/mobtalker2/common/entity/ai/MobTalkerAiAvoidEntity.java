/*
 * SPDX-FileCopyrightText: 2013-2019 Chimaine, MobTalker2 contributors
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */
package net.mobtalker.mobtalker2.common.entity.ai;

import static net.minecraft.entity.ai.RandomPositionGenerator.*;

import java.util.*;

import net.minecraft.entity.*;
import net.minecraft.pathfinding.*;
import net.minecraft.util.*;
import net.minecraft.world.World;
import net.mobtalker.mobtalker2.server.interaction.IInteractionAdapter;

import com.google.common.base.Predicate;

public class MobTalkerAiAvoidEntity extends MobTalkerAiBase<EntityCreature>
{
    private final double _nearSpeed;
    private final double _midSpeed;
    private final double _farSpeed;
    private final double _minDistance;
    
    private final Predicate<EntityLivingBase> _selector;
    private final EntityDistanceSorter _sorter;
    
    private final PathNavigate _navigator;
    
    private EntityLivingBase _fleeTarget;
    private PathEntity _currentPath;
    
    // ========================================
    
    public MobTalkerAiAvoidEntity( IInteractionAdapter adapter,
                                   double nearSpeed, double midSpeed, double farSpeed,
                                   double minDistance,
                                   Predicate<EntityLivingBase> selector )
    {
        super( EntityCreature.class, adapter );
        
        _nearSpeed = nearSpeed;
        _midSpeed = midSpeed;
        _farSpeed = farSpeed;
        _minDistance = minDistance;
        
        _selector = selector;
        _sorter = new EntityDistanceSorter( _entity );
        
        _navigator = _entity.getNavigator();
        
        setMutexBits( 1 );
    }
    
    // ========================================
    
    @Override
    public boolean shouldExecute()
    {
        EntityLivingBase target = getFleeTarget();
        if ( ( target == null ) || target.isDead )
            return false;
        
        Vec3 fleeVector = findRandomTargetBlockAwayFrom( _entity, 16, 7, new Vec3( target.posX, target.posY, target.posZ ) );
        
        if ( fleeVector == null )
            return false;
        if ( target.getDistanceSq( fleeVector.xCoord, fleeVector.yCoord, fleeVector.zCoord ) < target.getDistanceSqToEntity( _entity ) )
            return false;
        
        PathEntity path = _navigator.getPathToXYZ( fleeVector.xCoord, fleeVector.yCoord, fleeVector.zCoord );
        if ( ( path != null ) && path.isDestinationSame( fleeVector ) )
        {
            _currentPath = path;
            _fleeTarget = target;
            return true;
        }
        
        return false;
    }
    
    private EntityLivingBase getFleeTarget()
    {
        World world = _entity.worldObj;
        AxisAlignedBB aabb = _entity.getEntityBoundingBox().expand( _minDistance, 3.0D, _minDistance );
        
        @SuppressWarnings( "unchecked" )
        List<EntityLivingBase> entities = world.getEntitiesInAABBexcluding( _entity, aabb,
                                                                            PredicateAliveLivingEntity.Instance );
        if ( entities.isEmpty() )
            return null;
        
        Collections.sort( entities, _sorter );
        
        for ( EntityLivingBase entity : entities )
        {
            if ( _selector.apply( entity ) )
                return entity;
        }
        
        return null;
    }
    
    @Override
    public boolean continueExecuting()
    {
        return !_navigator.noPath();
    }
    
    @Override
    public void startExecuting()
    {
        _navigator.setPath( _currentPath, _farSpeed );
    }
    
    @Override
    public void resetTask()
    {
        _currentPath = null;
        _fleeTarget = null;
    }
    
    @Override
    public void updateTask()
    {
        double distance = _entity.getDistanceSqToEntity( _fleeTarget );
        if ( distance < 36.0 )
        {
            _navigator.setSpeed( _nearSpeed );
        }
        else if ( distance < 100.0 )
        {
            _navigator.setSpeed( _midSpeed );
        }
        else
        {
            _navigator.setSpeed( _farSpeed );
        }
    }
}
