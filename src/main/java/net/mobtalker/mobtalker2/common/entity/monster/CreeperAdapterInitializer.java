/*
 * SPDX-FileCopyrightText: 2013-2019 Chimaine, MobTalker2 contributors
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */
package net.mobtalker.mobtalker2.common.entity.monster;

import static net.mobtalker.mobtalker2.common.entity.ai.EntityAiTaskUtil.*;
import net.minecraft.entity.ai.*;
import net.minecraft.entity.monster.EntityCreeper;
import net.mobtalker.mobtalker2.common.entity.EntityType;
import net.mobtalker.mobtalker2.common.entity.ai.*;
import net.mobtalker.mobtalker2.server.interaction.*;
import net.mobtalker.mobtalker2.server.script.lib.EntityReaction;

public class CreeperAdapterInitializer implements IInteractionAdapterInitializer<EntityCreeper>
{
    @Override
    public void initialize( IInteractionAdapter adapter, EntityCreeper entity,
                            EntityAITasks tasks, EntityAITasks targetTasks )
    {
        // Tasks
        replaceIndex( tasks, 1,
                      EntityAICreeperSwell.class,
                      new MobTalkerAiCreeperSwell( adapter ) );
        replaceIndex( tasks, 2,
                      EntityAIAvoidEntity.class,
                      new MobTalkerAiAvoidEntity( adapter, 2.0D, 1.5D, 0.8D, 4.0D,
                                                  PredicateExplodingCreeper.Instance ) );
        replaceIndex( tasks, 3,
                      EntityAIAvoidEntity.class,
                      new MobTalkerAiAvoidEntity( adapter, 2.0D, 1.5D, 0.8D, 6.0D,
                                                  new PredicateReactionScared( adapter ) ) );
        
        addLast( tasks, 1,
                 new MobTalkerAiInteractPlayer( adapter ) );
        insert( tasks, 5,
                new MobTalkerAiFollowEntity( adapter, 1.0D, 3.0D, 10.0D ) );
        
        // Target Tasks
        replaceIndex( targetTasks, 0,
                      EntityAINearestAttackableTarget.class,
                      new MobTalkerAiNearestAttackableTarget<>( EntityCreeper.class, adapter ) );
        replaceIndex( targetTasks, 1,
                      EntityAIHurtByTarget.class,
                      new MobTalkerAiHurtByTarget( adapter, false ) );
        
        // Reactions
        adapter.setReaction( EntityReaction.SCARED, EntityType.Ocelot );
        adapter.setReaction( EntityReaction.HOSTILE, EntityType.Player );
    }
}
