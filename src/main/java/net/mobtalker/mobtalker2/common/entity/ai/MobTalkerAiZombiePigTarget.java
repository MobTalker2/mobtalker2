/*
 * SPDX-FileCopyrightText: 2013-2019 Chimaine, MobTalker2 contributors
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */
package net.mobtalker.mobtalker2.common.entity.ai;

import net.minecraft.entity.monster.EntityPigZombie;
import net.mobtalker.mobtalker2.server.interaction.IInteractionAdapter;

public class MobTalkerAiZombiePigTarget extends MobTalkerAiNearestAttackableTarget<EntityPigZombie>
{
    public MobTalkerAiZombiePigTarget( IInteractionAdapter adapter )
    {
        super( EntityPigZombie.class, adapter );
    }
    
    @Override
    public boolean shouldExecute()
    {
        return _entity.isAngry() && super.shouldExecute();
    }
}
