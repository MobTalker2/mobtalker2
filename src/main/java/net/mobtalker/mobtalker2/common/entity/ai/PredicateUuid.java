/*
 * SPDX-FileCopyrightText: 2013-2019 Chimaine, MobTalker2 contributors
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */
package net.mobtalker.mobtalker2.common.entity.ai;

import java.util.UUID;

import net.minecraft.entity.EntityLivingBase;

import com.google.common.base.Predicate;

public class PredicateUuid implements Predicate<EntityLivingBase>
{
    private final UUID _uuid;
    
    public PredicateUuid( UUID id )
    {
        _uuid = id;
    }
    
    @Override
    public boolean apply( EntityLivingBase entity )
    {
        return entity.getUniqueID().equals( _uuid );
    }
}