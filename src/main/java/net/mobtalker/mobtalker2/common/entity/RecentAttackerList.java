/*
 * SPDX-FileCopyrightText: 2013-2019 Chimaine, MobTalker2 contributors
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */
package net.mobtalker.mobtalker2.common.entity;

import java.util.*;

import net.minecraft.nbt.*;
import net.minecraft.util.MathHelper;
import net.minecraftforge.common.util.Constants.NBT;
import net.mobtalker.mobtalker2.server.interaction.IInteractionAdapter;

import com.google.common.collect.Maps;

public class RecentAttackerList
{
    private final IInteractionAdapter _adapter;
    private final Map<UUID, AttackRecord> _attackers;
    
    // ========================================
    
    public RecentAttackerList( IInteractionAdapter adapter )
    {
        _adapter = adapter;
        _attackers = Maps.newHashMapWithExpectedSize( 1 );
    }
    
    // ========================================
    
    public int count()
    {
        return _attackers.size();
    }
    
    public void addAttacker( UUID attacker, long timestamp, float amount )
    {
        AttackRecord existingRecord = _attackers.get( attacker );
        if ( ( existingRecord != null ) && !hasForgiven( existingRecord ) )
        {
            amount += existingRecord.getDamageCaused();
        }
        
        AttackRecord newRecord = new AttackRecord( attacker, timestamp, amount );
        _attackers.put( attacker, newRecord );
    }
    
    public boolean wasRecentlyAttackedBy( UUID attacker )
    {
        AttackRecord record = _attackers.get( attacker );
        if ( record == null )
            return false;
        
        if ( hasForgiven( record ) )
        {
            _attackers.remove( attacker );
            return false;
        }
        
        return true;
    }
    
    public void forgive( UUID attacker )
    {
        _attackers.remove( attacker );
    }
    
    public Set<UUID> getRecentAttackers()
    {
        return _attackers.keySet();
    }
    
    public void cleanup()
    {
        for ( Iterator<AttackRecord> iter = _attackers.values().iterator(); iter.hasNext(); )
        {
            if ( hasForgiven( iter.next() ) )
            {
                iter.remove();
            }
        }
    }
    
    public void clear()
    {
        _attackers.clear();
    }
    
    // ========================================
    
    private boolean hasForgiven( AttackRecord record )
    {
        float baseTime = record.getDamageCaused() * 100.0F;
        int revengeTime = MathHelper.floor_float( baseTime * _adapter.getUnforgivingness() );
        
        return ( record.getTimeOfAttack() + revengeTime ) <= _adapter.getEntity().worldObj.getWorldTime();
    }
    
    // ========================================
    
    public NBTTagCompound serialize()
    {
        cleanup();
        
        NBTTagCompound store = new NBTTagCompound();
        
        NBTTagList recordList = new NBTTagList();
        for ( AttackRecord record : _attackers.values() )
        {
            NBTTagCompound recordStore = new NBTTagCompound();
            recordStore.setString( "attacker", record.getAttacker().toString() );
            recordStore.setLong( "timestamp", record.getTimeOfAttack() );
            recordStore.setFloat( "damage", record.getDamageCaused() );
            
            recordList.appendTag( recordStore );
        }
        
        store.setTag( "records", recordList );
        
        return store;
    }
    
    public void deserialize( NBTTagCompound store )
    {
        if ( store.hasKey( "records", NBT.TAG_LIST ) )
        {
            NBTTagList recordList = store.getTagList( "records", NBT.TAG_COMPOUND );
            
            for ( int i = 0; i < recordList.tagCount(); i++ )
            {
                NBTTagCompound recordStore = recordList.getCompoundTagAt( i );
                UUID attacker = UUID.fromString( recordStore.getString( "attacker" ) );
                long timestamp = recordStore.getLong( "timestamp" );
                float damageCaused = recordStore.getFloat( "damage" );
                
                _attackers.put( attacker, new AttackRecord( attacker, timestamp, damageCaused ) );
            }
        }
        
        cleanup();
    }
}
