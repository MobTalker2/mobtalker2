/*
 * SPDX-FileCopyrightText: 2013-2019 Chimaine, MobTalker2 contributors
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */
package net.mobtalker.mobtalker2.common.entity.ai;

import java.util.List;

import net.minecraft.entity.*;
import net.minecraft.util.AxisAlignedBB;
import net.mobtalker.mobtalker2.server.interaction.IInteractionAdapter;

public class MobTalkerAiHurtByTarget extends MobTalkerAiTargetBase<EntityLiving>
{
    /**
     * If this entity should call for help when executing this task.
     */
    private final boolean _callForHelp;
    
    /**
     * Needed for an check. Not sure what that check's use is. Vanilla does it.
     */
    private int _hurtTime;
    
    // ========================================
    
    public MobTalkerAiHurtByTarget( IInteractionAdapter adapter, boolean checkSight )
    {
        super( EntityLiving.class, adapter, checkSight, false, false );
        setMutexBits( 0b0001 );
        
        _callForHelp = false;
    }
    
    // ========================================
    
    private AxisAlignedBB getCallForHelpArea()
    {
        double max = _adapter.getFollowRange();
        return new AxisAlignedBB( _entity.posX, _entity.posY, _entity.posZ,
                                  _entity.posX + 1.0D, _entity.posY + 1.0D, _entity.posZ + 1.0D ).expand( max, 10.0D, max );
    }
    
    protected void callForHelp( EntityLivingBase target )
    {
        @SuppressWarnings( "unchecked" )
        List<EntityCreature> entities = _entity.worldObj.getEntitiesWithinAABB( _entity.getClass(), getCallForHelpArea() );
        
        for ( EntityCreature entity : entities )
        {
            if ( entity.equals( _entity ) || entity.equals( target ) )
                continue;
            if ( ( entity.getAttackTarget() != null ) || entity.isOnSameTeam( target ) )
                continue;
            
            askEntityForHelp( entity, target );
        }
    }
    
    protected void askEntityForHelp( EntityCreature helper, EntityLivingBase target )
    {
        helper.setAttackTarget( target );
    }
    
    // ========================================
    
    @Override
    public boolean shouldExecute()
    {
        return ( _entity.hurtTime != _hurtTime ) && isValidTarget( _entity.getAITarget() );
    }
    
    @Override
    public void startExecuting()
    {
        super.startExecuting();
        
        EntityLivingBase target = _entity.getAITarget();
        
        _entity.setAttackTarget( target );
        _hurtTime = _entity.hurtTime;
        
        if ( _callForHelp )
        {
            callForHelp( target );
        }
    }
}
