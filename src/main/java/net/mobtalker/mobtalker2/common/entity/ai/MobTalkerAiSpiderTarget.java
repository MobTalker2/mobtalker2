/*
 * SPDX-FileCopyrightText: 2013-2019 Chimaine, MobTalker2 contributors
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */
package net.mobtalker.mobtalker2.common.entity.ai;

import net.minecraft.entity.monster.EntitySpider;
import net.mobtalker.mobtalker2.server.interaction.IInteractionAdapter;

public class MobTalkerAiSpiderTarget extends MobTalkerAiNearestAttackableTarget<EntitySpider>
{
    public MobTalkerAiSpiderTarget( IInteractionAdapter adapter )
    {
        super( EntitySpider.class, adapter );
    }
    
    @Override
    public boolean shouldExecute()
    {
        return ( _entity.getBrightness( 1.0F ) < 0.5F ) && super.shouldExecute();
    }
}
