/*
 * SPDX-FileCopyrightText: 2013-2019 Chimaine, MobTalker2 contributors
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */
package net.mobtalker.mobtalker2.common.entity.ai;

import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.monster.EntityCreeper;
import net.mobtalker.mobtalker2.server.interaction.IInteractionAdapter;

public class MobTalkerAiCreeperSwell extends MobTalkerAiBase<EntityCreeper>
{
    public MobTalkerAiCreeperSwell( IInteractionAdapter adapter )
    {
        super( EntityCreeper.class, adapter );
        setMutexBits( 0b001 );
    }
    
    // ========================================
    
    @Override
    public boolean shouldExecute()
    {
        EntityLivingBase target = _entity.getAttackTarget();
        
        if ( _entity.getCreeperState() > 0 ) // If the world was saved while the Creeper was swelling
            return true;
        
        if ( ( target == null ) || !target.isEntityAlive() )
            return false;
        if ( _adapter.isActive() )
            return false;
        if ( _entity.getDistanceSqToEntity( target ) >= 9.0D )
            return false;
        if ( !_entity.getEntitySenses().canSee( target ) )
            return false;
        
        return true;
    }
    
    @Override
    public boolean continueExecuting()
    {
        EntityLivingBase target = _entity.getAttackTarget();
        
        if ( ( target == null ) || !target.isEntityAlive() )
            return false;
        if ( _adapter.isActive() )
            return false;
        if ( _entity.getDistanceSqToEntity( target ) > 49.0D )
            return false;
        if ( !_entity.getEntitySenses().canSee( target ) )
            return false;
        
        return true;
    }
    
    @Override
    public void startExecuting()
    {
        _entity.getNavigator().clearPathEntity();
        _entity.setCreeperState( 1 );
    }
    
    @Override
    public void resetTask()
    {
        _entity.setCreeperState( -1 );
    }
}