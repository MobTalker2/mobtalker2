/*
 * SPDX-FileCopyrightText: 2013-2019 Chimaine, MobTalker2 contributors
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */
package net.mobtalker.mobtalker2.common.event;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.fml.common.eventhandler.*;
import net.mobtalker.mobtalker2.server.interaction.IInteractionAdapter;

public class MobTalkerEvent extends Event
{
    public static boolean queryReqestInteraction( IInteractionAdapter adapter, EntityPlayer player )
    {
        return !MinecraftForge.EVENT_BUS.post( new MobTalkerRequestInteractionEvent( adapter, player ) );
    }
    
    public static void notifyStartInteraction( IInteractionAdapter adapter )
    {
        MinecraftForge.EVENT_BUS.post( new MobTalkerStartInteractionEvent( adapter ) );
    }
    
    public static void notifyEndInteraction( IInteractionAdapter adapter )
    {
        MinecraftForge.EVENT_BUS.post( new MobTalkerEndInteractionEvent( adapter ) );
    }
    
    // ========================================
    
    private final IInteractionAdapter _adapter;
    
    // ========================================
    
    public MobTalkerEvent( IInteractionAdapter adapter )
    {
        _adapter = adapter;
    }
    
    // ========================================
    
    public IInteractionAdapter getAdapter()
    {
        return _adapter;
    }
    
    // ========================================
    
    @Cancelable
    public static class MobTalkerRequestInteractionEvent extends MobTalkerEvent
    {
        private final EntityPlayer _requester;
        
        public MobTalkerRequestInteractionEvent( IInteractionAdapter adapter, EntityPlayer requester )
        {
            super( adapter );
            _requester = requester;
        }
        
        public EntityPlayer getRequester()
        {
            return _requester;
        }
        
        @Override
        public boolean isCancelable()
        {
            return true;
        }
    }
    
    // ========================================
    
    public static class MobTalkerStartInteractionEvent extends MobTalkerEvent
    {
        public MobTalkerStartInteractionEvent( IInteractionAdapter adapter )
        {
            super( adapter );
        }
    }
    
    // ========================================
    
    @Cancelable
    public static class MobTalkerEndInteractionEvent extends MobTalkerEvent
    {
        public MobTalkerEndInteractionEvent( IInteractionAdapter adapter )
        {
            super( adapter );
        }
    }
    
}
