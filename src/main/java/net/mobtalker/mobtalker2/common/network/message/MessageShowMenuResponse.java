/*
 * SPDX-FileCopyrightText: 2013-2019 Chimaine, MobTalker2 contributors
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */
package net.mobtalker.mobtalker2.common.network.message;

import io.netty.buffer.ByteBuf;
import net.mobtalker.mobtalker2.common.network.ServerMessageHandler;

public class MessageShowMenuResponse extends ClientToServerMessage
{
    private int _choice;
    
    // ========================================
    
    public int getChoice()
    {
        return _choice;
    }
    
    public MessageShowMenuResponse setChoice( int choice )
    {
        _choice = choice;
        return this;
    }
    
    // ========================================
    
    @Override
    public void fromBytes( ByteBuf buf )
    {
        _choice = buf.readUnsignedByte();
    }
    
    @Override
    public void toBytes( ByteBuf buf )
    {
        buf.writeByte( _choice );
    }
    
    // ========================================
    
    public static class Handler extends ServerMessageHandler.RemoteCallResultDelegator<MessageShowMenuResponse>
    {}
}
