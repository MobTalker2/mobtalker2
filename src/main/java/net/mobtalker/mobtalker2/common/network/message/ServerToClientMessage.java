/*
 * SPDX-FileCopyrightText: 2013-2019 Chimaine, MobTalker2 contributors
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */
package net.mobtalker.mobtalker2.common.network.message;

import io.netty.buffer.ByteBuf;

public abstract class ServerToClientMessage extends MobTalkerMessage
{
    @Override
    public abstract void toBytes( ByteBuf buf );
    
    @Override
    public abstract void fromBytes( ByteBuf buf );
}
