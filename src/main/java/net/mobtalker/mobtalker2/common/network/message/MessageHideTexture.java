/*
 * SPDX-FileCopyrightText: 2013-2019 Chimaine, MobTalker2 contributors
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */
package net.mobtalker.mobtalker2.common.network.message;

import static net.minecraftforge.fml.relauncher.Side.*;
import io.netty.buffer.ByteBuf;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;
import net.minecraftforge.fml.relauncher.SideOnly;
import net.mobtalker.mobtalker2.client.network.ClientMessageHandler;

public class MessageHideTexture extends ServerToClientMessage
{
    private String _group;
    
    // ========================================
    
    public String getGroup()
    {
        return _group;
    }
    
    public MessageHideTexture setGroup( String group )
    {
        _group = group;
        return this;
    }
    
    // ========================================
    
    @Override
    public void fromBytes( ByteBuf buf )
    {
        _group = readString( buf );
    }
    
    @Override
    public void toBytes( ByteBuf buf )
    {
        writeString( buf, _group );
    }
    
    // ========================================
    
    public static class Handler extends ClientMessageHandler.Delegator<MessageHideTexture>
    {
        @Override
        public ClientToServerMessage onMessage( MessageHideTexture message, MessageContext ctx )
        {
            ClientMessageHandler.instance().onHideTexture( message );
            return null;
        }
    }
}
