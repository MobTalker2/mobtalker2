/*
 * SPDX-FileCopyrightText: 2013-2019 Chimaine, MobTalker2 contributors
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */
package net.mobtalker.mobtalker2.common.network.message;

import io.netty.buffer.ByteBuf;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;
import net.mobtalker.mobtalker2.common.network.ServerMessageHandler;

public class MessageCancelInteraction extends ClientToServerMessage
{
    @Override
    public void fromBytes( ByteBuf buf )
    {}
    
    @Override
    public void toBytes( ByteBuf buf )
    {}
    
    // ========================================
    
    public static class Handler extends ServerMessageHandler.Delegator<MessageCancelInteraction>
    {
        @Override
        public ServerToClientMessage onMessage( MessageCancelInteraction message, MessageContext ctx )
        {
            ServerMessageHandler.instance().onCancelInteraction( ctx.getServerHandler().playerEntity, message );
            return null;
        }
    }
}
