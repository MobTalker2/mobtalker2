/*
 * SPDX-FileCopyrightText: 2013-2019 Chimaine, MobTalker2 contributors
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */
package net.mobtalker.mobtalker2.common.network.message;

import static net.minecraftforge.fml.relauncher.Side.*;
import io.netty.buffer.ByteBuf;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;
import net.minecraftforge.fml.relauncher.SideOnly;
import net.mobtalker.mobtalker2.client.network.ClientMessageHandler;

public class MessageGuiConfig extends ServerToClientMessage
{
    private String _config;
    
    // ========================================
    
    public String getConfig()
    {
        return _config;
    }
    
    public MessageGuiConfig setConfig( String config )
    {
        _config = config;
        return this;
    }
    
    // ========================================
    
    @Override
    public void fromBytes( ByteBuf buf )
    {
        _config = readString( buf );
    }
    
    @Override
    public void toBytes( ByteBuf buf )
    {
        writeString( buf, _config );
    }
    
    // ========================================
    
    public static class Handler extends ClientMessageHandler.Delegator<MessageGuiConfig>
    {
        @Override
        public ClientToServerMessage onMessage( MessageGuiConfig message, MessageContext ctx )
        {
            if ( !checkThread( message, ctx ) )
                return null;
            
            ClientMessageHandler.instance().onGuiConfig( message );
            return null;
        }
    }
}