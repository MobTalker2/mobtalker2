/*
 * SPDX-FileCopyrightText: 2013-2019 Chimaine, MobTalker2 contributors
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */
package net.mobtalker.mobtalker2.common.network.message;

import io.netty.buffer.ByteBuf;

import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.mobtalker.mobtalker2.common.MobTalkerConfig;

public abstract class MobTalkerMessage implements IMessage
{
    public static void writeString( ByteBuf buf, String str )
    {
        byte[] bytes = str.getBytes( MobTalkerConfig.CHARSET );
        buf.writeShort( bytes.length );
        buf.writeBytes( bytes );
    }
    
    public static String readString( ByteBuf buf )
    {
        int length = buf.readUnsignedShort();
        return new String( buf.readBytes( length ).array(), MobTalkerConfig.CHARSET );
    }
}
