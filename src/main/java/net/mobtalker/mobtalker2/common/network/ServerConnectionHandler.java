/*
 * SPDX-FileCopyrightText: 2013-2019 Chimaine, MobTalker2 contributors
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */
package net.mobtalker.mobtalker2.common.network;

import static net.mobtalker.mobtalker2.util.ChatMessageHelper.sendMessageToEntity;

import java.util.*;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.EnumChatFormatting;
import net.minecraftforge.fml.common.FMLCommonHandler;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.gameevent.PlayerEvent.PlayerLoggedInEvent;
import net.mobtalker.mobtalker2.MobTalker2;
import net.mobtalker.mobtalker2.server.resources.scriptpack.*;
import net.mobtalker.mobtalker2.server.script.ScriptFactory;
import net.mobtalker.mobtalker2.util.ChatMessageHelper;
import net.mobtalker.mobtalkerscript.v3.compiler.MtsSyntaxError;

public class ServerConnectionHandler
{
    private static final ServerConnectionHandler instance = new ServerConnectionHandler();
    
    public static void register()
    {
        FMLCommonHandler.instance().bus().register( instance );
    }
    
    // ========================================
    
    @SubscribeEvent
    public void onPlayerConnected( PlayerLoggedInEvent event )
    {
        EntityPlayer player = event.player;
        MobTalker2.LOG.trace( "Player connected: %s", player.getDisplayName() );
        
        printNoScriptsWarning( player );
        printScriptPackWarnings( player );
        printScriptErrorsToPlayer( player );
    }
    
    private void printNoScriptsWarning( EntityPlayer player )
    {
        if ( !ScriptPackRepository.isLoaded() )
            return;
        
        if ( !ScriptPackRepository.instance().hasScripts() )
        {
            ChatMessageHelper.sendMessageToEntity( player,
                                                   "[MobTalker2] This server has no interaction scripts.",
                                                   EnumChatFormatting.RED );
        }
    }
    
    private void printScriptPackWarnings( EntityPlayer player )
    {
        if ( !ScriptPackRepository.isLoaded() )
            return;
        
        List<String> warnings = ScriptPackRepository.instance().getWarnings();
        if ( !warnings.isEmpty() )
        {
            ChatMessageHelper.sendMessageToEntity( player,
                                                   "[MobTalker2] Some script packs contain errors",
                                                   EnumChatFormatting.RED );
            
            for ( String warning : warnings )
            {
                ChatMessageHelper.sendMessageToEntity( player, warning, EnumChatFormatting.RED );
            }
        }
    }
    
    private void printScriptErrorsToPlayer( EntityPlayer player )
    {
        if ( !ScriptFactory.isLoaded() )
            return;
        
        Map<IScriptFile, MtsSyntaxError> syntaxErrors = ScriptFactory.instance().getSyntaxErrors();
        if ( !syntaxErrors.isEmpty() )
        {
            ChatMessageHelper.sendMessageToEntity( player,
                                                   "[MobTalker2] Some interaction scripts contain syntax errors",
                                                   EnumChatFormatting.RED );
            
            for ( MtsSyntaxError syntaxError : syntaxErrors.values() )
            {
                ChatMessageHelper.sendMessageToEntity( player, syntaxError.getMessage(), EnumChatFormatting.RED );
            }
        }
    }
    
    // private void printVersionCheckResult( EntityPlayer player )
    // {
    // if ( VersionChecker.newerVersionAvailable() )
    // {
    // ComparableVersion newestVersion = VersionChecker.getNewestVersion();
    //
    // sendMessageToPlayer( player, //
    // "A newer version of MobTalker2 is available ("
    // + newestVersion.toString()
    // + "), consider updating.",
    // EnumChatFormatting.WHITE );
    // }
    // }
}
