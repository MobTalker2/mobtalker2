/*
 * SPDX-FileCopyrightText: 2013-2019 Chimaine, MobTalker2 contributors
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */
package net.mobtalker.mobtalker2.common.network.message;

import static net.minecraftforge.fml.relauncher.Side.*;
import io.netty.buffer.ByteBuf;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;
import net.minecraftforge.fml.relauncher.SideOnly;
import net.mobtalker.mobtalker2.client.network.ClientMessageHandler;

public class MessageShowScene extends ServerToClientMessage
{
    private String _path;
    private String _mode;
    
    // ========================================
    
    public String getPath()
    {
        return _path;
    }
    
    public String getMode()
    {
        return _mode;
    }
    
    public MessageShowScene setPath( String path )
    {
        _path = path;
        return this;
    }
    
    public MessageShowScene setMode( String mode )
    {
        _mode = mode;
        return this;
    }
    
    // ========================================
    
    @Override
    public void fromBytes( ByteBuf buf )
    {
        _path = readString( buf );
        _mode = readString( buf );
    }
    
    @Override
    public void toBytes( ByteBuf buf )
    {
        writeString( buf, _path );
        writeString( buf, _mode );
    }
    
    // ========================================
    
    public static class Handler extends ClientMessageHandler.Delegator<MessageShowScene>
    {
        @Override
        public ClientToServerMessage onMessage( MessageShowScene message, MessageContext ctx )
        {
            if ( !checkThread( message, ctx ) )
                return null;
            
            ClientMessageHandler.instance().onShowScene( message );
            return null;
        }
    }
}
