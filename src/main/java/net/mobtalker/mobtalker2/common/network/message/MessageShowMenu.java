/*
 * SPDX-FileCopyrightText: 2013-2019 Chimaine, MobTalker2 contributors
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */
package net.mobtalker.mobtalker2.common.network.message;

import static net.minecraftforge.fml.relauncher.Side.*;
import io.netty.buffer.ByteBuf;

import java.util.List;

import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;
import net.minecraftforge.fml.relauncher.SideOnly;
import net.mobtalker.mobtalker2.client.network.ClientMessageHandler;

import com.google.common.collect.Lists;

public class MessageShowMenu extends RemoteCallMessage
{
    private String _caption;
    private List<String> _choices;
    
    // ========================================
    
    public String getCaption()
    {
        return _caption;
    }
    
    public List<String> getChoices()
    {
        return _choices;
    }
    
    public MessageShowMenu setCaption( String caption )
    {
        _caption = caption;
        return this;
    }
    
    public MessageShowMenu setChoices( List<String> choices )
    {
        _choices = choices;
        return this;
    }
    
    // ========================================
    
    @Override
    public void fromBytes( ByteBuf buf )
    {
        _caption = readString( buf );
        
        int nChoices = buf.readUnsignedByte();
        _choices = Lists.newArrayListWithExpectedSize( nChoices );
        for ( int i = 0; i < nChoices; i++ )
        {
            _choices.add( readString( buf ) );
        }
    }
    
    @Override
    public void toBytes( ByteBuf buf )
    {
        writeString( buf, _caption );
        
        buf.writeByte( _choices.size() );
        for ( String choice : _choices )
        {
            writeString( buf, choice );
        }
    }
    
    @Override
    public Class<? extends ClientToServerMessage> getResponseType()
    {
        return MessageShowMenuResponse.class;
    }
    
    // ========================================
    
    public static class Handler extends ClientMessageHandler.Delegator<MessageShowMenu>
    {
        @Override
        public ClientToServerMessage onMessage( MessageShowMenu message, MessageContext ctx )
        {
            if ( !checkThread( message, ctx ) )
                return null;
            
            ClientMessageHandler.instance().onShowMenu( message );
            return null;
        }
    }
}
