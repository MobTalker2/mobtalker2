/*
 * SPDX-FileCopyrightText: 2013-2019 Chimaine, MobTalker2 contributors
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */
package net.mobtalker.mobtalker2.common.network;

public final class MessageIDs
{
    public static final int InteractionStart = 1;
    public static final int InteractionCancel = 2;
    
    public static final int ShowText = 4;
    public static final int ShowTextResponse = -ShowText;
    
    public static final int ShowMenu = 5;
    public static final int ShowMenuResponse = -ShowMenu;
    
    public static final int ShowSprite = 6;
    
    public static final int ShowScene = 7;
    
    public static final int HideTexture = 8;
    
    public static final int GuiConfig = 9;
    
    // ========================================
    
    private MessageIDs()
    {}
}
