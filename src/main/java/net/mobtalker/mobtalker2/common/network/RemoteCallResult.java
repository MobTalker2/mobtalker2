/*
 * SPDX-FileCopyrightText: 2013-2019 Chimaine, MobTalker2 contributors
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */
package net.mobtalker.mobtalker2.common.network;

import net.mobtalker.mobtalker2.common.network.message.ClientToServerMessage;

import static com.google.common.base.Preconditions.*;

import java.util.concurrent.*;
import java.util.concurrent.locks.*;

class RemoteCallResult implements Future<ClientToServerMessage>
{
    private final Lock _lock;
    private final Condition _doneCondition;
    
    private boolean _isDone;
    private boolean _isCancelled;
    
    private ClientToServerMessage _result;
    private ExecutionException _exception;
    
    private final Class<? extends ClientToServerMessage> _responseType;
    
    // ========================================
    
    public RemoteCallResult( Class<? extends ClientToServerMessage> responseType )
    {
        _lock = new ReentrantLock();
        _doneCondition = _lock.newCondition();
        _responseType = responseType;
    }
    
    // ========================================
    
    public Class<? extends ClientToServerMessage> getResponseType()
    {
        return _responseType;
    }
    
    @Override
    public boolean isCancelled()
    {
        _lock.lock();
        try
        {
            return _isCancelled;
        }
        finally
        {
            _lock.unlock();
        }
    }
    
    @Override
    public boolean isDone()
    {
        _lock.lock();
        try
        {
            return _isDone;
        }
        finally
        {
            _lock.unlock();
        }
    }
    
    // ========================================
    
    void set( ClientToServerMessage result, Exception ex )
    {
        _lock.lock();
        try
        {
            checkState( !_isDone, "result is already set" );
            
            _result = result;
            _exception = ( ex != null ) ? new ExecutionException( ex ) : null;
            
            _isDone = true;
            
            _doneCondition.signalAll();
        }
        finally
        {
            _lock.unlock();
        }
    }
    
    @Override
    public ClientToServerMessage get()
        throws InterruptedException, ExecutionException
    {
        _lock.lock();
        try
        {
            while ( !_isDone )
                _doneCondition.await();
            
            if ( isCancelled() )
                throw new CancellationException();
            
            if ( _exception != null )
                throw _exception;
            
            return _result;
        }
        finally
        {
            _lock.unlock();
        }
    }
    
    @Override
    public ClientToServerMessage get( long timeout, TimeUnit unit )
        throws InterruptedException, ExecutionException, TimeoutException
    {
        _lock.lock();
        try
        {
            if ( !_isDone && !_doneCondition.await( timeout, unit ) )
                throw new TimeoutException();
            
            if ( isCancelled() )
                throw new CancellationException();
            
            if ( _exception != null )
                throw _exception;
            
            return _result;
        }
        finally
        {
            _lock.unlock();
        }
    }
    
    @Override
    public boolean cancel( boolean mayInterruptIfRunning )
    {
        if ( !mayInterruptIfRunning )
            return false;
        
        _lock.lock();
        try
        {
            if ( isDone() )
                return false;
            if ( isCancelled() )
                return true;
            
            _isCancelled = true;
            _isDone = true;
            
            _doneCondition.signalAll();
            
            return true;
        }
        finally
        {
            _lock.unlock();
        }
    }
}
