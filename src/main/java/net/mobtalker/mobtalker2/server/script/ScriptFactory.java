/*
 * SPDX-FileCopyrightText: 2013-2019 Chimaine, MobTalker2 contributors
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */
package net.mobtalker.mobtalker2.server.script;

import static com.google.common.base.Preconditions.checkState;
import static net.mobtalker.mobtalker2.util.CollectionUtil.addIf;
import static net.mobtalker.mobtalkerscript.v3.value.MtsMetaMethods.*;
import static net.mobtalker.mobtalkerscript.v3.value.userdata.MtsNatives.createLibrary;

import java.nio.file.Path;
import java.util.*;
import java.util.Map.Entry;
import java.util.concurrent.*;

import com.google.common.base.Throwables;
import com.google.common.collect.*;

import net.mobtalker.mobtalker2.MobTalker2;
import net.mobtalker.mobtalker2.common.entity.EntityType;
import net.mobtalker.mobtalker2.server.interaction.*;
import net.mobtalker.mobtalker2.server.resources.scriptpack.*;
import net.mobtalker.mobtalker2.server.script.lib.*;
import net.mobtalker.mobtalker2.server.script.values.MtsVector;
import net.mobtalker.mobtalker2.util.*;
import net.mobtalker.mobtalker2.util.logging.*;
import net.mobtalker.mobtalkerscript.v3.*;
import net.mobtalker.mobtalkerscript.v3.compiler.MtsSyntaxError;
import net.mobtalker.mobtalkerscript.v3.value.*;

import org.apache.logging.log4j.Level;

public class ScriptFactory
{
    private static final MobTalkerLog LOG = new MobTalkerLog( "ScriptFactory" );
    private static final MobTalkerLog SCRIPT_LOG = new MobTalkerLog( "Script" );
    
    // ========================================
    
    private static ScriptFactory instance;
    
    public static ScriptFactory instance()
    {
        checkState( isLoaded(), "No ScriptFactory is currently loaded" );
        return instance;
    }
    
    public static void load()
    {
        checkState( !isLoaded(), "Already loaded" );
        
        LOG.debug( "Loading" );
        instance = new ScriptFactory();
    }
    
    public static void unload()
    {
        checkState( isLoaded(), "Already unloaded" );
        
        LOG.debug( "Unloading" );
        instance = null;
    }
    
    public static boolean isLoaded()
    {
        checkState( MinecraftUtil.isServer() );
        
        return instance != null;
    }
    
    // ========================================
    
    private CompilerService _compilerService;
    private final Map<IScriptFile, MtsFunctionPrototype> _prototypes;
    private final Map<IScriptFile, MtsSyntaxError> _syntaxErrors;
    
    /**
     * Globals shared by all scripts
     */
    private final MtsGlobals _globals;
    
    /**
     * MT protecting the globals
     */
    private final MtsTable _metatable;
    
    // ========================================
    
    private ScriptFactory()
    {
        _prototypes = Maps.newHashMap();
        _syntaxErrors = Maps.newHashMap();
        
        _globals = createGlobals();
        _metatable = createMetatable( _globals );
    }
    
    // ========================================
    
    private static MtsGlobals createGlobals()
    {
        MtsGlobals G = new MtsGlobals();
        
        G.out = new MobTalkerLogPrintStream( SCRIPT_LOG, Level.INFO, System.out );
        G.err = new MobTalkerLogPrintStream( SCRIPT_LOG, Level.ERROR, System.err );
        
        G.set( "MT2_VERSION", MtsString.of( MobTalker2.VERSION ) );
        G.set( "MT2_API_VERSION", MtsString.of( MobTalker2.SCRIPT_API_VERSION.toString() ) );
        
        // Ensure that `require` calls load files from .minecraft/mobtalker2/scripts
        Path basePath = PathUtil.getMobTalkerPath( "scripts" );
        G.PackageLib.setBasePath( basePath.toString() );
        LOG.debug( "Set base path of package lib to '%s'", basePath );
        
        return G;
    }
    
    public static List<String> getAvailableMinecraftCommands()
    {
        List<String> commands = Lists.newArrayList();
        
        addIf( commands, "clone", CommandUtil::commandExists );
        addIf( commands, "fill", CommandUtil::commandExists );
        addIf( commands, "particle", CommandUtil::commandExists );
        addIf( commands, "playsound", CommandUtil::commandExists );
        addIf( commands, "setblock", CommandUtil::commandExists );
        addIf( commands, "summon", CommandUtil::commandExists );
        addIf( commands, "tellraw", CommandUtil::commandExists );
        addIf( commands, "time", CommandUtil::commandExists );
        addIf( commands, "title", CommandUtil::commandExists );
        addIf( commands, "tp", CommandUtil::commandExists );
        addIf( commands, "trigger", CommandUtil::commandExists );
        addIf( commands, "weather", CommandUtil::commandExists );
        
        return commands;
    }
    
    private MtsTable createScriptEnvironment( IInteractionAdapter adapter )
    {
        MtsTable env = new MtsTable();
        {
            createLibrary( env, new InteractionCommandLib( adapter ) );
        }
        {
            MtsTable lib = new MtsTable( 0, 0 );
            createLibrary( lib, CharacterLib.class );
            createLibrary( lib, new CharacterInteractionLib( adapter ) );
            env.set( "Character", lib );
        }
        {
            MtsTable lib = CommandLib.create( adapter, getAvailableMinecraftCommands() );
            // createLibrary( lib, CommandLib.class );
            env.set( "Command", lib );
        }
        {
            MtsTable lib = new MtsTable( 0, 0 );
            createLibrary( lib, new ScoreboardLib( adapter ) );
            env.set( "Scoreboard", lib );
        }
        {
            MtsTable lib = new MtsTable( 0, 0 );
            createLibrary( lib, WorldLib.class );
            createLibrary( lib, new WorldInteractionLib( adapter ) );
            env.set( "World", lib );
        }
        {
            env.set( "Material", createLibrary( MaterialLib.class ) );
        }
        {
            env.set( "Vector", createLibrary( MtsVector.NativeFunctions.class ) );
        }
        
        env.set( "_G", env );
        env.setMetaTable( _metatable );
        
        return env;
    }
    
    private static MtsTable createMetatable( MtsGlobals globals )
    {
        MtsTable t = new MtsTable();
        t.set( __index, globals );
        
        // No one should have access to our globals.
        t.set( __metatable, MtsBoolean.True );
        
        return t;
    }
    
    // ========================================
    
    public void beginPrecompile()
    {
        LOG.debug( "Precompiling interaction scripts" );
        
        _compilerService = new CompilerService();
        for ( InteractionScriptEntry entry : ScriptPackRepository.instance().getActiveInteractionScripts() )
        {
            List<IScriptFile> files = entry.getFiles();
            assert !files.isEmpty();
            
            for ( IScriptFile file : files )
            {
                _compilerService.compile( file );
            }
        }
    }
    
    public void finishPrecompile()
        throws InterruptedException
    {
        LOG.debug( "Waiting for precompilation to finish..." );
        _compilerService.shutdown( 30L, TimeUnit.SECONDS );
        
        Map<IScriptFile, Future<MtsFunctionPrototype>> futures = _compilerService.getFutures();
        for ( Entry<IScriptFile, Future<MtsFunctionPrototype>> entry : futures.entrySet() )
        {
            IScriptFile file = entry.getKey();
            MtsFunctionPrototype prototype;
            
            try
            {
                prototype = entry.getValue().get();
            }
            catch ( InterruptedException | CancellationException ex )
            {
                LOG.error( "Compilation of '%s' was cancelled", file.getSourceName() );
                continue;
            }
            catch ( ExecutionException ex )
            {
                Throwable cause = ex.getCause();
                if ( cause instanceof MtsSyntaxError )
                {
                    _syntaxErrors.put( file, (MtsSyntaxError) cause );
                    continue;
                }
                
                throw Throwables.propagate( cause );
            }
            
            _prototypes.put( file, prototype );
        }
        
        _compilerService = null;
        LOG.debug( "Precompilation finished" );
    }
    
    // ========================================
    
    private List<MtsFunctionPrototype> getPrototypesFor( ScriptPackEntry entry )
    {
        List<IScriptFile> files = entry.getFiles();
        assert !files.isEmpty();
        
        List<MtsFunctionPrototype> prototypes = new ArrayList<>( files.size() );
        for ( IScriptFile file : files )
        {
            rethrowIfSyntaxError( file );
            
            MtsFunctionPrototype prototype = _prototypes.get( file );
            assert prototype != null;
            
            prototypes.add( prototype );
        }
        
        return prototypes;
    }
    
    private static List<MtsClosure> createClosures( List<MtsFunctionPrototype> prototypes, MtsTable env )
    {
        List<MtsClosure> closures = new ArrayList<>( prototypes.size() );
        for ( MtsFunctionPrototype prototype : prototypes )
        {
            closures.add( new MtsClosure( prototype, env ) );
        }
        return closures;
    }
    
    public InteractionScript createInstance( IInteractionAdapter adapter, InteractionScriptEntry entry )
    {
        List<MtsFunctionPrototype> prototypes = getPrototypesFor( entry );
        
        MtsTable env = createScriptEnvironment( adapter );
        List<MtsClosure> closures = createClosures( prototypes, env );
        
        EventLib eventLib = new EventLib();
        env.set( "Events", createLibrary( eventLib ) );
        
        return new InteractionScript( adapter, entry, env, closures, eventLib );
    }
    
    public InteractionScript createInstance( IInteractionAdapter adapter, String scriptName )
    {
        InteractionScriptEntry entry = ScriptPackRepository.instance().getScriptForName( scriptName );
        if ( entry == null )
            throw new MissingScriptException( scriptName );
        
        return createInstance( adapter, entry );
    }
    
    public InteractionScript createInstance( IInteractionAdapter adapter, EntityType entityType )
    {
        InteractionScriptEntry entry = ScriptPackRepository.instance().getScriptForType( entityType );
        if ( entry == null )
            throw new MissingScriptException( adapter.getScriptName() );
        
        return createInstance( adapter, entry );
    }
    
    // ========================================
    
    private void rethrowIfSyntaxError( IScriptFile file )
    {
        if ( _syntaxErrors.containsKey( file ) )
            throw _syntaxErrors.get( file );
    }
    
    public Map<IScriptFile, MtsSyntaxError> getSyntaxErrors()
    {
        return Collections.unmodifiableMap( _syntaxErrors );
    }
}
