/*
 * SPDX-FileCopyrightText: 2013-2019 Chimaine, MobTalker2 contributors
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */
package net.mobtalker.mobtalker2.server.script.lib;

import static net.mobtalker.mobtalkerscript.v3.MtsCheck.checkString;

import net.mobtalker.mobtalker2.common.entity.EntityType;
import net.mobtalker.mobtalker2.server.script.lib.metatables.Metatables;
import net.mobtalker.mobtalker2.server.script.values.MtsSimpleCharacter;
import net.mobtalker.mobtalkerscript.v3.value.MtsValue;
import net.mobtalker.mobtalkerscript.v3.value.userdata.MtsNativeFunction;

public class CharacterLib
{
    /**
     * Creates a new character which is not bound to a specific entity.
     * 
     * <p>
     * This <i>native function</i> can be used in scripts by calling {@code Character.Create(...)}.
     * </p>
     * 
     * @param argName name of the new character
     * @param argTexturePath base texture path of the new character; if nil, defaults to {@code argName}
     * @return a new {@linkplain MtsSimpleCharacter simple character}
     * @see <a href="http://www.mobtalker.net/wiki/Script_API/Libraries/Character/Create">MobTalker2 Wiki</a>
     */
    @MtsNativeFunction
    public static MtsValue create( MtsValue argName, MtsValue argTexturePath )
    {
        String name = checkString( argName, 0 );
        String texturePath = checkString( argTexturePath, 1, name );
        
        return new MtsSimpleCharacter( Metatables.get( EntityType.All ), name, texturePath );
    }
}
