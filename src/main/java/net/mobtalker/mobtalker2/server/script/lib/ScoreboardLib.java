/*
 * SPDX-FileCopyrightText: 2013-2019 Chimaine, MobTalker2 contributors
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */
package net.mobtalker.mobtalker2.server.script.lib;

import static net.mobtalker.mobtalker2.server.script.lib.ScriptApiConstants.*;
import static net.mobtalker.mobtalkerscript.v3.MtsCheck.*;

import java.util.List;

import com.google.common.base.Strings;

import net.mobtalker.mobtalker2.server.interaction.IInteractionAdapter;
import net.mobtalker.mobtalker2.util.*;
import net.mobtalker.mobtalkerscript.v3.MtsArgumentException;
import net.mobtalker.mobtalkerscript.v3.value.*;
import net.mobtalker.mobtalkerscript.v3.value.userdata.MtsNativeFunction;

/**
 * @deprecated as of 0.7.2. Will be removed in the next version. Please use
 * {@link net.mobtalker.mobtalker2.server.script.values.MtsScoreboard MtsScoreboard} instead.
 */
@Deprecated
public class ScoreboardLib
{
    private final IInteractionAdapter adapter;
    
    public ScoreboardLib( IInteractionAdapter adapter )
    {
        this.adapter = adapter;
    }
    
    private int getDimension()
    {
        return adapter.getEntity().dimension;
    }
    
    /**
     * Sets the objective which should be displayed in a given display slot.
     *
     * <p>
     * This <i>native function</i> can be used in scripts by calling {@code Scoreboard.SetDisplayedObjective(...)}.
     * </p>
     *
     * @param argSlotName name of the slot where the objective should be displayed
     * @param argObjectiveName name of the objective to be displayed
     * @see <a href="http://www.mobtalker.net/wiki/Script_API/Libraries/Scoreboard/SetDisplayedObjective">MobTalker2 Wiki</a>
     */
    @MtsNativeFunction
    public void setDisplayedObjective( MtsValue argSlotName, MtsValue argObjectiveName )
    {
        String slot = checkString( argSlotName, 0 );
        String objective = checkString( argObjectiveName, 1 );
        
        if ( !ScoreboardWrapper.checkObjectiveDisplaySlot( slot ) )
            throw new MtsArgumentException( 0, "invalid display slot '%s'", slot );
        if ( !ScoreboardWrapper.hasObjective( getDimension(), objective ) )
            throw new MtsArgumentException( 1, "unknown objective '%s'", objective );
        
        ScoreboardWrapper.setDisplayedObjective( getDimension(), slot, objective );
    }
    
    /**
     * Clears the objective that was displayed in a given display slot.
     *
     * <p>
     * This <i>native function</i> can be used in scripts by calling {@code Scoreboard.SetDisplayedObjective(...)}.
     * </p>
     *
     * @param argSlotName name of the slot which should be cleared
     * @see <a href="http://www.mobtalker.net/wiki/Script_API/Libraries/Scoreboard/ClearDisplayedObjective">MobTalker2 Wiki</a>
     */
    @MtsNativeFunction
    public void clearDisplayedObjective( MtsValue argSlotName )
    {
        String slot = checkString( argSlotName, 0 );
        
        if ( !ScoreboardWrapper.checkObjectiveDisplaySlot( slot ) )
            throw new MtsArgumentException( 0, "invalid display slot '%s'", slot );
        
        ScoreboardWrapper.clearDisplayedObjective( getDimension(), slot );
    }
    
    // ========================================
    // Objectives
    
    /**
     * Returns all available objectives.
     *
     * <p>
     * This <i>native function</i> can be used in scripts by calling {@code Scoreboard.GetObjectives()}.
     * </p>
     *
     * @return a table listing key-value pairs of objectives and their respective criteria
     * @see <a href="http://www.mobtalker.net/wiki/Script_API/Libraries/Scoreboard/GetObjectives">MobTalker2 Wiki</a>
     */
    @MtsNativeFunction
    public MtsTable getObjectives()
    {
        List<ScoreboardObjectiveInfo> objectives = ScoreboardWrapper.getObjectives( getDimension() );
        
        MtsTable result = new MtsTable( 0, objectives.size() );
        for ( ScoreboardObjectiveInfo objective : objectives )
        {
            result.set( MtsString.of( objective.Name ), MtsString.of( objective.Criteria ) );
        }
        
        return result;
    }
    
    /**
     * Checks if a certain objective exists.
     *
     * <p>
     * This <i>native function</i> can be used in scripts by calling {@code Scoreboard.HasObjective()}.
     * </p>
     *
     * @param argName name of the objective to check for
     * @return {@link MtsBoolean#True true} if the scoreboard contains an objective with the given name, {@link MtsBoolean#False false} otherwise.
     * @see <a href="http://www.mobtalker.net/wiki/Script_API/Libraries/Scoreboard/HasObjective">MobTalker2 Wiki</a>
     */
    @MtsNativeFunction
    public MtsBoolean hasObjective( MtsValue argName )
    {
        return MtsBoolean.of( ScoreboardWrapper.hasObjective( getDimension(), checkString( argName, 0 ) ) );
    }
    
    /**
     * Adds an objective to the scoreboard.
     *
     * <p>
     * This <i>native function</i> can be used in scripts by calling {@code Scoreboard.AddObjective(...)}.
     * </p>
     *
     * @param argName name of the new objective; must not be nil, empty or longer than 16 characters
     * @param argCriteraName name of the criteria that should be used
     * @param argDisplayName display name of the new objective; must not be longer than 32 characters
     * @see <a href="http://www.mobtalker.net/wiki/Script_API/Libraries/Scoreboard/AddObjective">MobTalker2 Wiki</a>
     */
    @MtsNativeFunction
    public void addObjective( MtsValue argName, MtsValue argCriteraName, MtsValue argDisplayName )
    {
        String name = checkString( argName, 0 );
        String criteria = checkString( argCriteraName, 1 );
        String displayName = checkString( argDisplayName, 2, null );
        
        if ( Strings.isNullOrEmpty( name ) )
            throw new MtsArgumentException( 0, "name is empty" );
        if ( name.length() > 16 )
            throw new MtsArgumentException( 0, "name must be no longer than 16 characters" );
        if ( ScoreboardWrapper.hasObjective( getDimension(), name ) )
            throw new MtsArgumentException( 0, "objective '%s' already exists", name );
        if ( !ScoreboardWrapper.hasCriteria( criteria ) )
            throw new MtsArgumentException( 1, "unknown criteria '%s'", criteria );
        if ( !Strings.isNullOrEmpty( displayName ) && ( displayName.length() > 32 ) )
            throw new MtsArgumentException( 2, "display name must be no longer than 32 characters" );
        
        ScoreboardWrapper.addObjective( getDimension(), name, criteria, displayName );
    }
    
    /**
     * Removes an existing objective from the scoreboard.
     *
     * <p>
     * This <i>native function</i> can be used in scripts by calling {@code Scoreboard.RemoveObjective(...)}.
     * </p>
     *
     * @param argName name of the objective that is to be removed
     * @see <a href="http://www.mobtalker.net/wiki/Script_API/Libraries/Scoreboard/RemoveObjective">MobTalker2 Wiki</a>
     */
    @MtsNativeFunction
    public void removeObjective( MtsValue argName )
    {
        String name = checkString( argName, 0 );
        
        if ( !ScoreboardWrapper.hasObjective( getDimension(), name ) )
            throw new MtsArgumentException( 0, "unknown objective '%s'", name );
        
        ScoreboardWrapper.removeObjective( getDimension(), name );
    }
    
    // ========================================
    // Players
    
    /**
     * Returns a list of all tracked players of the scoreboard.
     *
     * <p>
     * This <i>native function</i> can be used in scripts by calling {@code Scoreboard.GetPlayers()}.
     * </p>
     *
     * @return A table containing all player names of the scoreboard.
     * @see <a href="http://www.mobtalker.net/wiki/Script_API/Libraries/Scoreboard/GetPlayers">MobTalker2 Wiki</a>
     */
    @MtsNativeFunction
    public MtsTable getPlayers()
    {
        List<String> names = ScoreboardWrapper.getPlayerNames( getDimension() );
        
        MtsTable result = new MtsTable( names.size(), 0 );
        for ( String name : names )
        {
            result.list().add( MtsString.of( name ) );
        }
        
        return result;
    }
    
    /**
     * Returns the scores of a player on all objectives.
     *
     * <p>
     * This <i>native function</i> can be used in scripts by calling {@code Scoreboard.GetScores(...)}.
     * </p>
     *
     * @param argPlayerName name of the player whose scores are to be retrieved
     * @return A table listing key-value pairs of objectives and the respective scores of the player.
     * @see <a href="http://www.mobtalker.net/wiki/Script_API/Libraries/Scoreboard/GetScores">MobTalker2 Wiki</a>
     */
    @MtsNativeFunction
    public MtsTable getScores( MtsValue argPlayerName )
    {
        List<ScoreboardScoreInfo> scores = ScoreboardWrapper.getPlayerScores( getDimension(),
                                                                              checkString( argPlayerName, 0 ) );
        
        MtsTable result = new MtsTable( 0, scores.size() );
        for ( ScoreboardScoreInfo score : scores )
        {
            result.set( MtsString.of( score.Objective.Name ), MtsNumber.of( score.Score ) );
        }
        
        return result;
    }
    
    /**
     * Returns the score of a player on a given objective.
     *
     * <p>
     * This <i>native function</i> can be used in scripts by calling {@code Scoreboard.GetScore(...)}.
     * </p>
     *
     * @param argPlayerName name of the player whose score is to be retrieved
     * @param argObjectiveName name of the objective of which the player's score is to be retrieved
     * @return The score of the player on that objective.
     * @see <a href="http://www.mobtalker.net/wiki/Script_API/Libraries/Scoreboard/GetScore">MobTalker2 Wiki</a>
     */
    @MtsNativeFunction
    public MtsNumber getScore( MtsValue argPlayerName, MtsValue argObjectiveName )
    {
        String player = checkString( argPlayerName, 0 );
        String objective = checkString( argObjectiveName, 1 );
        
        if ( !ScoreboardWrapper.hasObjective( getDimension(), objective ) )
            throw new MtsArgumentException( 1, "unknown objective '%s'", objective );
        
        return MtsNumber.of( ScoreboardWrapper.getPlayerScore( getDimension(), player, objective ).Score );
    }
    
    /**
     * Sets the score of a player on a given objective.
     *
     * <p>
     * This <i>native function</i> can be used in scripts by calling {@code Scoreboard.SetScore(...)}.
     * </p>
     *
     * @param argPlayerName name of the player whose score is to be set
     * @param argObjectiveName name of the objective of which the player's score is to be set
     * @param argValue score that should be set
     * @see <a href="http://www.mobtalker.net/wiki/Script_API/Libraries/Scoreboard/SetScore">MobTalker2 Wiki</a>
     */
    @MtsNativeFunction
    public void setScore( MtsValue argPlayerName, MtsValue argObjectiveName, MtsValue argValue )
    {
        String objective = checkString( argObjectiveName, 1 );
        if ( !ScoreboardWrapper.hasObjective( getDimension(), objective ) )
            throw new MtsArgumentException( 1, "unknown objective '%s'", objective );
        
        ScoreboardWrapper.setPlayerScore( getDimension(), checkString( argPlayerName, 0 ),
                                          objective,
                                          checkInteger( argValue, 2 ) );
    }
    
    /**
     * Increases the score of a player on a given objective by a certain amount.
     *
     * <p>
     * This <i>native function</i> can be used in scripts by calling {@code Scoreboard.IncreaseScore(...)}.
     * </p>
     *
     * @param argPlayerName name of the player whose score is to be increased
     * @param argObjectiveName name of the objective of which the player's score is to be increased
     * @param argValue amount by which the score should be increased; if nil, defaults to {@code 1}
     * @return The player's score on the objective after it was increased.
     * @see <a href="http://www.mobtalker.net/wiki/Script_API/Libraries/Scoreboard/IncreaseScore">MobTalker2 Wiki</a>
     */
    @MtsNativeFunction
    public MtsNumber increaseScore( MtsValue argPlayerName, MtsValue argObjectiveName, MtsValue argValue )
    {
        String objective = checkString( argObjectiveName, 1 );
        if ( !ScoreboardWrapper.hasObjective( getDimension(), objective ) )
            throw new MtsArgumentException( 1, "unknown objective '%s'", objective );
        
        return MtsNumber.of( ScoreboardWrapper.increasePlayerScore( getDimension(), checkString( argPlayerName, 0 ),
                                                                    objective,
                                                                    checkIntegerWithMinimum( argValue, 2, 0, 1 ) ) );
    }
    
    /**
     * Decreases the score of a player on a given objective by a certain amount.
     *
     * <p>
     * This <i>native function</i> can be used in scripts by calling {@code Scoreboard.DecreaseScore(...)}.
     * </p>
     *
     * @param argPlayerName name of the player whose score is to be decreased
     * @param argObjectiveName name of the objective of which the player's score is to be decreased
     * @param argValue amount by which the score should be decreased; if nil, defaults to {@code 1}
     * @return The player's score on the objective after it was decreased
     * @see <a href="http://www.mobtalker.net/wiki/Script_API/Libraries/Scoreboard/DecreaseScore">MobTalker2 Wiki</a>
     */
    @MtsNativeFunction
    public MtsNumber decreaseScore( MtsValue argPlayerName, MtsValue argObjectiveName, MtsValue argValue )
    {
        String objective = checkString( argObjectiveName, 1 );
        if ( !ScoreboardWrapper.hasObjective( getDimension(), objective ) )
            throw new MtsArgumentException( 1, "unknown objective '%s'", objective );
        
        return MtsNumber.of( ScoreboardWrapper.decreasePlayerScore( getDimension(), checkString( argPlayerName, 0 ),
                                                                    objective,
                                                                    checkIntegerWithMinimum( argValue, 2, 0, 1 ) ) );
    }
    
    // ========================================
    // Teams
    
    /**
     * Returns information on all tracked teams of the scoreboard.
     *
     * <p>
     * This <i>native function</i> can be used in scripts by calling {@code Scoreboard.GetTeams()}.
     * </p>
     *
     * @return A table listing key-value pairs of all team names and detailed information on the respective team's settings.
     * @see <a href="http://www.mobtalker.net/wiki/Script_API/Libraries/Scoreboard/GetTeams">MobTalker2 Wiki</a>
     */
    @MtsNativeFunction
    public MtsTable getTeams()
    {
        List<ScoreboardTeamInfo> teams = ScoreboardWrapper.getTeamInfos( getDimension() );
        
        MtsTable result = new MtsTable( 0, teams.size() );
        for ( ScoreboardTeamInfo team : teams )
        {
            MtsTable info = new MtsTable( 0, 3 );
            info.set( KEY_SCOREBOARD_TEAM_COLOR, MtsString.of( team.Color ) );
            info.set( KEY_SCOREBOARD_TEAM_FRIENDLYFIRE, MtsBoolean.of( team.FriendlyFire ) );
            info.set( KEY_SCOREBOARD_TEAM_SEEFIRENDLYINVISIBLES, MtsBoolean.of( team.CanSeeInvisibleMembers ) );
            result.set( team.Name, info );
        }
        
        return result;
    }
    
    /**
     * Returns a list of all players on a team.
     *
     * <p>
     * This <i>native function</i> can be used in scripts by calling {@code Scoreboard.GetTeamMembers(...)}.
     * </p>
     *
     * @param argTeamName name of the team whose members should be returned
     * @return A table listing the player names of all members on the given team.
     * @see <a href="http://www.mobtalker.net/wiki/Script_API/Libraries/Scoreboard/GetTeamMembers">MobTalker2 Wiki</a>
     */
    @MtsNativeFunction
    public MtsTable getTeamMembers( MtsValue argTeamName )
    {
        String teamName = checkString( argTeamName, 0 );
        
        if ( !ScoreboardWrapper.hasTeam( teamName ) )
            throw new MtsArgumentException( 0, "unknown team '%s'", teamName );
        
        List<String> players = ScoreboardWrapper.getTeamMembers( getDimension(), teamName );
        
        MtsTable result = new MtsTable( players.size(), 0 );
        for ( String player : players )
        {
            result.list().add( MtsString.of( player ) );
        }
        
        return result;
    }
    
    /**
     * Adds a team to the scoreboard.
     *
     * <p>
     * This <i>native function</i> can be used in scripts by calling {@code Scoreboard.AddTeam(...)}.
     * </p>
     *
     * @param argTeamName name of the new team; must not be nil, empty or longer than 16 characters
     * @param argDisplayName display name of the new team; must not be longer than 32 characters
     * @see <a href="http://www.mobtalker.net/wiki/Script_API/Libraries/Scoreboard/AddTeam">MobTalker2 Wiki</a>
     */
    @MtsNativeFunction
    public void addTeam( MtsValue argTeamName, MtsValue argDisplayName )
    {
        String name = checkString( argTeamName, 0 );
        String displayName = checkString( argDisplayName, 1, null );
        
        if ( ScoreboardWrapper.hasTeam( getDimension(), name ) )
            throw new MtsArgumentException( 0, "team '%s' already exists", name );
        if ( Strings.isNullOrEmpty( name ) )
            throw new MtsArgumentException( 0, "empty team name" );
        if ( name.length() > 16 )
            throw new MtsArgumentException( 0, "name must not be longer than 16 characters", name );
        if ( ( displayName != null ) && ( displayName.length() > 32 ) )
            throw new MtsArgumentException( 1, "display name must not be longer than 32 characters", displayName );
        
        ScoreboardWrapper.addTeam( getDimension(), name, displayName );
    }
    
    /**
     * Removes an existing objective from the scoreboard.
     *
     * <p>
     * This <i>native function</i> can be used in scripts by calling {@code Scoreboard.RemoveTeam(...)}.
     * </p>
     *
     * @param argTeamName name of the team that is to be removed
     * @see <a href="http://www.mobtalker.net/wiki/Script_API/Libraries/Scoreboard/RemoveTeam">MobTalker2 Wiki</a>
     */
    @MtsNativeFunction
    public void removeTeam( MtsValue argTeamName )
    {
        String name = checkString( argTeamName, 0 );
        
        if ( !ScoreboardWrapper.hasTeam( getDimension(), name ) )
            throw new MtsArgumentException( 0, "unknown team '%s'", name );
        
        ScoreboardWrapper.removeTeam( getDimension(), name );
    }
    
    /**
     * Adds a player as a member to a scoreboard team.
     *
     * <p>
     * This <i>native function</i> can be used in scripts by calling {@code Scoreboard.AddTeamMember(...)}.
     * </p>
     *
     * @param argTeamName name of the team to which the player should be added
     * @param argPlayerName name of the player who should be added as a member
     * @see <a href="http://www.mobtalker.net/wiki/Script_API/Libraries/Scoreboard/AddTeamMember">MobTalker2 Wiki</a>
     */
    @MtsNativeFunction
    public void addTeamMember( MtsValue argTeamName, MtsValue argPlayerName )
    {
        String teamName = checkString( argTeamName, 0 );
        
        if ( !ScoreboardWrapper.hasTeam( getDimension(), teamName ) )
            throw new MtsArgumentException( 0, "unknown team '%s'", teamName );
        
        ScoreboardWrapper.addTeamMember( getDimension(), teamName, checkString( argPlayerName, 1 ) );
    }
    
    /**
     * Removes a player from a scoreboard team.
     *
     * <p>
     * This <i>native function</i> can be used in scripts by calling {@code Scoreboard.RemoveTeamMember(...)}.
     * </p>
     *
     * @param argTeamName name of the team from which the player should be removed
     * @param argPlayerName name of the player who should be removed
     */
    @MtsNativeFunction
    public void removeTeamMember( MtsValue argTeamName, MtsValue argPlayerName )
    {
        String teamName = checkString( argTeamName, 0 );
        
        if ( !ScoreboardWrapper.hasTeam( getDimension(), teamName ) )
            throw new MtsArgumentException( 0, "unknown team '%s'", teamName );
        
        ScoreboardWrapper.removeTeamMember( getDimension(), teamName, checkString( argPlayerName, 1 ) );
    }
}
