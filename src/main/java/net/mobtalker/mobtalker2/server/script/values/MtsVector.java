/*
 * SPDX-FileCopyrightText: 2013-2019 Chimaine, MobTalker2 contributors
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */
package net.mobtalker.mobtalker2.server.script.values;

import static net.mobtalker.mobtalker2.server.script.lib.ScriptApiCheck.checkVector;
import static net.mobtalker.mobtalkerscript.v3.MtsCheck.checkNumber;
import static org.apache.commons.lang3.Validate.notNull;

import java.util.Objects;

import net.minecraft.util.*;
import net.mobtalker.mobtalker2.server.script.lib.metatables.Metatables;
import net.mobtalker.mobtalkerscript.v3.value.*;
import net.mobtalker.mobtalkerscript.v3.value.userdata.MtsNativeFunction;

/**
 * @since 0.7.2
 */
public class MtsVector extends MtsValueWithMetaTable
{
    public static MtsVector of( BlockPos pos )
    {
        return of( pos.getX(), pos.getY(), pos.getZ() );
    }
    
    public static MtsVector of( Vec3 vec )
    {
        return new MtsVector( Metatables.get( TYPE ), vec );
    }
    
    public static MtsVector of( double x, double y, double z )
    {
        return of( new Vec3( x, y, z ) );
    }
    
    public static MtsVector of( double[] coords )
    {
        if ( coords == null || coords.length != 3 ) throw new IllegalArgumentException();
        return of( coords[0], coords[1], coords[2] );
    }
    
    // ========================================
    
    public static final MtsType TYPE = MtsType.forName( "vector" );
    
    // ========================================
    
    private Vec3 _vector;
    
    // ========================================
    
    private MtsVector( MtsTable metatable, Vec3 vector )
    {
        _vector = notNull( vector );
        setMetaTable( notNull( metatable ) );
    }
    
    // ========================================
    
    public BlockPos toBlockPos()
    {
        return new BlockPos( _vector );
    }
    
    public Vec3 toVec3()
    {
        return new Vec3( _vector.xCoord, _vector.yCoord, _vector.zCoord );
    }
    
    // ========================================
    
    public double getX()
    {
        return _vector.xCoord;
    }
    
    public double getY()
    {
        return _vector.yCoord;
    }
    
    public double getZ()
    {
        return _vector.zCoord;
    }
    
    // ========================================
    
    protected MtsVector offset( EnumFacing direction, double amount )
    {
        return add( direction.getFrontOffsetX() * amount,
                    direction.getFrontOffsetY() * amount,
                    direction.getFrontOffsetZ() * amount );
    }
    
    public double length()
    {
        return _vector.lengthVector();
    }
    
    @Override
    public MtsNumber getLength()
    {
        return MtsNumber.of( length() );
    }
    
    public double dotProduct( double x, double y, double z )
    {
        return getX() * x + getY() * y + getZ() * z;
    }
    
    public double dotProduct( MtsVector other )
    {
        return _vector.dotProduct( other._vector );
    }
    
    public MtsVector inverted()
    {
        return of( -getX(), -getY(), -getZ() );
    }
    
    public MtsVector normalized()
    {
        return of( _vector.normalize() );
    }
    
    public MtsVector add( double x, double y, double z )
    {
        return of( _vector.addVector( x, y, z ) );
    }
    
    public MtsVector add( MtsVector other )
    {
        return of( _vector.add( other._vector ) );
    }
    
    public MtsVector subtract( double x, double y, double z )
    {
        return of( _vector.subtract( x, y, z ) );
    }
    
    public MtsVector subtract( MtsVector other )
    {
        return of( _vector.subtract( other._vector ) );
    }
    
    public MtsVector scale( double scalar )
    {
        double x = getX() * scalar;
        double y = getY() * scalar;
        double z = getZ() * scalar;
        return of( x, y, z );
    }
    
    public double squareDistanceTo( double x, double y, double z )
    {
        double _x = getX() - x;
        double _y = getY() - y;
        double _z = getZ() - z;
        return _x * _x + _y * _y + _z * _z;
    }
    
    public double squareDistanceTo( MtsVector other )
    {
        return _vector.squareDistanceTo( other._vector );
    }
    
    public double distanceTo( double x, double y, double z )
    {
        return Math.sqrt( squareDistanceTo( x, y, z ) );
    }
    
    public double distanceTo( MtsVector other )
    {
        return Math.sqrt( squareDistanceTo( other ) );
    }
    
    public MtsVector crossProduct( double x, double y, double z )
    {
        double xNew = getY() * z - getZ() * y;
        double yNew = getZ() * x - getX() * z;
        double zNew = getX() * y - getY() * x;
        return of( xNew, yNew, zNew );
    }
    
    public MtsVector crossProduct( MtsVector other )
    {
        return of( _vector.crossProduct( other._vector ) );
    }
    
    public MtsVector up( double amount )
    {
        return offset( EnumFacing.UP, amount );
    }
    
    public MtsVector down( double amount )
    {
        return offset( EnumFacing.DOWN, amount );
    }
    
    public MtsVector north( double amount )
    {
        return offset( EnumFacing.NORTH, amount );
    }
    
    public MtsVector east( double amount )
    {
        return offset( EnumFacing.EAST, amount );
    }
    
    public MtsVector south( double amount )
    {
        return offset( EnumFacing.SOUTH, amount );
    }
    
    public MtsVector west( double amount )
    {
        return offset( EnumFacing.WEST, amount );
    }
    
    // ========================================
    
    @Override
    public MtsType getType()
    {
        return TYPE;
    }
    
    // ========================================
    
    @Override
    protected MtsVector multiplyWith( MtsNumber a )
    {
        return scale( a.toJavaDouble() );
    }
    
    @Override
    public MtsVector unaryMinus()
    {
        return inverted();
    }
    
    @Override
    public MtsVector add( MtsValue b )
    {
        if ( b.isTable() ) return add( checkVector( b, 1, true ) );
        if ( b.isVarArgs() ) return add( checkVector( (MtsVarargs) b, 1, true ) );
        
        return add( checkVector( b, 1 ) );
    }
    
    @Override
    public MtsVector substract( MtsValue b )
    {
        if ( b.isTable() ) return add( checkVector( b, 1, true ) );
        if ( b.isVarArgs() ) return add( checkVector( (MtsVarargs) b, 1, true ) );
        return subtract( checkVector( b, 1 ) );
    }
    
    @Override
    public MtsVector multiplyBy( MtsValue b )
    {
        return scale( checkNumber( b ) );
    }
    
    @Override
    public MtsValue divideBy( MtsValue b )
    {
        return scale( 1 / checkNumber( b ) );
    }
    
    // ========================================
    
    @Override
    public int hashCode()
    {
        return Objects.hashCode( _vector );
    }
    
    @Override
    public boolean equals( Object obj )
    {
        if ( this == obj ) return true;
        if ( obj == null ) return false;
        if ( !( obj instanceof MtsVector ) ) return false;
        
        MtsVector other = (MtsVector) obj;
        return Objects.equals( _vector, other._vector );
    }
    
    // ========================================
    
    @Override
    public String toString()
    {
        return String.format( "%s[%.2f,%.2f,%.2f]", getType().getName(), getX(), getY(), getZ() );
    }
    
    // ========================================
    
    /**
     * <p>
     * TODO <a href="https://www.mobtalker.net/wiki/Script_API/Libraries/Vector">wiki article</a>
     * </p>
     *
     * @see <a href="https://www.mobtalker.net/wiki/Script_API/Libraries/Vector">MobTalker2 wiki</a>
     */
    public static class NativeFunctions
    {
        /**
         * <p>
         * TODO <a href="https://www.mobtalker.net/wiki/Script_API/Libraries/Vector/New">wiki article</a>
         * </p>
         *
         * Creates a new vector.
         *
         * <p>
         * This <i>function</i> can be used in scripts by calling {@code Vector.New(...)}.
         * </p>
         *
         * <b>NOTE:</b> If the argument of this function is already a value of type {@code Vector}, no new vector will
         * be created and the argument is returned instead.
         *
         * @param args a varargs argument for which the allowed argument types are:
         * <ul>
         * <li><code>&lt;vector&gt;</code></li>
         * <li><code>&lt;table&gt;</code></li>
         * <li><code>&lt;number&gt; &lt;number&gt; &lt;number&gt;</code></li>
         * </ul>
         * @return a new vector which represents the coordinates described by the given arguments
         * @see <a href="https://www.mobtalker.net/wiki/Script_API/Types/Vector/New">MobTalker2 wiki</a>
         */
        @MtsNativeFunction( "New" )
        public static MtsVector create( MtsVarargs args )
        {
            return checkVector( args, 0, true );
        }
    }
    
    // ========================================
    
    public static class Methods
    {
        /**
         * <p>
         * TODO <a href="https://www.mobtalker.net/wiki/Script_API/Types/Vector/GetX">wiki article</a>
         * </p>
         *
         * Returns a vector's X component.
         *
         * <p>
         * This <i>method</i> can be used in scripts by appending {@code :GetX()} to a {@code Vector} value.
         * </p>
         *
         * @param argVector vector whose X component should be returned
         * @return The X component of the given vector.
         * @see #x(MtsValue)
         * @see <a href="https://www.mobtalker.net/wiki/Script_API/Types/Vector/GetX">MobTalker2 wiki</a>
         */
        @MtsNativeFunction
        public static MtsNumber getX( MtsValue argVector )
        {
            return MtsNumber.of( checkVector( argVector, 0 ).getX() );
        }
        
        /**
         * <p>
         * TODO <a href="https://www.mobtalker.net/wiki/Script_API/Types/Vector/GetX">wiki article</a>
         * </p>
         *
         * Returns a vector's X component. <i>Alias for {@link #getX(MtsValue) GetX}.</i>
         *
         * <p>
         * This <i>method</i> can be used in scripts by appending {@code :X()} to a {@code Vector} value.
         * </p>
         *
         * @param argVector vector whose X component should be returned
         * @return the X component of the given vector
         * @see #getX(MtsValue)
         * @see <a href="https://www.mobtalker.net/wiki/Script_API/Types/Vector/GetX">MobTalker2 wiki</a>
         */
        @MtsNativeFunction
        public static MtsNumber x( MtsValue argVector )
        {
            return getX( argVector );
        }
        
        /**
         * <p>
         * TODO <a href="https://www.mobtalker.net/wiki/Script_API/Types/Vector/GetY">wiki article</a>
         * </p>
         *
         * Returns a vector's Y component.
         *
         * <p>
         * This <i>method</i> can be used in scripts by appending {@code :GetY()} to a {@code Vector} value.
         * </p>
         *
         * @param argVector vector whose Y component should be returned
         * @return the Y component of the given vector
         * @see #y(MtsValue)
         * @see <a href="https://www.mobtalker.net/wiki/Script_API/Types/Vector/GetY">MobTalker2 wiki</a>
         */
        @MtsNativeFunction
        public static MtsNumber getY( MtsValue argVector )
        {
            return MtsNumber.of( checkVector( argVector, 0 ).getY() );
        }
        
        /**
         * <p>
         * TODO <a href="https://www.mobtalker.net/wiki/Script_API/Types/Vector/GetY">wiki article</a>
         * </p>
         *
         * Returns a vector's Y component. <i>Alias for {@link #getY(MtsValue) GetY}.</i>
         *
         * <p>
         * This <i>method</i> can be used in scripts by appending {@code :Y()} to a {@code Vector} value.
         * </p>
         *
         * @param argVector vector whose Y component should be returned
         * @return the Y component of the given vector
         * @see #getY(MtsValue)
         * @see <a href="https://www.mobtalker.net/wiki/Script_API/Types/Vector/GetY">MobTalker2 wiki</a>
         */
        @MtsNativeFunction
        public static MtsNumber y( MtsValue argVector )
        {
            return getY( argVector );
        }
        
        /**
         * <p>
         * TODO <a href="https://www.mobtalker.net/wiki/Script_API/Types/Vector/GetZ">wiki article</a>
         * </p>
         *
         * Returns a vector's Z component.
         *
         * <p>
         * This <i>method</i> can be used in scripts by appending {@code :GetZ()} to a {@code Vector} value.
         * </p>
         *
         * @param argVector vector whose Z component should be returned
         * @return the Z component of the given vector
         * @see #z(MtsValue)
         * @see <a href="https://www.mobtalker.net/wiki/Script_API/Types/Vector/GetZ">MobTalker2 wiki</a>
         */
        @MtsNativeFunction
        public static MtsNumber getZ( MtsValue argVector )
        {
            return MtsNumber.of( checkVector( argVector, 0 ).getZ() );
        }
        
        /**
         * <p>
         * TODO <a href="https://www.mobtalker.net/wiki/Script_API/Types/Vector/GetZ">wiki article</a>
         * </p>
         *
         * Returns a vector's Z component. <i>Alias for {@link #getZ(MtsValue) GetZ}.</i>
         *
         * <p>
         * This <i>method</i> can be used in scripts by appending {@code :Z()} to a {@code Vector} value.
         * </p>
         *
         * @param argVector vector whose Z component should be returned
         * @return the Z component of the given vector
         * @see #getZ(MtsValue)
         * @see <a href="https://www.mobtalker.net/wiki/Script_API/Types/Vector/GetZ">MobTalker2 wiki</a>
         */
        @MtsNativeFunction
        public static MtsNumber z( MtsValue argVector )
        {
            return getZ( argVector );
        }
        
        // ========================================
        
        /**
         * <p>
         * TODO <a href="https://www.mobtalker.net/wiki/Script_API/Types/Vector/Inverted">wiki article</a>
         * </p>
         *
         * Returns the inverse of a vector.
         *
         * <p>
         * This <i>method</i> can be used in scripts by appending {@code :Inverted()} to a {@code Vector} value.
         * </p>
         *
         * @param argVector vector whose inverse should be returned
         * @return the inverse of the given vector
         * @see <a href="https://www.mobtalker.net/wiki/Script_API/Types/Vector/Inverted">MobTalker2 wiki</a>
         */
        @MtsNativeFunction
        public static MtsVector inverted( MtsValue argVector )
        {
            return checkVector( argVector, 0 ).inverted();
        }
        
        /**
         * <p>
         * TODO <a href="https://www.mobtalker.net/wiki/Script_API/Types/Vector/Normalized">wiki article</a>
         * </p>
         *
         * Returns the unit vector of a vector.
         *
         * <p>
         * This <i>method</i> can be used in scripts by appending {@code :Normalized()} to a {@code Vector} value.
         * </p>
         *
         * @param argVector vector whose unit vector should be returned
         * @return the unit vector of the given vector
         * @see #normalised(MtsVector)
         * @see <a href="https://www.mobtalker.net/wiki/Script_API/Types/Vector/Normalized">MobTalker2 wiki</a>
         */
        @MtsNativeFunction
        public static MtsVector normalized( MtsValue argVector )
        {
            return checkVector( argVector, 0 ).normalized();
        }
        
        /**
         * <p>
         * TODO <a href="https://www.mobtalker.net/wiki/Script_API/Types/Vector/Normalized">wiki article</a>
         * </p>
         *
         * Returns the unit vector of a vector. <i>Alias for {@link #normalized(MtsValue) Normalized}.</i>
         *
         * <p>
         * This <i>method</i> can be used in scripts by appending {@code :Normalised()} to a {@code Vector} value.
         * </p>
         *
         * @param argVector vector whose unit vector should be returned
         * @return the unit vector of the given vector
         * @see #normalized(MtsValue)
         * @see <a href="https://www.mobtalker.net/wiki/Script_API/Types/Vector/Normalized">MobTalker2 wiki</a>
         */
        @MtsNativeFunction
        public static MtsVector normalised( MtsVector argVector )
        {
            return normalized( argVector );
        }
        
        /**
         * <p>
         * TODO <a href="https://www.mobtalker.net/wiki/Script_API/Types/Vector/Length">wiki article</a>
         * </p>
         *
         * Returns a vector's Euclidean length. <i>Alias for {@code #vector}.</i>
         *
         * <p>
         * This <i>method</i> can be used in scripts by appending {@code :Length()} to a {@code Vector} value.
         * </p>
         *
         * @param argVector vector whose length should be returned
         * @return the length of the given vector
         * @see MtsValue#getLength()
         * @see <a href="https://www.mobtalker.net/wiki/Script_API/Types/Vector/Length">MobTalker2 wiki</a>
         */
        @MtsNativeFunction
        public static MtsNumber length( MtsValue argVector )
        {
            return MtsNumber.of( checkVector( argVector, 0 ).length() );
        }
        
        /**
         * <p>
         * TODO <a href="https://www.mobtalker.net/wiki/Script_API/Types/Vector/DotProduct">wiki article</a>
         * </p>
         *
         * Returns the result of the dot product of two vectors.
         *
         * <p>
         * This <i>method</i> can be used in scripts by appending {@code :DotProduct(...)} to a {@code Vector} value.
         * </p>
         *
         * @param args a varargs argument for which the allowed argument types are:
         * <ul>
         * <li><code>&lt;vector&gt; &lt;vector&gt;</code></li>
         * <li><code>&lt;vector&gt; &lt;table&gt;</code></li>
         * <li><code>&lt;vector&gt; &lt;number&gt; &lt;number&gt; &lt;number&gt;</code></li>
         * </ul>
         * @return the result of the dot product of the two given vectors
         * @see #dot(MtsVarargs)
         * @see <a href="https://www.mobtalker.net/wiki/Script_API/Types/Vector/DotProduct">MobTalker2 wiki</a>
         */
        @MtsNativeFunction
        public static MtsNumber dotProduct( MtsVarargs args )
        {
            return MtsNumber.of( checkVector( args, 0 ).dotProduct( checkVector( args, 1, true ) ) );
        }
        
        /**
         * <p>
         * TODO <a href="https://www.mobtalker.net/wiki/Script_API/Types/Vector/DotProduct">wiki article</a>
         * </p>
         *
         * Returns the result of the dot product of two vectors. <i>Alias for
         * {@link #dotProduct(MtsVarargs) DotProduct}</i>
         *
         * <p>
         * This <i>method</i> can be used in scripts by appending {@code :Dot(...)} to a {@code Vector} value.
         * </p>
         *
         * @param args a varargs argument for which the allowed argument types are:
         * <ul>
         * <li><code>&lt;vector&gt; &lt;vector&gt;</code></li>
         * <li><code>&lt;vector&gt; &lt;blockvector&gt;</code></li>
         * <li><code>&lt;vector&gt; &lt;table&gt;</code></li>
         * <li><code>&lt;vector&gt; &lt;number&gt; &lt;number&gt; &lt;number&gt;</code></li>
         * </ul>
         * @return the result of the dot product of the two given vectors
         * @see #dotProduct(MtsVarargs)
         * @see <a href="https://www.mobtalker.net/wiki/Script_API/Types/Vector/DotProduct">MobTalker2 wiki</a>
         */
        @MtsNativeFunction
        public static MtsNumber dot( MtsVarargs args )
        {
            return dotProduct( args );
        }
        
        /**
         * <p>
         * TODO <a href="https://www.mobtalker.net/wiki/Script_API/Types/Vector/SquareDistanceTo">wiki article</a>
         * </p>
         *
         * Returns the Euclidean Distance between two vectors. If the actual Distance is to be returned,
         * {@link #distanceTo(MtsVarargs) DistanceTo} should be used instead.
         *
         * <p>
         * This <i>method</i> can be used in scripts by appending {@code :SquareDistanceTo(...)} to a {@code Vector} value.
         * </p>
         *
         * @param args a varargs argument for which the allowed argument types are:
         * <ul>
         * <li><code>&lt;vector&gt; &lt;vector&gt;</code></li>
         * <li><code>&lt;vector&gt; &lt;table&gt;</code></li>
         * <li><code>&lt;vector&gt; &lt;number&gt; &lt;number&gt; &lt;number&gt;</code></li>
         * </ul>
         * @return the squared Distance between the two given vectors
         * @see #distanceTo(MtsVarargs)
         * @see <a href="https://www.mobtalker.net/wiki/Script_API/Types/Vector/SquareDistanceTo">MobTalker2 wiki</a>
         */
        @MtsNativeFunction
        public static MtsNumber squareDistanceTo( MtsVarargs args )
        {
            return MtsNumber.of( checkVector( args, 0 ).squareDistanceTo( checkVector( args, 1, true ) ) );
        }
        
        /**
         * <p>
         * TODO <a href="https://www.mobtalker.net/wiki/Script_API/Types/Vector/DistanceTo">wiki article</a>
         * </p>
         *
         * Returns the Distance between two vectors. For a comparison of distances,
         * {@link #squareDistanceTo(MtsVarargs) SquareDistanceTo} should be used instead.
         *
         * <p>
         * This <i>method</i> can be used in scripts by appending {@code :DistanceTo(...)} to a {@code Vector} value.
         * </p>
         *
         * @param args a varargs argument for which the allowed argument types are:
         * <ul>
         * <li><code>&lt;vector&gt; &lt;vector&gt;</code></li>
         * <li><code>&lt;vector&gt; &lt;table&gt;</code></li>
         * <li><code>&lt;vector&gt; &lt;number&gt; &lt;number&gt; &lt;number&gt;</code></li>
         * </ul>
         * @return the Distance between the two given vectors
         * @see #squareDistanceTo(MtsVarargs)
         * @see <a href="https://www.mobtalker.net/wiki/Script_API/Types/Vector/DistanceTo">MobTalker2 wiki</a>
         */
        @MtsNativeFunction
        public static MtsNumber distanceTo( MtsVarargs args )
        {
            return MtsNumber.of( checkVector( args, 0 ).distanceTo( checkVector( args, 1, true ) ) );
        }
        
        /**
         * <p>
         * TODO <a href="https://www.mobtalker.net/wiki/Script_API/Types/Vector/CrossProduct">wiki article</a>
         * </p>
         *
         * Returns the result of the cross product between two vectors.
         *
         * <p>
         * This <i>method</i> can be used in scripts by appending {@code :CrossProduct(...)} to a {@code Vector} value.
         * </p>
         *
         * @param args a varargs argument for which the allowed argument types are:
         * <ul>
         * <li><code>&lt;vector&gt; &lt;vector&gt;</code></li>
         * <li><code>&lt;vector&gt; &lt;table&gt;</code></li>
         * <li><code>&lt;vector&gt; &lt;number&gt; &lt;number&gt; &lt;number&gt;</code></li>
         * </ul>
         * @return the result of the cross product between the two given vectors
         * @see #cross(MtsVarargs)
         * @see <a href="https://www.mobtalker.net/wiki/Script_API/Types/Vector/CrossProduct">MobTalker2 wiki</a>
         */
        @MtsNativeFunction
        public static MtsVector crossProduct( MtsVarargs args )
        {
            return checkVector( args, 0 ).crossProduct( checkVector( args, 1, true ) );
        }
        
        /**
         * <p>
         * TODO <a href="https://www.mobtalker.net/wiki/Script_API/Types/Vector/CrossProduct">wiki article</a>
         * </p>
         *
         * Returns the result of the cross product between two vectors. <i>Alias for
         * {@link #crossProduct(MtsVarargs) CrossProduct}</i>
         *
         * <p>
         * This <i>method</i> can be used in scripts by appending {@code :Cross(...)} to a {@code Vector} value.
         * </p>
         *
         * @param args a varargs argument for which the allowed argument types are:
         * <ul>
         * <li><code>&lt;vector&gt; &lt;vector&gt;</code></li>
         * <li><code>&lt;vector&gt; &lt;table&gt;</code></li>
         * <li><code>&lt;vector&gt; &lt;number&gt; &lt;number&gt; &lt;number&gt;</code></li>
         * </ul>
         * @return the result of the cross product between the two given vectors
         * @see #crossProduct(MtsVarargs)
         * @see <a href="https://www.mobtalker.net/wiki/Script_API/Types/Vector/CrossProduct">MobTalker2 wiki</a>
         */
        @MtsNativeFunction
        public static MtsVector cross( MtsVarargs args )
        {
            return crossProduct( args );
        }
        
        // ========================================
        
        /**
         * <p>
         * TODO <a href="https://www.mobtalker.net/wiki/Script_API/Types/Vector/Up">wiki article</a>
         * </p>
         *
         * Returns a new vector whose end point is moved up by a certain amount.
         *
         * <p>
         * This <i>method</i> can be used in scripts by appending {@code :Up(...)} to a {@code Vector} value.
         * </p>
         *
         * @param argVector base vector
         * @param argAmount amount of blocks to move the vector end point
         * @return the result of moving the given vector's end point upwards by {@code argAmount}
         * @see <a href="https://www.mobtalker.net/wiki/Script_API/Types/Vector/Up">MobTalker2 wiki</a>
         */
        @MtsNativeFunction
        public static MtsVector up( MtsValue argVector, MtsValue argAmount )
        {
            return checkVector( argVector, 0 ).up( checkNumber( argAmount, 1 ) );
        }
        
        /**
         * <p>
         * TODO <a href="https://www.mobtalker.net/wiki/Script_API/Types/Vector/Down">wiki article</a>
         * </p>
         *
         * Returns a new vector whose end point is moved down by a certain amount.
         *
         * <p>
         * This <i>method</i> can be used in scripts by appending {@code :Down(...)} to a {@code Vector} value.
         * </p>
         *
         * @param argVector base vector
         * @param argAmount amount of blocks to move the vector end point
         * @return the result of moving the given vector's end point downwards by {@code argAmount}
         * @see <a href="https://www.mobtalker.net/wiki/Script_API/Types/Vector/Down">MobTalker2 wiki</a>
         */
        @MtsNativeFunction
        public static MtsVector down( MtsValue argVector, MtsValue argAmount )
        {
            return checkVector( argVector, 0 ).down( checkNumber( argAmount, 1 ) );
        }
        
        /**
         * <p>
         * TODO <a href="https://www.mobtalker.net/wiki/Script_API/Types/Vector/North">wiki article</a>
         * </p>
         *
         * Returns a new vector whose end point is moved north by a certain amount.
         *
         * <p>
         * This <i>method</i> can be used in scripts by appending {@code :North(...)} to a {@code Vector} value.
         * </p>
         *
         * @param argVector base vector
         * @param argAmount amount of blocks to move the vector end point
         * @return the result of moving the given vector's end point north by {@code argAmount}
         * @see <a href="https://www.mobtalker.net/wiki/Script_API/Types/Vector/North">MobTalker2 wiki</a>
         */
        @MtsNativeFunction
        public static MtsVector north( MtsValue argVector, MtsValue argAmount )
        {
            return checkVector( argVector, 0 ).north( checkNumber( argAmount, 1 ) );
        }
        
        /**
         * <p>
         * TODO <a href="https://www.mobtalker.net/wiki/Script_API/Types/Vector/East">wiki article</a>
         * </p>
         *
         * Returns a new vector whose end point is moved east by a certain amount.
         *
         * <p>
         * This <i>method</i> can be used in scripts by appending {@code :East(...)} to a {@code Vector} value.
         * </p>
         *
         * @param argVector base vector
         * @param argAmount amount of blocks to move the vector end point
         * @return the result of moving the given vector's end point east by {@code argAmount}
         * @see <a href="https://www.mobtalker.net/wiki/Script_API/Types/Vector/East">MobTalker2 wiki</a>
         */
        @MtsNativeFunction
        public static MtsVector east( MtsValue argVector, MtsValue argAmount )
        {
            return checkVector( argVector, 0 ).east( checkNumber( argAmount, 1 ) );
        }
        
        /**
         * <p>
         * TODO <a href="https://www.mobtalker.net/wiki/Script_API/Types/Vector/South">wiki article</a>
         * </p>
         *
         * Returns a new vector whose end point is moved south by a certain amount.
         *
         * <p>
         * This <i>method</i> can be used in scripts by appending {@code :South(...)} to a {@code Vector} value.
         * </p>
         *
         * @param argVector base vector
         * @param argAmount amount of blocks to move the vector end point
         * @return the result of moving the given vector's end point south by {@code argAmount}
         * @see <a href="https://www.mobtalker.net/wiki/Script_API/Types/Vector/South">MobTalker2 wiki</a>
         */
        @MtsNativeFunction
        public static MtsVector south( MtsValue argVector, MtsValue argAmount )
        {
            return checkVector( argVector, 0 ).south( checkNumber( argAmount, 1 ) );
        }
        
        /**
         * <p>
         * TODO <a href="https://www.mobtalker.net/wiki/Script_API/Types/Vector/West">wiki article</a>
         * </p>
         *
         * Returns a new vector whose end point is moved west by a certain amount.
         *
         * <p>
         * This <i>method</i> can be used in scripts by appending {@code :West(...)} to a {@code Vector} value.
         * </p>
         *
         * @param argVector base vector
         * @param argAmount amount of blocks to move the vector end point
         * @return the result of moving the given vector's end point west by {@code argAmount}
         * @see <a href="https://www.mobtalker.net/wiki/Script_API/Types/Vector/West">MobTalker2 wiki</a>
         */
        @MtsNativeFunction
        public static MtsVector west( MtsValue argVector, MtsValue argAmount )
        {
            return checkVector( argVector, 0 ).west( checkNumber( argAmount, 1 ) );
        }
    }
}
