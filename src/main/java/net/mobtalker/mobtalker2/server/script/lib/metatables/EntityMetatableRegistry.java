/*
 * SPDX-FileCopyrightText: 2013-2019 Chimaine, MobTalker2 contributors
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */
package net.mobtalker.mobtalker2.server.script.lib.metatables;

import java.util.Set;

import net.mobtalker.mobtalker2.common.entity.EntityType;
import net.mobtalker.mobtalkerscript.v3.value.MtsTable;
import net.mobtalker.mobtalkerscript.v3.value.userdata.MtsNatives;

public class EntityMetatableRegistry extends AbstractMetatableRegistry<EntityType>
{
    public EntityMetatableRegistry()
    {}
    
    // ========================================
    
    @Override
    protected MtsTable build( EntityType key )
    {
        MtsTable lib = new MtsTable( 0, 0 );
        
        // Collect all classes from parents
        do
        {
            Set<Class<?>> libaryClasses = _libraryClasses.get( key );
            if ( libaryClasses.isEmpty() )
                continue;
            
            for ( Class<?> libaryClass : libaryClasses )
            {
                MtsNatives.createLibrary( lib, libaryClass );
            }
        }
        while ( ( key = key.getParent() ) != null );
        
        return lib;
    }
    
    @Override
    public MtsTable get( EntityType key )
    {
        MtsTable result;
        do
        {
            result = super.get( key );
        }
        while ( ( result == null ) && ( ( key = key.getParent() ) != null ) );
        
        return result;
    }
}
