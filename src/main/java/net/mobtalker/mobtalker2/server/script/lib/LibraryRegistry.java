/*
 * SPDX-FileCopyrightText: 2013-2019 Chimaine, MobTalker2 contributors
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */
package net.mobtalker.mobtalker2.server.script.lib;

import java.util.*;

import net.mobtalker.mobtalkerscript.v3.value.MtsTable;
import net.mobtalker.mobtalkerscript.v3.value.userdata.MtsNatives;

public class LibraryRegistry
{
    private static final Map<String, MtsTable> _libraries = new HashMap<>();
    
    // ========================================
    
    static
    {
        mixInto( "character:interaction",
                 CharacterLib.class, CharacterInteractionLib.class );
    }
    
    // ========================================
    
    public static void mixInto( String name, Class<?>... clazzes )
    {
        MtsTable lib = get( name );
        for ( Class<?> clazz : clazzes )
        {
            MtsNatives.createLibrary( lib, clazz );
        }
    }
    
    public static MtsTable get( String name )
    {
        MtsTable lib = _libraries.get( name );
        if ( lib == null )
        {
            lib = new MtsTable( 0, 0 );
            _libraries.put( name, lib );
        }
        
        return lib;
    }
}
