/*
 * SPDX-FileCopyrightText: 2013-2019 Chimaine, MobTalker2 contributors
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */
package net.mobtalker.mobtalker2.server.script.lib;

import static net.mobtalker.mobtalker2.server.script.lib.ScriptApiConstants.*;
import static net.mobtalker.mobtalkerscript.v3.MtsCheck.*;

import java.util.*;
import java.util.function.*;

import net.minecraft.entity.*;
import net.minecraft.util.Vec3;
import net.mobtalker.mobtalker2.MobTalker2;
import net.mobtalker.mobtalker2.common.entity.EntityType;
import net.mobtalker.mobtalker2.server.interaction.*;
import net.mobtalker.mobtalker2.server.script.values.*;
import net.mobtalker.mobtalker2.util.*;
import net.mobtalker.mobtalker2.util.fp.Either;
import net.mobtalker.mobtalkerscript.v3.*;
import net.mobtalker.mobtalkerscript.v3.value.*;

public class ScriptApiCheck
{
    protected static double[] getCoordinatesFromTable( MtsTable tbl )
    {
        if ( tbl.containsKey( KEY_VECTOR_X ) && tbl.containsKey( KEY_VECTOR_Y ) && tbl.containsKey( KEY_VECTOR_Z ) )
        {
            double x = checkNumber( tbl.get( KEY_VECTOR_X ) );
            double y = checkNumber( tbl.get( KEY_VECTOR_Y ) );
            double z = checkNumber( tbl.get( KEY_VECTOR_Z ) );
            return new double[]{ x, y, z };
        }
        
        MtsTableList list = tbl.list();
        double x = checkNumber( list.get( 0 ) );
        double y = checkNumber( list.get( 1 ) );
        double z = checkNumber( list.get( 2 ) );
        return new double[]{ x, y, z };
    }
    
    protected static double[] getXZCoordinatesFromTable( MtsTable tbl )
    {
        if ( tbl.containsKey( KEY_VECTOR_X ) && tbl.containsKey( KEY_VECTOR_Z ) )
        {
            double x = checkNumber( tbl.get( KEY_VECTOR_X ) );
            double z = checkNumber( tbl.get( KEY_VECTOR_Z ) );
            return new double[]{ x, 0, z };
        }
        
        MtsTableList list = tbl.list();
        double x = checkNumber( list.get( 0 ) );
        double z = checkNumber( list.get( 1 ) );
        return new double[]{ x, 0, z };
    }
    
    protected static String getCheckVectorErrorMessage( boolean numbers, boolean characters )
    {
        String message = "expected ";
        List<Object> args = new ArrayList<>( 4 );
        
        // MtsVector
        message += "%s";
        args.add( MtsVector.TYPE );
        
        if ( numbers )
        {
            // MtsTable, MtsNumber
            message += ", %s" + ( characters ? ", " : " or " ) + "%s";
            args.add( MtsType.TABLE );
            args.add( MtsType.NUMBER );
        }
        
        if ( characters )
        {
            // MtsCharacter
            message += " or %s";
            args.add( MtsCharacter.TYPE );
        }
        
        message += ", got %%s";
        
        return String.format( message, args.toArray() );
    }
    
    // ========================================
    
    // TODO add to MobTalkerScript's MtsCheck
    public static boolean checkBoolean( MtsValue value )
    {
        if ( !value.isBoolean() )
            throw new MtsArgumentException( MtsType.BOOLEAN, value.getType() );
        
        return value.isTrue();
    }
    
    public static boolean checkBoolean( MtsValue value, int index )
    {
        if ( !value.isBoolean() )
            throw new MtsArgumentException( index, MtsType.BOOLEAN, value.getType() );
        
        return value.isTrue();
    }
    
    public static boolean checkBoolean( MtsVarargs args, int index )
    {
        return checkBoolean( args.get( index ), index );
    }
    
    // ========================================
    
    public static MtsCharacter checkCharacter( MtsValue value )
    {
        if ( !value.is( MtsCharacter.TYPE ) )
            throw new MtsArgumentException( MtsCharacter.TYPE, value.getType() );
        
        return (MtsCharacter) value;
    }
    
    public static MtsCharacter checkCharacter( MtsValue value, int i )
    {
        if ( !value.is( MtsCharacter.TYPE ) )
            throw new MtsArgumentException( i, MtsCharacter.TYPE, value.getType() );
        
        return (MtsCharacter) value;
    }
    
    public static MtsCharacter checkCharacter( MtsVarargs args, int i )
    {
        return checkCharacter( args.get( i ), i );
    }
    
    // ========================================
    
    public static <T extends EntityLivingBase> T checkEntity( MtsValue value )
    {
        return checkCharacter( value ).getEntity();
    }
    
    public static <T extends EntityLivingBase> T checkEntity( MtsValue value, int i )
    {
        return checkCharacter( value, i ).getEntity();
    }
    
    public static <T extends EntityLivingBase> T checkEntity( MtsVarargs args, int i )
    {
        return checkCharacter( args, i ).getEntity();
    }
    
    // ========================================
    
    public static MtsWorld checkWorld( MtsValue value, int i )
    {
        if ( !value.is( MtsWorld.TYPE ) )
            throw new MtsArgumentException( i, MtsWorld.TYPE, value.getType() );
        
        return (MtsWorld) value;
    }
    
    public static MtsWorld checkWorld( MtsVarargs values, int i )
    {
        return checkWorld( values.get( i ), i );
    }
    
    // ========================================
    
    public static MtsBiome checkBiome( MtsValue value, int i )
    {
        if ( !value.is( MtsBiome.TYPE ) )
            throw new MtsArgumentException( i, MtsBiome.TYPE, value.getType() );
        
        return (MtsBiome) value;
    }
    
    public static MtsBiome checkBiome( MtsVarargs values, int i )
    {
        return checkBiome( values.get( i ), i );
    }
    
    // ========================================
    
    public static IInteractionAdapter checkAdapter( EntityLiving entity, boolean requireInteraction )
    {
        IInteractionAdapter adapter = InteractionRegistry.instance().getAdapter( entity );
        if ( adapter == null )
            throw new MtsRuntimeException( "'%s' is not controlled by %s",
                                           EntityUtil.getName( entity ), MobTalker2.NAME );
        if ( requireInteraction && !adapter.isActive() )
            throw new MtsRuntimeException( "'%s' is not interacting",
                                           EntityUtil.getName( entity ) );
        
        return adapter;
    }
    
    // ========================================
    
    public static String checkItemName( MtsValue arg, int i )
    {
        String name = checkString( arg, i );
        if ( !ItemUtil.isValidItem( name ) )
            throw new MtsArgumentException( i, "unknown item '%s'", name );
        
        return name;
    }
    
    // ========================================
    
    public static EntityType checkEntityType( String arg, int i )
    {
        Either<String, EntityType> type = EntityType.tryForName( arg );
        if ( type.isLeft() )
        {
            throw new MtsArgumentException( i, "'%s' is not a valid entity type: %s", arg, type.left().get() );
        }
        return type.right().get();
    }
    
    // ========================================
    
    public static EntityReaction checkReaction( MtsValue arg, int i )
    {
        String s = checkString( arg, i );
        
        EntityReaction result = EntityReaction.forName( s );
        if ( result == null )
            throw new MtsArgumentException( i, "'%s' is not a valid reaction", s );
        
        return result;
    }
    
    // ========================================
    
    public static MtsBlock checkBlock( MtsValue value, int i )
    {
        if ( !value.is( MtsBlock.TYPE ) )
            throw new MtsArgumentException( i, MtsBlock.TYPE, value.getType() );
        
        return (MtsBlock) value;
    }
    
    public static MtsBlock checkBlock( MtsVarargs value, int i )
    {
        return checkBlock( value.get( i ), i );
    }
    
    // ========================================
    
    public static MtsBlockState checkBlockState( MtsValue value, int i )
    {
        if ( !value.is( MtsBlockState.TYPE ) )
            throw new MtsArgumentException( i, MtsBlockState.TYPE, value.getType() );
        
        return (MtsBlockState) value;
    }
    
    public static MtsBlockState checkBlockState( MtsVarargs args, int i )
    {
        return checkBlockState( args.get( i ), i );
    }
    
    // ========================================
    
    public static MtsMaterial checkMaterial( MtsValue value, int i )
    {
        if ( !value.is( MtsMaterial.TYPE ) )
            throw new MtsArgumentException( i, MtsMaterial.TYPE, value.getType() );
        
        return (MtsMaterial) value;
    }
    
    public static MtsMaterial checkMaterial( MtsVarargs value, int i )
    {
        return checkMaterial( value.get( i ), i );
    }
    
    // ========================================
    
    public static MtsVector checkVector( MtsValue value, int i )
    {
        return checkVector( value, i, false );
    }
    
    public static MtsVector checkVector( MtsValue value, int i, boolean allowNumbers )
    {
        return checkVector( value, i, allowNumbers, false );
    }
    
    public static MtsVector checkVector( MtsValue value, int i, boolean allowNumbers, boolean allowCharacters )
    {
        if ( allowNumbers && value.isTable() ) return MtsVector.of( getCoordinatesFromTable( value.asTable() ) );
        
        if ( allowCharacters && value.is( MtsCharacter.TYPE ) )
            return MtsVector.of( ( (MtsCharacter) value ).getEntity().getPositionVector() );
        
        if ( !value.is( MtsVector.TYPE ) )
        {
            String msg = getCheckVectorErrorMessage( allowNumbers, allowCharacters );
            throw new MtsArgumentException( i, msg, value.getType() );
        }
        
        return (MtsVector) value;
    }
    
    public static MtsVector checkVector( MtsVarargs value, int i )
    {
        return checkVector( value, new Counter( i ), false, false );
    }
    
    public static MtsVector checkVector( MtsVarargs value, int i, boolean allowNumbers )
    {
        return checkVector( value, new Counter( i ), allowNumbers, false );
    }
    
    public static MtsVector checkVector( MtsVarargs value, int i, boolean allowNumbers, boolean allowCharacters )
    {
        return checkVector( value, new Counter( i ), allowNumbers, allowCharacters );
    }
    
    public static MtsVector checkVector( MtsVarargs value, Counter c )
    {
        return checkVector( value, c, true );
    }
    
    public static MtsVector checkVector( MtsVarargs value, Counter c, boolean allowNumbers )
    {
        return checkVector( value, c, allowNumbers, true );
    }
    
    public static MtsVector checkVector( MtsVarargs value, Counter c, boolean allowNumbers, boolean allowCharacters )
    {
        MtsValue val = value.get( c.postIncrement() );
        if ( allowNumbers )
        {
            if ( val.isTable() )
            {
                return MtsVector.of( getCoordinatesFromTable( val.asTable() ) );
            }
            
            if ( val.isNumber() )
            {
                double x = checkNumber( val );
                double y = checkNumber( value, c.postIncrement() );
                double z = checkNumber( value, c.postIncrement() );
                return MtsVector.of( x, y, z );
            }
        }
        
        if ( allowCharacters && val.is( MtsCharacter.TYPE ) )
            return MtsVector.of( ( (MtsCharacter) val ).getEntity().getPositionVector() );
        
        if ( !val.is( MtsVector.TYPE ) )
        {
            String msg = getCheckVectorErrorMessage( allowNumbers, allowCharacters );
            throw new MtsArgumentException( c.preDecrement(), msg, val.getType() );
        }
        
        return (MtsVector) val;
    }
    
    // ========================================
    
    public static MtsVector check2DVector( MtsValue value, int i )
    {
        return check2DVector( value, i, false, false );
    }
    
    public static MtsVector check2DVector( MtsValue value, int i, boolean allowNumbers )
    {
        return check2DVector( value, i, allowNumbers, false );
    }
    
    public static MtsVector check2DVector( MtsValue value, int i, boolean allowNumbers, boolean allowCharacters )
    {
        if ( allowNumbers && value.isTable() ) return MtsVector.of( getXZCoordinatesFromTable( value.asTable() ) );
        
        if ( allowCharacters && value.is( MtsCharacter.TYPE ) )
        {
            Vec3 vector = ( (MtsCharacter) value ).getEntity().getPositionVector();
            return MtsVector.of( vector.xCoord, 0, vector.zCoord );
        }
        
        if ( !value.is( MtsVector.TYPE ) )
        {
            String msg = getCheckVectorErrorMessage( allowNumbers, allowCharacters );
            throw new MtsArgumentException( i, msg, value.getType() );
        }
        
        MtsVector vector = (MtsVector) value;
        return vector.subtract( 0, vector.getY(), 0 );
    }
    
    public static MtsVector check2DVector( MtsVarargs value, int i )
    {
        return check2DVector( value, new Counter( i ), false, false );
    }
    
    public static MtsVector check2DVector( MtsVarargs value, int i, boolean allowNumbers )
    {
        return check2DVector( value, new Counter( i ), allowNumbers, false );
    }
    
    public static MtsVector check2DVector( MtsVarargs value, int i, boolean allowNumbers, boolean allowCharacters )
    {
        return check2DVector( value, new Counter( i ), allowNumbers, allowCharacters );
    }
    
    public static MtsVector check2DVector( MtsVarargs value, Counter c )
    {
        return check2DVector( value, c, true, true );
    }
    
    public static MtsVector check2DVector( MtsVarargs value, Counter c, boolean allowNumbers )
    {
        return check2DVector( value, c, allowNumbers, true );
    }
    
    public static MtsVector check2DVector( MtsVarargs value, Counter c, boolean allowNumbers, boolean allowCharacters )
    {
        MtsValue val = value.get( c.postIncrement() );
        if ( allowNumbers )
        {
            if ( val.isTable() )
            {
                return MtsVector.of( getXZCoordinatesFromTable( val.asTable() ) );
            }
            
            if ( val.isNumber() )
            {
                double x = checkNumber( value, c.value() );
                double z = checkNumber( value, c.postIncrement() );
                return MtsVector.of( x, 0, z );
            }
        }
        
        if ( allowCharacters && val.is( MtsCharacter.TYPE ) )
        {
            Vec3 vector = ( (MtsCharacter) val ).getEntity().getPositionVector();
            return MtsVector.of( vector.xCoord, 0, vector.zCoord );
        }
        
        if ( !val.is( MtsVector.TYPE ) )
        {
            String msg = getCheckVectorErrorMessage( allowNumbers, allowCharacters );
            throw new MtsArgumentException( c.preDecrement(), msg, val.getType() );
        }
        
        MtsVector vector = (MtsVector) val;
        return vector.subtract( 0, vector.getY(), 0 );
    }
    
    // ========================================
    
    public static MtsScoreboard checkScoreboard( MtsValue value )
    {
        if ( !value.is( MtsScoreboard.TYPE ) )
            throw new MtsArgumentException( MtsScoreboard.TYPE, value.getType() );
        
        return (MtsScoreboard) value;
    }
    
    public static MtsScoreboard checkScoreboard( MtsValue value, int index )
    {
        if ( !value.is( MtsScoreboard.TYPE ) )
            throw new MtsArgumentException( index, MtsScoreboard.TYPE, value.getType() );
        
        return (MtsScoreboard) value;
    }
    
    public static MtsScoreboard checkScoreboard( MtsVarargs args, int index )
    {
        return checkScoreboard( args.get( index ), index );
    }
    
    // ========================================
    
    public static MtsTeam checkTeam( MtsValue value )
    {
        if ( !value.is( MtsTeam.TYPE ) )
            throw new MtsArgumentException( MtsTeam.TYPE, value.getType() );
        
        return (MtsTeam) value;
    }
    
    public static MtsTeam checkTeam( MtsValue value, int index )
    {
        if ( !value.is( MtsTeam.TYPE ) )
            throw new MtsArgumentException( index, MtsTeam.TYPE, value.getType() );
        
        return (MtsTeam) value;
    }
    
    public static MtsTeam checkTeam( MtsVarargs args, int index )
    {
        return checkTeam( args.get( index ), index );
    }
    
    // ========================================
    
    public static MtsObjective checkObjective( MtsValue value )
    {
        if ( !value.is( MtsObjective.TYPE ) )
            throw new MtsArgumentException( MtsTeam.TYPE, value.getType() );
        
        return (MtsObjective) value;
    }
    
    public static MtsObjective checkObjective( MtsValue value, int index )
    {
        if ( !value.is( MtsObjective.TYPE ) )
            throw new MtsArgumentException( index, MtsTeam.TYPE, value.getType() );
        
        return (MtsObjective) value;
    }
    
    public static MtsObjective checkObjective( MtsVarargs args, int index )
    {
        return checkObjective( args.get( index ), index );
    }
    
    // ========================================
    
    public static MtsValue checkOption( MtsValue options, int index, String key )
    {
        return checkOption( options, index, MtsString.of( key ) );
    }
    
    public static MtsValue checkOption( MtsValue options, int index, MtsString key )
    {
        if ( options == null || options.isNil() ) return MtsValue.Nil;
        MtsTable table = checkTable( options, index );
        return table.get( key );
    }
    
    public static MtsValue checkOption( MtsVarargs args, int index, String key )
    {
        return checkOption( args, index, MtsString.of( key ) );
    }
    
    public static MtsValue checkOption( MtsVarargs args, int index, MtsString key )
    {
        return checkOption( args.get( index ), index, key );
    }
    
    public static MtsValue checkOptionAndConsume( MtsValue options, int index, String key,
                                                  Consumer<? super MtsValue> action )
    {
        return checkOptionAndConsume( options, index, MtsString.of( key ), action );
    }
    
    public static MtsValue checkOptionAndConsume( MtsValue options, int index, MtsString key,
                                                  Consumer<? super MtsValue> action )
    {
        MtsValue option = checkOption( options, index, key );
        if ( !option.isNil() )
        {
            try
            {
                action.accept( option );
            }
            catch ( MtsRuntimeException e )
            {
                throw new MtsArgumentException( "invalid value for key '%s': %s", key, e.getMessage() );
            }
        }
        return option;
    }
    
    public static MtsValue checkOptionAndConsume( MtsVarargs args, int index, String key,
                                                  Consumer<? super MtsValue> action )
    {
        return checkOptionAndConsume( args, index, MtsString.of( key ), action );
    }
    
    public static MtsValue checkOptionAndConsume( MtsVarargs args, int index, MtsString key,
                                                  Consumer<? super MtsValue> action )
    {
        return checkOptionAndConsume( args.get( index ), index, key, action );
    }
    
    public static <T> T checkOptionAndApply( MtsValue options, int index, String key,
                                             Function<? super MtsValue, ? extends T> func )
    {
        return checkOptionAndApply( options, index, MtsString.of( key ), func );
    }
    
    public static <T> T checkOptionAndApply( MtsValue options, int index, MtsString key,
                                             Function<? super MtsValue, ? extends T> func )
    {
        return checkOptionAndApply( options, index, key, true, func );
    }
    
    public static <T> T checkOptionAndApply( MtsVarargs args, int index, String key,
                                             Function<? super MtsValue, ? extends T> func )
    {
        return checkOptionAndApply( args, index, MtsString.of( key ), func );
    }
    
    public static <T> T checkOptionAndApply( MtsVarargs args, int index, MtsString key,
                                             Function<? super MtsValue, ? extends T> func )
    {
        return checkOptionAndApply( args.get( index ), index, key, func );
    }
    
    public static <T> T checkOptionAndApply( MtsValue options, int index, String key, boolean executeIfNil,
                                             Function<? super MtsValue, ? extends T> func )
    {
        return checkOptionAndApply( options, index, MtsString.of( key ), executeIfNil, func );
    }
    
    public static <T> T checkOptionAndApply( MtsValue options, int index, MtsString key, boolean executeIfNil,
                                             Function<? super MtsValue, ? extends T> func )
    {
        MtsValue option = checkOption( options, index, key );
        if ( !executeIfNil && option.isNil() ) return null;
        try
        {
            return func.apply( option );
        }
        catch ( MtsRuntimeException e )
        {
            throw new MtsArgumentException( "invalid value for key '%s': %s", key, e.getMessage() );
        }
    }
    
    public static <T> T checkOptionAndApply( MtsVarargs args, int index, String key, boolean executeIfNil,
                                             Function<? super MtsValue, ? extends T> func )
    {
        return checkOptionAndApply( args, index, MtsString.of( key ), executeIfNil, func );
    }
    
    public static <T> T checkOptionAndApply( MtsVarargs args, int index, MtsString key, boolean executeIfNil,
                                             Function<? super MtsValue, ? extends T> func )
    {
        return checkOptionAndApply( args.get( index ), index, key, executeIfNil, func );
    }
}
