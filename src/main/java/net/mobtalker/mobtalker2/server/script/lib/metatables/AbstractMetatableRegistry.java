/*
 * SPDX-FileCopyrightText: 2013-2019 Chimaine, MobTalker2 contributors
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */
package net.mobtalker.mobtalker2.server.script.lib.metatables;

import static net.mobtalker.mobtalkerscript.v3.value.MtsMetaMethods.*;

import java.util.*;

import com.google.common.collect.*;

import net.mobtalker.mobtalkerscript.v3.value.*;

public abstract class AbstractMetatableRegistry<T> implements IMetatableRegistry<T>
{
    protected SetMultimap<T, Class<?>> _libraryClasses;
    protected Map<T, MtsTable> _metatables;
    
    // ========================================
    
    protected AbstractMetatableRegistry()
    {
        _libraryClasses = MultimapBuilder.hashKeys().hashSetValues( 1 ).build();
    }
    
    // ========================================
    
    @Override
    public void register( T key, Class<?>... libraries )
    {
        if ( isBuilt() )
            throw new IllegalStateException( "metatable registry was already built" );
        if ( libraries == null )
            return;
        
        Set<Class<?>> libraryClasses = _libraryClasses.get( key );
        libraryClasses.addAll( Arrays.asList( libraries ) );
    }
    
    @Override
    public void build()
    {
        if ( isBuilt() )
            throw new IllegalStateException( "metatable registry was already built" );
        
        _metatables = new HashMap<>();
        
        for ( T key : _libraryClasses.keySet() )
        {
            if ( _metatables.containsKey( key ) )
            {
                continue;
            }
            
            MtsTable metaTable = new MtsTable( 0, 2 );
            metaTable.set( __index, build( key ) );
            metaTable.set( __metatable, MtsBoolean.True );
            _metatables.put( key, metaTable );
        }
        
        _libraryClasses = null;
    }
    
    protected abstract MtsTable build( T key );
    
    @Override
    public boolean isBuilt()
    {
        return _metatables != null;
    }
    
    // ========================================
    
    @Override
    public MtsTable get( T key )
    {
        if ( !isBuilt() )
            throw new IllegalStateException( "metatable registry has not been built yet" );
        
        return _metatables.get( key );
    }
}
