/*
 * SPDX-FileCopyrightText: 2013-2019 Chimaine, MobTalker2 contributors
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */
package net.mobtalker.mobtalker2.server.script.lib.metatables;

import java.util.Set;

import net.mobtalker.mobtalkerscript.v3.value.*;
import net.mobtalker.mobtalkerscript.v3.value.userdata.MtsNatives;

public class TypeMetatableRegistry extends AbstractMetatableRegistry<MtsType>
{
    public TypeMetatableRegistry()
    {
    }
    
    // ========================================
    
    @Override
    protected MtsTable build( MtsType key )
    {
        Set<Class<?>> libraryClasses = _libraryClasses.get( key );
        MtsTable lib = new MtsTable( 0, 0 );
        
        for ( Class<?> libraryClass : libraryClasses )
        {
            MtsNatives.createLibrary( lib, libraryClass );
        }
        
        return lib;
    }
}
