/*
 * SPDX-FileCopyrightText: 2013-2019 Chimaine, MobTalker2 contributors
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */
package net.mobtalker.mobtalker2.server.script.values;

import net.minecraft.block.Block;
import net.minecraft.block.state.IBlockState;
import net.minecraftforge.fml.common.registry.GameData;
import net.mobtalker.mobtalker2.server.script.lib.metatables.Metatables;
import net.mobtalker.mobtalkerscript.v3.value.*;
import net.mobtalker.mobtalkerscript.v3.value.userdata.MtsNativeFunction;

import java.util.Map;
import java.util.WeakHashMap;

import static net.mobtalker.mobtalker2.server.script.lib.ScriptApiCheck.checkMaterial;
import static net.mobtalker.mobtalkerscript.v3.MtsCheck.checkInteger;
import static org.apache.commons.lang3.Validate.notNull;

/**
 * Immutable data structure which is independent from changes made to the world
 */
public class MtsMaterial extends MtsValueWithMetaTable
{
    private static final Map<Block, MtsMaterial> _cache = new WeakHashMap<>( 256 );
    
    public static MtsMaterial of( Block block )
    {
        return _cache.computeIfAbsent( block, b -> new MtsMaterial( Metatables.get( TYPE ), b ) );
    }
    
    // ========================================
    
    public static final MtsType TYPE = MtsType.forName( "material" );
    
    // ========================================
    
    private Block _block;
    
    // ========================================
    
    private MtsMaterial( MtsTable metatable, Block block )
    {
        _block = notNull( block );
        setMetaTable( notNull( metatable ) );
    }
    
    // ========================================
    
    @Deprecated
    public Block getBlock()
    {
        return toBlock();
    }
    
    public Block toBlock()
    {
        return _block;
    }
    
    // ========================================
    
    public String getName()
    {
        return GameData.getBlockRegistry().getNameForObject( toBlock() ).toString();
    }
    
    // ========================================
    
    @Deprecated
    public boolean isValidMetadata( int meta )
    {
        IBlockState state = toBlock().getStateFromMeta( meta );
        return state != null;
    }
    
    // ========================================
    
    public MtsBlockState getDefaultState()
    {
        return MtsBlockState.of( toBlock().getDefaultState() );
    }
    
    // ========================================
    
    @Override
    public MtsType getType()
    {
        return TYPE;
    }
    
    // ========================================
    
    @Override
    public int hashCode()
    {
        return _block.hashCode();
    }
    
    @Override
    public boolean equals( Object o )
    {
        if ( this == o )
        {
            return true;
        }
        if ( o == null )
        {
            return false;
        }
        if ( !( o instanceof MtsMaterial ) )
        {
            return false;
        }
        
        MtsMaterial other = (MtsMaterial) o;
        return other.toBlock().equals( toBlock() );
    }
    
    // ========================================
    
    @Override
    public String toString()
    {
        return TYPE.getName() + "[" + getName() + "]";
    }
    
    // ========================================
    
    public static class Methods
    {
        /**
         * <p>
         * TODO <a href="http://mobtalker.net/wiki/Script_API/Types/Material/GetName">wiki</a>
         * </p>
         *
         * Returns the name of a material.
         *
         * <p>
         * This <i>method</i> can be used in scripts by appending {@code :GetName()} to a {@code Material} value.
         * </p>
         *
         * @param argMaterial material whose name should be get
         *
         * @return The "unlocalized" name of the material.
         * @see <a href="http://mobtalker.net/wiki/Script_API/Types/Material/GetName">MobTalker2 wiki</a>
         */
        @MtsNativeFunction
        public static MtsString getName( MtsValue argMaterial )
        {
            return MtsString.of( checkMaterial( argMaterial, 0 ).getName() );
        }
        
        /**
         * Returns the default {@linkplain MtsBlockState block state} of a material.
         *
         * <p>
         * This <i>method</i> can be used in scripts by appending {@code :GetDefaultState()} to a {@code material} value.
         * </p>
         *
         * @param argMaterial The material for which the default block state should be returned.
         *
         * @return The default block state of the given material.
         * @see <a href="https://mobtalker.net/wiki/Script_API/Types/Material/GetDefaultState">MobTalker2 wiki</a>
         * @since 0.7.2
         */
        @MtsNativeFunction
        public static MtsBlockState getDefaultState( MtsValue argMaterial )
        {
            return checkMaterial( argMaterial, 0 ).getDefaultState();
        }
        
        /**
         * <p>
         * TODO <a href="http://mobtalker.net/wiki/Script_API/Types/Material/IsValidMetadata">wiki</a>
         * </p>
         *
         * Checks whether a metadata value is valid for a material. This only returns false if the block the material is based on returns {@code null} in its
         * {@link Block#getStateFromMeta(int)} method. Because most blocks return a default state instead of {@code null}, that case will be fairly rare.
         *
         * <p>
         * This <i>method</i> can be used in scripts by appending {@code :IsValidMetadata(...)} to a {@code Material} value.
         * </p>
         *
         * @param argMaterial material which should be used
         * @param argMetadata metadata value which should be checked
         *
         * @return {@link MtsBoolean#True true} if the metadata is valid, {@link MtsBoolean#False false} otherwise.
         * @see <a href="http://mobtalker.net/wiki/Script_API/Types/Material/IsValidMetadata">MobTalker2 wiki</a>
         * @deprecated as of 0.7.2.
         */
        @Deprecated
        @MtsNativeFunction
        public static MtsBoolean isValidMetadata( MtsValue argMaterial, MtsValue argMetadata )
        {
            MtsMaterial material = checkMaterial( argMaterial, 0 );
            int meta = checkInteger( argMetadata, 1 );
            return MtsBoolean.of( material.isValidMetadata( meta ) );
        }
        
        /**
         * <p>
         * TODO <a href="http://mobtalker.net/wiki/Script_API/Types/Material/GetHarvestLevel">wiki</a>
         * </p>
         *
         * Returns the harvest level of a material.
         *
         * <p>
         * This <i>method</i> can be used in scripts by appending {@code :GetHarvestLevel()} to a {@code Material} value.
         * </p>
         *
         * @param argMaterial material for which the harvest level should be returned
         *
         * @return The harvest level of the material
         * @see <a href="http://mobtalker.net/wiki/Script_API/Types/Material/GetHarvestLevel">MobTalker2 wiki</a>
         */
        @MtsNativeFunction
        public static MtsNumber getHarvestLevel( MtsValue argMaterial )
        {
            MtsMaterial material = checkMaterial( argMaterial, 0 );
            Block block = material.toBlock();
            return MtsNumber.of( block.getHarvestLevel( block.getDefaultState() ) );
        }
        
        /**
         * <p>
         * TODO <a href="http://mobtalker.net/wiki/Script_API/Types/Material/IsAssociated">wiki</a>
         * </p>
         *
         * Checks whether a material is associated with another material.
         *
         * <p>
         * This <i>method</i> can be used in scripts by appending {@code :IsAssociated(...)} to a {@code Material} value.
         * </p>
         *
         * @param argMaterial base material which determines whether the other material is associated or not
         * @param argOther    material which is to be checked
         *
         * @return {@link MtsBoolean#True true} if the material is associated with the base material, {@link MtsBoolean#False false} otherwise.
         * @see <a href="http://mobtalker.net/wiki/Script_API/Types/Material/IsAssociated">MobTalker2 wiki</a>
         */
        @MtsNativeFunction
        public static MtsBoolean isAssociated( MtsValue argMaterial, MtsValue argOther )
        {
            MtsMaterial material = checkMaterial( argMaterial, 0 );
            MtsMaterial other = checkMaterial( argOther, 1 );
            return MtsBoolean.of( material.toBlock().isAssociatedBlock( other.toBlock() ) );
        }
        
        /**
         * <p>
         * TODO <a href="http://mobtalker.net/wiki/Script_API/Types/Material/IsLiquid">wiki</a>
         * </p>
         *
         * Checks whether a material is a liquid.
         *
         * <p>
         * This <i>method</i> can be used in scripts by appending {@code :IsLiquid()} to a {@code Material} value.
         * </p>
         *
         * @param argMaterial material to be checked
         *
         * @return {@link MtsBoolean#True true} if the material is a liquid, {@link MtsBoolean#False false} otherwise.
         * @see <a href="http://mobtalker.net/wiki/Script_API/Types/Material/IsLiquid">MobTalker2 wiki</a>
         */
        @MtsNativeFunction
        public static MtsBoolean isLiquid( MtsValue argMaterial )
        {
            MtsMaterial material = checkMaterial( argMaterial, 0 );
            return MtsBoolean.of( material.toBlock().getMaterial().isLiquid() );
        }
        
        /**
         * <p>
         * TODO <a href="http://mobtalker.net/wiki/Script_API/Types/Material/IsSolid">wiki</a>
         * </p>
         *
         * Checks whether a material is solid.
         *
         * <p>
         * This <i>method</i> can be used in scripts by appending {@code :IsSolid()} to a {@code Material} value.
         * </p>
         *
         * @param argMaterial material to be checked
         *
         * @return {@link MtsBoolean#True true} if the material is solid, {@link MtsBoolean#False false} otherwise.
         * @see <a href="http://mobtalker.net/wiki/Script_API/Types/Material/IsSolid">MobTalker2 wiki</a>
         */
        @MtsNativeFunction
        public static MtsBoolean isSolid( MtsValue argMaterial )
        {
            MtsMaterial material = checkMaterial( argMaterial, 0 );
            return MtsBoolean.of( material.toBlock().getMaterial().isSolid() );
        }
        
        /**
         * <p>
         * TODO <a href="http://mobtalker.net/wiki/Script_API/Types/Material/IsCollidable">wiki</a>
         * </p>
         *
         * Checks whether a material blocks movement when an entity tries to walk through it.
         *
         * <p>
         * This <i>method</i> can be used in scripts by appending {@code :IsCollidable()} to a {@code Material} value.
         * </p>
         *
         * @param argMaterial material to be checked
         *
         * @return {@link MtsBoolean#True true} if the material can not be walked through, {@link MtsBoolean#False false} otherwise.
         * @see <a href="http://mobtalker.net/wiki/Script_API/Types/Material/IsCollidable">MobTalker2 wiki</a>
         */
        @MtsNativeFunction
        public static MtsBoolean isCollidable( MtsValue argMaterial )
        {
            MtsMaterial material = checkMaterial( argMaterial, 0 );
            return MtsBoolean.of( material.toBlock().isCollidable() );
        }
    }
}
