/*
 * SPDX-FileCopyrightText: 2013-2019 Chimaine, MobTalker2 contributors
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */
package net.mobtalker.mobtalker2.server.script.lib.metatables;

import net.mobtalker.mobtalker2.common.entity.EntityType;
import net.mobtalker.mobtalker2.server.script.values.*;
import net.mobtalker.mobtalkerscript.v3.value.*;

public class Metatables
{
    private static final IMetatableRegistry<String> General;
    private static final IMetatableRegistry<MtsType> Types;
    private static final IMetatableRegistry<EntityType> Entities;
    
    // ========================================
    
    static
    {
        //
        General = new StringMetatableRegistry();
        
        //
        Types = new TypeMetatableRegistry();
        
        Types.register( MtsBiome.TYPE, MtsBiome.Methods.class );
        Types.register( MtsWorld.TYPE, MtsWorld.Methods.class );
        Types.register( MtsBlock.TYPE, MtsBlock.Methods.class );
        Types.register( MtsBlockState.TYPE, MtsBlockState.Methods.class );
        Types.register( MtsMaterial.TYPE, MtsMaterial.Methods.class );
        Types.register( MtsVector.TYPE, MtsVector.Methods.class );
        Types.register( MtsScoreboard.TYPE, MtsScoreboard.Methods.class );
        Types.register( MtsTeam.TYPE, MtsTeam.Methods.class );
        Types.register( MtsObjective.TYPE, MtsObjective.Methods.class );
        
        //
        Entities = new EntityMetatableRegistry();
        
        Entities.register( EntityType.All, MtsCharacter.Methods.class );
        
        Entities.register( EntityType.Creature, MtsCharacter.CreatureMethods.class );
        Entities.register( EntityType.Creeper, MtsCharacter.CreeperMethods.class );
        
        Entities.register( EntityType.Animal );
        
        Entities.register( EntityType.Player, MtsCharacter.PlayerMethods.class );
        Entities.register( EntityType.Villager, MtsCharacter.VillagerMethods.class );
    }
    
    public static void build()
    {
        if ( !General.isBuilt() )
        {
            General.build();
        }
        if ( !Types.isBuilt() )
        {
            Types.build();
        }
        if ( !Entities.isBuilt() )
        {
            Entities.build();
        }
    }
    
    // ========================================
    
    public static void register( String key, Class<?>... libraryClasses )
    {
        General.register( key, libraryClasses );
    }
    
    public static MtsTable get( String key )
    {
        return General.get( key );
    }
    
    // ========================================
    
    public static void register( MtsType type, Class<?>... libraryClasses )
    {
        Types.register( type, libraryClasses );
    }
    
    public static MtsTable get( MtsType key )
    {
        return Types.get( key );
    }
    
    // ========================================
    
    public static void register( EntityType type, Class<?>... libraryClasses )
    {
        Entities.register( type, libraryClasses );
    }
    
    public static MtsTable get( EntityType type )
    {
        return Entities.get( type );
    }
}
