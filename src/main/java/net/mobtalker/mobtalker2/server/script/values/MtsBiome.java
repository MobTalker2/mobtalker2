/*
 * SPDX-FileCopyrightText: 2013-2019 Chimaine, MobTalker2 contributors
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */
package net.mobtalker.mobtalker2.server.script.values;

import com.google.common.base.Strings;
import net.minecraft.world.biome.BiomeGenBase;
import net.minecraftforge.common.BiomeDictionary;
import net.mobtalker.mobtalker2.server.script.lib.metatables.Metatables;
import net.mobtalker.mobtalkerscript.v3.MtsArgumentException;
import net.mobtalker.mobtalkerscript.v3.value.*;
import net.mobtalker.mobtalkerscript.v3.value.userdata.MtsNativeFunction;
import org.apache.commons.lang3.StringUtils;

import java.util.Map;
import java.util.WeakHashMap;
import java.util.function.BiFunction;

import static net.mobtalker.mobtalker2.server.script.lib.ScriptApiCheck.checkBiome;
import static net.mobtalker.mobtalkerscript.v3.MtsCheck.checkNumber;
import static net.mobtalker.mobtalkerscript.v3.MtsCheck.checkString;
import static org.apache.commons.lang3.Validate.notNull;

public class MtsBiome
        extends MtsValueWithMetaTable
{
    private static final BiomeCache _cache = new BiomeCache( 3 );
    
    public static MtsBiome of( MtsWorld world, BiomeGenBase biome )
    {
        return _cache.computeIfAbsent( world, biome, ( w, b ) -> new MtsBiome( Metatables.get( TYPE ), w, b ) );
    }
    
    // ========================================
    
    public static final MtsType TYPE = MtsType.forName( "biome" );
    
    // ========================================
    
    private final MtsWorld _world;
    private final BiomeGenBase _biome;
    
    // ========================================
    
    private MtsBiome( MtsTable metatable, MtsWorld world, BiomeGenBase biome )
    {
        _world = world;
        _biome = biome;
        setMetaTable( metatable );
    }
    
    // ========================================
    
    @Deprecated
    public BiomeGenBase getBiome()
    {
        return toBiome();
    }
    
    public BiomeGenBase toBiome()
    {
        return _biome;
    }
    
    // ========================================
    
    public MtsWorld getWorld()
    {
        return _world;
    }
    
    // ========================================
    
    public String getName()
    {
        return StringUtils.remove( _biome.biomeName, ' ' );
    }
    
    public void setName( String name )
    {
        _biome.setBiomeName( name );
    }
    
    public int getId()
    {
        return _biome.biomeID;
    }
    
    // ========================================
    
    public float getTemperature()
    {
        return _biome.temperature;
    }
    
    public Temperature getTemperatureEnum()
    {
        if ( isNetherBiome() )
        {
            return Temperature.HELLISH;
        }
        
        float t = getTemperature();
        if ( t >= 2.0f )
        {
            return Temperature.HOT;
        }
        if ( t >= 1.2f )
        {
            return Temperature.WARM;
        }
        if ( t >= 0.2f )
        {
            return Temperature.NORMAL;
        }
        if ( t >= 0.05f )
        {
            return Temperature.COLD;
        }
        
        return Temperature.ICY;
    }
    
    public void setTemperature( float temperature )
    {
        _biome.setTemperatureRainfall( temperature, getHumidity() );
    }
    
    public boolean isNetherBiome()
    {
        return BiomeDictionary.isBiomeOfType( _biome, BiomeDictionary.Type.NETHER );
    }
    
    public float getHumidity()
    {
        return _biome.rainfall;
    }
    
    public Humidity getHumidityEnum()
    {
        float h = getHumidity();
        if ( h >= 0.9f )
        {
            return Humidity.DAMP;
        }
        if ( h >= 0.3f )
        {
            return Humidity.NORMAL;
        }
        
        return Humidity.ARID;
    }
    
    public void setHumidity( float humidity )
    {
        _biome.setTemperatureRainfall( getTemperature(), humidity );
    }
    
    // ========================================
    
    public boolean isRaining()
    {
        return _world.toWorld().isRaining() && getHumidity() > 0;
    }
    
    public boolean isThundering()
    {
        return _world.toWorld().isThundering();
    }
    
    public boolean canSnow()
    {
        return _biome.getEnableSnow();
    }
    
    public Weather getWeather()
    {
        if ( isRaining() )
        {
            return canSnow() ? Weather.SNOW : Weather.RAIN;
        }
        return Weather.CLEAR;
    }
    
    // ========================================
    
    @Override
    public MtsType getType()
    {
        return TYPE;
    }
    
    // ========================================
    
    @Override
    public String toString()
    {
        return TYPE.getName() + "[" + StringUtils.remove( _biome.biomeName, ' ' ) + "]";
    }
    
    // ========================================
    
    @Override
    public boolean equals( Object obj )
    {
        if ( obj == null )
        {
            return false;
        }
        if ( obj == this )
        {
            return true;
        }
        if ( !( obj instanceof MtsBiome ) )
        {
            return false;
        }
        
        MtsBiome other = (MtsBiome) obj;
        return other.toBiome().biomeID == toBiome().biomeID;
    }
    
    @Override
    public int hashCode()
    {
        return _biome.biomeID;
    }
    
    // ========================================
    
    /**
     * Weak cache of created biome objects
     */
    private static class BiomeCache
    {
        private final Map<MtsWorld, Map<BiomeGenBase, MtsBiome>> _cache;
        
        public BiomeCache( int worldCount )
        {
            _cache = new WeakHashMap<>( worldCount );
        }
        
        public MtsBiome get( MtsWorld world, BiomeGenBase biome )
        {
            Map<BiomeGenBase, MtsBiome> biomes = _cache.get( world );
            if ( biomes == null )
            {
                return null;
            }
            return biomes.get( biome );
        }
        
        public MtsBiome computeIfAbsent( MtsWorld world, BiomeGenBase biome,
                                         BiFunction<? super MtsWorld, ? super BiomeGenBase, ? extends MtsBiome> mapping )
        {
            notNull( mapping );
            Map<BiomeGenBase, MtsBiome> map = _cache.computeIfAbsent( world, w -> new WeakHashMap<>() );
            MtsBiome value = map.get( biome );
            if ( value == null )
            {
                MtsBiome newValue = mapping.apply( world, biome );
                if ( newValue != null )
                {
                    map.put( biome, newValue );
                    return newValue;
                }
            }
            return value;
        }
    }
    
    // ========================================
    
    public enum Temperature
    {
        ICY( "icy" ),
        COLD( "cold" ),
        NORMAL( "normal" ),
        WARM( "warm" ),
        HOT( "hot" ),
        HELLISH( "hellish" );
        
        private final String _name;
        private final MtsString _nameMts;
        
        Temperature( String name )
        {
            _name = notNull( name );
            _nameMts = MtsString.of( name );
        }
        
        public MtsString toMtsString()
        {
            return _nameMts;
        }
        
        @Override
        public String toString()
        {
            return _name;
        }
    }
    
    // ========================================
    
    public enum Humidity
    {
        DAMP( "damp" ),
        NORMAL( "normal" ),
        ARID( "arid" );
        
        private final String _name;
        private final MtsString _nameMts;
        
        Humidity( String name )
        {
            _name = notNull( name );
            _nameMts = MtsString.of( name );
        }
        
        public MtsString toMtsString()
        {
            return _nameMts;
        }
        
        @Override
        public String toString()
        {
            return _name;
        }
    }
    
    // ========================================
    
    public enum Weather
    {
        RAIN( "rain" ),
        SNOW( "snow" ),
        CLEAR( "clear" );
        
        private final String _name;
        private final MtsString _nameMts;
        
        Weather( String name )
        {
            _name = notNull( name );
            _nameMts = MtsString.of( name );
        }
        
        public MtsString toMtsString()
        {
            return _nameMts;
        }
        
        @Override
        public String toString()
        {
            return _name;
        }
    }
    
    // ========================================
    
    public static class Methods
    {
        /**
         * Returns the name of a biome.
         *
         * <p>
         * This <i>method</i> can be used in scripts by appending {@code :GetName()} to a {@code Biome} value.
         * </p>
         *
         * @param argBiome biome to get the name for
         *
         * @return The biome's name.
         * @see <a href="http://mobtalker.net/wiki/Script_API/Types/Biome/GetName">MobTalker2 wiki</a>
         */
        @MtsNativeFunction
        public static MtsString getName( MtsValue argBiome )
        {
            MtsBiome biome = checkBiome( argBiome, 0 );
            return MtsString.of( biome.getName() );
        }
        
        /**
         * Modifies the name of a biome. If the name to set is {@link MtsValue#Nil nil} or empty, an exception is thrown.
         *
         * <p>
         * This <i>method</i> can be used in scripts by appending {@code :SetName} to a {@code Biome} value.
         * </p>
         *
         * @param argBiome biome to set the name for
         * @param argName  new name of the biome
         *
         * @see <a href="https://mobtalker.net/wiki/Script_API/Types/Biome/SetName">MobTalker2 wiki</a>
         * @since 0.7.2
         */
        @MtsNativeFunction
        public static void setName( MtsValue argBiome, MtsValue argName )
        {
            String name = checkString( argName, 1 );
            if ( Strings.isNullOrEmpty( name ) )
            {
                throw new MtsArgumentException( "biome name must not be empty" );
            }
            
            checkBiome( argBiome, 0 ).setName( name );
        }
        
        // ========================================
        
        /**
         * Retrieves the temperature level of a biome. The temperature is represented as a number internally, but will be returned as a string as described by
         * the following table:
         *
         * <table cellpadding="3">
         * <tr>
         * <th>Return Value</th>
         * <th>Min</th>
         * <th>Max</th>
         * </tr>
         * <tr>
         * <td>{@code "icy"}</td>
         * <td>-&infin;</td>
         * <td>&lt; 0.05</td>
         * </tr>
         * <tr>
         * <td>{@code "cold"}</td>
         * <td>0.05</td>
         * <td>&lt; 0.2</td>
         * </tr>
         * <tr>
         * <td>{@code "normal"}</td>
         * <td>0.2</td>
         * <td>&lt; 1.2</td>
         * </tr>
         * <tr>
         * <td>{@code "warm"}</td>
         * <td>1.2</td>
         * <td>&lt; 2.0</td>
         * </tr>
         * <tr>
         * <td>{@code "hot"}</td>
         * <td>2.0</td>
         * <td>&infin;</td>
         * </tr>
         * </table>
         *
         * <p>
         * <b>NOTE:</b> Nether biomes always return the value {@code "hellish"}, regardless of their actual temperature.
         * </p>
         *
         * <p>
         * This <i>method</i> can be used in scripts by appending {@code :GetTemperature()} to a {@code Biome} value.
         * </p>
         *
         * @param argBiome The biome whose temperature should be returned
         *
         * @return One of the following {@linkplain MtsString strings}, as described above: {@code "icy"}, {@code "cold"}, {@code "normal"}, {@code "warm"},
         * {@code "hot"} or {@code "hellish"}.
         * @see Temperature
         * @see #getTemperatureEnum()
         * @see <a href="https://mobtalker.net/wiki/Script_API/Types/Biome/GetTemperature">MobTalker2 wiki</a>
         * @since 0.7.2
         */
        @MtsNativeFunction
        public static MtsString getTemperature( MtsValue argBiome )
        {
            return checkBiome( argBiome, 0 ).getTemperatureEnum().toMtsString();
        }
        
        /**
         * Modifies the temperature of a biome.
         *
         * <p>
         * Please note that due to implementation specifics of Minecraft's code all temperature values between {@code 0.1} and {@code 0.2} can not be used. For
         * that reason, if any value between {@code 0.1} and {@code 0.2} is provided as an argument to this method, an error will be thrown and script execution
         * will be aborted.
         * </p>
         *
         * <p>
         * <b>NOTE:</b> While {@link #getTemperature(MtsValue) GetTemperature} returns a string value, {@code
         * SetTemperature} requires a numeric value as argument.
         * </p>
         *
         * <p>
         * This <i>method</i> can be used in scripts by appending {@code :SetTemperature(...)} to a {@code Biome} value.
         * </p>
         *
         * @param argBiome       The biome whose temperature should be modified.
         * @param argTemperature A number which represents the temperature to be set. Must not be between 0.1 and 0.2 (both exclusive).
         *
         * @see #setTemperature(float)
         * @see <a href="https://mobtalker.net/wiki/Script_API/Types/Biome/SetTemperature">MobTalker2 wiki</a>
         * @since 0.7.2
         */
        @MtsNativeFunction
        public static void setTemperature( MtsValue argBiome, MtsValue argTemperature )
        {
            MtsBiome biome = checkBiome( argBiome, 0 );
            float temperature = (float) checkNumber( argTemperature, 1 );
            
            // Check for temperature range ]0.1;0.2[ as precaution for BiomeGenBase.setTemperatureRainfall
            if ( temperature > 0.1f && temperature < 0.2f )
            {
                throw new MtsArgumentException( 1, "temperature must not be between 0.1 and 0.2" );
            }
            
            biome.setTemperature( temperature );
        }
        
        /**
         * Returns the humidity level of a biome. Even though humidity is represented as a number internally, the return value will be a string as described by
         * the following table:
         *
         * <table cellpadding="3">
         * <tr>
         * <th>Return value</th>
         * <th>Min</th>
         * <th>Max</th>
         * </tr>
         * <tr>
         * <td>{@code "arid"}</td>
         * <td>-&infin;</td>
         * <td>&lt; 0.3</td>
         * </tr>
         * <tr>
         * <td>{@code "normal"}</td>
         * <td>0.3</td>
         * <td>&lt; 0.9</td>
         * </tr>
         * <tr>
         * <td>{@code "damp"}</td>
         * <td>0.9</td>
         * <td>&infin;</td>
         * </tr>
         * </table>
         *
         * <p>
         * This <i>method</i> can be used in scripts by appending {@code :GetHumidity()} to a {@code Biome} value.
         * </p>
         *
         * @param argBiome The biome to return the humidity for.
         *
         * @return One of the following {@linkplain MtsString strings}, as outlined above: {@code "arid"}, {@code "normal"} or {@code "damp"}.
         * @see Humidity
         * @see #getHumidityEnum()
         * @see <a href="https://mobtalker.net/wiki/Script_API/Types/Biome/GetHumidity">MobTalker2 wiki</a>
         * @since 0.7.2
         */
        @MtsNativeFunction
        public static MtsString getHumidity( MtsValue argBiome )
        {
            return checkBiome( argBiome, 0 ).getHumidityEnum().toMtsString();
        }
        
        /**
         * Modifies the humidity of a biome.
         *
         * <p>
         * <b>NOTE:</b> While {@link #getHumidity(MtsValue) GetHumidity} returns a string value, {@code SetHumidity}
         * requires a numeric value as argument.
         * </p>
         *
         * <p>
         * This <i>method</i> can be used in scripts by appending {@code :SetHumidity(...)} to a {@code Biome} value.
         * </p>
         *
         * @param argBiome    The biome for which the humidity should be set.
         * @param argHumidity A number which represents the humidity to set.
         *
         * @see #setHumidity(float)
         * @see <a href="https://mobtalker.net/wiki/Script_API/Types/Biome/SetHumidity">MobTalker2 wiki</a>
         * @since 0.7.2
         */
        @MtsNativeFunction
        public static void setHumidity( MtsValue argBiome, MtsValue argHumidity )
        {
            checkBiome( argBiome, 0 ).setHumidity( (float) checkNumber( argHumidity, 1 ) );
        }
        
        /**
         * Returns the climate of a biome, which includes temperature and humidity.
         *
         * <p>
         * This <i>method</i> can be used in scripts by appending {@code :GetClimate()} to a {@code Biome} value.
         * </p>
         *
         * @param argBiome biome to get the climate for
         *
         * @return A varargs value. The first value (index 0) is a string which represents the temperature of the given biome. The second value (index 1) is a
         * string which represents the humidity of the given biome.
         * @see #getTemperature(MtsValue)
         * @see #getHumidity(MtsValue)
         * @see <a href="http://mobtalker.net/wiki/Script_API/Types/Biome/GetClimate">MobTalker2 wiki</a>
         */
        @MtsNativeFunction
        public static MtsVarargs getClimate( MtsValue argBiome )
        {
            MtsBiome biome = checkBiome( argBiome, 0 );
            return MtsVarargs.of( getTemperature( biome ), getHumidity( biome ) );
        }
        
        // ========================================
        
        /**
         * Returns the current weather of a biome.
         *
         * <p>
         * This <i>method</i> can be used in scripts by appending {@code :GetWeather()} to a {@code Biome} value.
         * </p>
         *
         * @param argBiome biome to get the value for
         *
         * @return A varargs value. The first value (index 0) is a string which represents the current weather. The second value (index 1) is either {@code
         * true} or {@code false}, depending on if it's thundering or not.
         * @see <a href="http://mobtalker.net/wiki/Script_API/Types/Biome/GetWeather">MobTalker2 wiki</a>
         */
        @MtsNativeFunction
        public static MtsVarargs getWeather( MtsValue argBiome )
        {
            MtsBiome biome = checkBiome( argBiome, 0 );
            return MtsVarargs.of( biome.getWeather().toMtsString(),
                                  MtsBoolean.of( biome.isThundering() ) );
        }
        
        // ========================================
        
        /**
         * <p>
         * TODO <a href="http://mobtalker.net/wiki/Script_API/Types/Biome/GetWorld">wiki</a>
         * </p>
         *
         * Returns the world in which a biome is located.
         *
         * <p>
         * This <i>method</i> can be used in scripts by appending {@code :GetWorld()} to a {@code Biome} value.
         * </p>
         *
         * @param argBiome biome whose world should be returned
         *
         * @return The world in which the biome is located.
         * @see <a href="http://mobtalker.net/wiki/Script_API/Types/Biome/GetWorld">MobTalker2 wiki</a>
         */
        @MtsNativeFunction
        public static MtsValue getWorld( MtsValue argBiome )
        {
            MtsBiome biome = checkBiome( argBiome, 0 );
            return biome.getWorld();
        }
    }
}
