/*
 * SPDX-FileCopyrightText: 2013-2019 Chimaine, MobTalker2 contributors
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */
package net.mobtalker.mobtalker2.server.script.values;

import net.minecraft.entity.*;
import net.minecraft.entity.player.EntityPlayer;
import net.mobtalker.mobtalker2.common.entity.EntityType;
import net.mobtalker.mobtalker2.server.script.lib.metatables.Metatables;
import net.mobtalker.mobtalker2.util.EntityUtil;
import net.mobtalker.mobtalkerscript.v3.MtsRuntimeException;

public class MtsEntityCharacter extends MtsCharacter
{
    private final EntityLivingBase _entity;
    private final EntityType _entityType;
    
    // ========================================
    
    public MtsEntityCharacter( EntityLivingBase entity, EntityType type )
    {
        _entity = entity;
        _entityType = type;
        setMetaTable( Metatables.get( type ) );
    }
    
    // ========================================
    
    @Override
    public boolean hasEntity()
    {
        return true;
    }
    
    @SuppressWarnings( "unchecked" )
    @Override
    public <T extends EntityLivingBase> T getEntity()
    {
        return (T) _entity;
    }
    
    @Override
    public EntityType getEntityType()
    {
        return _entityType;
    }
    
    // ========================================
    
    @Override
    public String getName()
    {
        return EntityUtil.getDisplayName( _entity );
    }
    
    @Override
    public void setName( String name )
    {
        if ( _entity instanceof EntityPlayer )
            throw new MtsRuntimeException( "cannot set name of a player" );
        
        _entity.setCustomNameTag( name );
        
        if ( _entity instanceof EntityLiving )
        {
            ( (EntityLiving) _entity ).enablePersistence();
        }
    }
    
    @Override
    public void clearName()
    {
        _entity.setCustomNameTag( "" );
        // TODO check if setting a custom name tag for a player will lead to issues
        // TODO There is currently no way to remove the persistence flag. Investigate.
    }
}
