/*
 * SPDX-FileCopyrightText: 2013-2019 Chimaine, MobTalker2 contributors
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */
package net.mobtalker.mobtalker2.server.script.values;

import com.google.common.collect.Maps;
import net.minecraft.block.properties.IProperty;
import net.minecraft.block.state.IBlockState;
import net.mobtalker.mobtalker2.server.script.lib.metatables.Metatables;
import net.mobtalker.mobtalkerscript.v3.MtsArgumentException;
import net.mobtalker.mobtalkerscript.v3.value.*;
import net.mobtalker.mobtalkerscript.v3.value.userdata.MtsNativeFunction;
import org.apache.commons.lang3.EnumUtils;

import java.util.*;
import java.util.Map.Entry;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static net.mobtalker.mobtalker2.server.script.lib.ScriptApiCheck.checkBlockState;
import static net.mobtalker.mobtalker2.server.script.lib.ScriptApiCheck.checkBoolean;
import static net.mobtalker.mobtalker2.util.CollectionUtil.generify;
import static net.mobtalker.mobtalker2.util.CollectionUtil.join;
import static net.mobtalker.mobtalkerscript.v3.MtsCheck.*;
import static org.apache.commons.lang3.Validate.notNull;

/**
 * Immutable MobTalkerScript accessor for Minecraft's IBlockState. All methods that modify properties of a block state will return a new block state with the
 * applied changes, but the block state on which the changes were made stays unchanged.
 *
 * @since 0.7.2
 */
public class MtsBlockState extends MtsValueWithMetaTable
{
    public static MtsBlockState of( IBlockState state )
    {
        return new MtsBlockState( Metatables.get( TYPE ), state );
    }
    
    // ========================================
    
    public static final MtsType TYPE = MtsType.forName( "blockstate" );
    
    // ========================================
    
    private final IBlockState _state;
    
    // ========================================
    
    private MtsBlockState( MtsTable metatable, IBlockState state )
    {
        _state = notNull( state );
        setMetaTable( notNull( metatable ) );
    }
    
    // ========================================
    
    public IBlockState toBlockState()
    {
        return _state;
    }
    
    // ========================================
    
    public MtsMaterial getMaterial()
    {
        return MtsMaterial.of( toBlockState().getBlock() );
    }
    
    // ========================================
    
    public List<IProperty> keys()
    {
        Collection<?> properties = toBlockState().getPropertyNames();
        return generify( properties );
    }
    
    public Map<IProperty, Comparable<?>> values()
    {
        Map<?, ?> values = toBlockState().getProperties();
        return generify( values );
    }
    
    public boolean has( IProperty key )
    {
        return keys().contains( key );
    }
    
    public boolean has( String key )
    {
        return keys().stream().map( IProperty::getName ).anyMatch( key::equals );
    }
    
    public boolean isValid( IProperty key, MtsValue value )
    {
        Class<?> valueClass = key.getValueClass();
        if ( valueClass == Boolean.class )
        {
            return value.isBoolean();
        }
        
        if ( valueClass == Integer.class )
        {
            if ( !value.isNumber() )
            {
                return false;
            }
            
            int i = value.asNumber().toJavaInt();
            return key.getAllowedValues().contains( i );
        }
        
        if ( Enum.class.isAssignableFrom( valueClass ) )
        {
            if ( !value.isString() )
            {
                return false;
            }
            
            String s = value.asString().toJava();
            return EnumUtils.isValidEnum( valueClass.asSubclass( Enum.class ), s );
        }
        
        return false;
    }
    
    public String getValueTypeName( IProperty key )
    {
        Class<?> valueClass = key.getValueClass();
        if ( valueClass == Boolean.class )
        {
            return MtsType.BOOLEAN.getName();
        }
        if ( valueClass == Integer.class )
        {
            return MtsType.NUMBER.getName();
        }
        if ( Enum.class.isAssignableFrom( valueClass ) )
        {
            return MtsType.STRING.getName();
        }
        throw new MtsArgumentException( "property has unsupported value type '%s'", valueClass.getName() );
    }
    
    public Collection<Comparable<?>> getAllowedValues( IProperty key )
    {
        return generify( key.getAllowedValues() );
    }
    
    public Comparable<?> get( IProperty key )
    {
        return toBlockState().getValue( key );
    }
    
    public boolean getBoolean( IProperty key )
    {
        if ( key.getValueClass() != Boolean.class )
        {
            throw new IllegalArgumentException( "expected boolean, got " + key.getValueClass().getName() );
        }
        
        return (Boolean) get( key );
    }
    
    public int getInt( IProperty key )
    {
        if ( key.getValueClass() != Integer.class )
        {
            throw new IllegalArgumentException( "expected int, got " + key.getValueClass().getName() );
        }
        
        return (Integer) get( key );
    }
    
    @SuppressWarnings( "unchecked" )
    public <E extends Enum<E>> E getEnum( IProperty key )
    {
        if ( !Enum.class.isAssignableFrom( key.getValueClass() ) )
        {
            throw new IllegalArgumentException( "expected enum value, got " + key.getValueClass().getName() );
        }
        
        return (E) get( key );
    }
    
    public String getString( IProperty key )
    {
        return key.getName( get( key ) );
    }
    
    public Map<IProperty, Comparable<?>> getAll( List<? extends IProperty> keys )
    {
        Map<IProperty, Comparable<?>> map = new WeakHashMap<>();
        for ( IProperty key : keys )
        {
            map.put( key, get( key ) );
        }
        return map;
    }
    
    public MtsBlockState cycle( IProperty key )
    {
        return of( toBlockState().cycleProperty( key ) );
    }
    
    public MtsBlockState cycleAll( List<? extends IProperty> keys )
    {
        IBlockState state = toBlockState();
        for ( IProperty key : keys )
        {
            state = state.cycleProperty( key );
        }
        return state == toBlockState() ? this : of( state );
    }
    
    public MtsBlockState set( IProperty key, Comparable<?> value )
    {
        return of( toBlockState().withProperty( key, value ) );
    }
    
    public MtsBlockState setAll( Map<? extends IProperty, ? extends Comparable<?>> map )
    {
        IBlockState state = toBlockState();
        for ( Entry<? extends IProperty, ? extends Comparable<?>> entry : map.entrySet() )
        {
            state = state.withProperty( entry.getKey(), entry.getValue() );
        }
        return state == toBlockState() ? this : of( state );
    }
    
    // ========================================
    
    @Override
    public MtsTable asTable()
    {
        Map<IProperty, Comparable<?>> values = values();
        MtsTable table = new MtsTable( 0, values.size() );
        
        for ( Entry<IProperty, Comparable<?>> entry : values.entrySet() )
        {
            IProperty key = entry.getKey();
            table.set( key.getName(), Converter.toMtsValue( key, entry.getValue() ) );
        }
        
        return table;
    }
    
    // ========================================
    
    @Override
    public MtsType getType()
    {
        return TYPE;
    }
    
    // ========================================
    
    @Override
    public int hashCode()
    {
        return Objects.hash( _state );
    }
    
    @Override
    public boolean equals( Object o )
    {
        if ( this == o )
        {
            return true;
        }
        if ( o == null )
        {
            return false;
        }
        if ( !( o instanceof MtsBlockState ) )
        {
            return false;
        }
        
        MtsBlockState other = (MtsBlockState) o;
        return Objects.equals( _state, other._state );
    }
    
    // ========================================
    
    @Override
    public String toString()
    {
        String properties = join( keys(), e -> String.format( "%s=>%s", e.getName(), getString( e ) ), "," );
        return String.format( "%s[%s,{%s}]", TYPE.getName(), getMaterial().getName(), properties );
    }
    
    // ========================================
    
    private IProperty checkPropertyName( MtsValue value )
    {
        String name = checkString( value );
        Optional<IProperty> key = keys().stream().filter( e -> e.getName().equals( name ) ).findFirst();
        if ( !key.isPresent() )
        {
            throw new MtsArgumentException( "bad argument (invalid property name '%s')", name );
        }
        return key.get();
    }
    
    private IProperty checkPropertyName( MtsValue value, int index )
    {
        String name = checkString( value, index );
        Optional<IProperty> key = keys().stream().filter( e -> e.getName().equals( name ) ).findFirst();
        if ( !key.isPresent() )
        {
            throw new MtsArgumentException( index, "invalid property name '%s'", name );
        }
        return key.get();
    }
    
    // ========================================
    
    public static final class Converter
    {
        protected static Comparable<?> toPropertyValue( IProperty property, MtsValue value, int index )
        {
            Collection<?> allowedValues = property.getAllowedValues();
            Class<?> cls = property.getValueClass();
            if ( cls == Boolean.class )
            {
                boolean b = checkBoolean( value, index );
                if ( !allowedValues.contains( b ) )
                {
                    throw new MtsArgumentException( index, "invalid property value '%s'", value );
                }
                return b;
            }
            if ( cls == Integer.class )
            {
                int i = checkInteger( value, index );
                if ( !allowedValues.contains( i ) )
                {
                    throw new MtsArgumentException( index, "invalid property value '%s'", value );
                }
                return i;
            }
            if ( Enum.class.isAssignableFrom( cls ) )
            {
                Enum<?> e = Enum.valueOf( cls.asSubclass( Enum.class ), checkString( value, index ) );
                if ( !allowedValues.contains( e ) )
                {
                    throw new MtsArgumentException( index, "invalid property value '%s'", value );
                }
                return e;
            }
            
            // TODO check for additional typical values
            throw new MtsArgumentException( index, "property has unsupported value type '%s'", cls.getName() );
        }
        
        public static MtsValue toMtsValue( IProperty property, Comparable<?> value )
        {
            Class<?> cls = property.getValueClass();
            if ( cls == Boolean.class )
            {
                return MtsBoolean.of( (Boolean) value );
            }
            if ( cls == Integer.class )
            {
                return MtsNumber.of( (Integer) value );
            }
            //if ( Number.class.isAssignableFrom( cls ) ) return MtsNumber.of( ( (Number) value ).doubleValue() );
            return MtsString.of( property.getName( value ) );
        }
        
        public static Comparable<?> toPropertyValue( IProperty property, MtsValue value )
        {
            return toPropertyValue( property, value, -1 );
        }
    }
    
    // ========================================
    
    public static final class Methods
    {
        /**
         * Returns a table containing all valid property names of a block state.
         *
         * <p>
         * This <i>method</i> can be used in scripts by appending {@code :GetKeys()} to a {@code blockstate} value.
         * </p>
         *
         * @param argState The block state for which the list of valid properties should be returned.
         *
         * @return A {@link MtsTable table} value containing all valid propertiy names of the given block state.
         * @see <a href="https://mobtalker.net/wiki/Script_API/Types/BlockState/GetKeys">MobTalker2 wiki</a>
         * @since 0.7.2
         */
        @MtsNativeFunction
        public static MtsTable getKeys( MtsValue argState )
        {
            MtsBlockState state = checkBlockState( argState, 0 );
            List<IProperty> keys = state.keys();
            MtsTable table = new MtsTable( keys.size(), 0 );
            MtsTableList list = table.list();
            
            for ( IProperty property : keys )
            {
                list.add( MtsString.of( property.getName() ) );
            }
            
            return table;
        }
        
        /**
         * Returns a table representation of a block state. The table maps from property name to corresponding property value.
         *
         * <p>
         * This <i>method</i> can be used in scripts by appending {@code :GetValues()} to a {@code blockstate} value.
         * </p>
         *
         * @param argState The block state which should be converted to a table.
         *
         * @return A {@link MtsTable table} that represents the given block state.
         * @see <a href="https://mobtalker.net/wiki/Script_API/Types/BlockState/GetValues">MobTalker2 wiki</a>
         * @since 0.7.2
         */
        @MtsNativeFunction
        public static MtsTable getValues( MtsValue argState )
        {
            return checkBlockState( argState, 0 ).asTable();
        }
        
        /**
         * Returns the value that is assigned to a block state property.
         *
         * <p>
         * Depending on the property accessed, this method will return a value of either of the following types: {@link MtsBoolean boolean}, {@link MtsNumber
         * number} or {@link MtsString string}. The kind of value that will be returned by this method can be determined with {@link #getValueType(MtsValue,
         * MtsValue) GetValueType}.
         * </p>
         *
         * <p>
         * This <i>method</i> can be used in scripts by appending {@code :Get(...)} to a {@code blockstate} value.
         * </p>
         *
         * @param argState    The block state which should be checked for the property.
         * @param argProperty The name of the property whose value should be returned.
         *
         * @return The value that is assigned to the given property. This can be a value of either of the following types: {@code boolean}, {@code number} or
         * {@code string}.
         * @see Converter#toMtsValue(IProperty, Comparable)
         * @see <a href="https://mobtalker.net/wiki/Script_API/Types/BlockState/Get">MobTalker2 wiki</a>
         * @since 0.7.2
         */
        @MtsNativeFunction
        public static MtsValue get( MtsValue argState, MtsValue argProperty )
        {
            MtsBlockState state = checkBlockState( argState, 0 );
            IProperty property = state.checkPropertyName( argProperty, 1 );
            return Converter.toMtsValue( property, state.get( property ) );
        }
        
        /**
         * Returns the values of all given properties as a table which is mapped from property to their respective values.
         *
         * <p>
         * Depending on the property, the value will have either of the following types: {@link MtsBoolean boolean}, {@link MtsNumber number} or {@link
         * MtsString string}. The kind of value that will be used for one specific property can be determined with {@link #getValueType(MtsValue, MtsValue)
         * GetValueType}.
         * </p>
         *
         * <p>
         * This <i>method</i> can be used in scripts by appending {@code :GetAll(...)} to a {@code blockstate} value.
         * </p>
         *
         * @param args A varargs parameter with the following restrictions:
         *             <ul>
         *             <li>The first parameter must be a {@code blockstate} and describes the block state for which the property values should be
         *             returned.</li>
         *             <li>After that, either a {@link MtsTable table} or at least one {@code string} must be given and is used as a list of all properties
         *             that should be queried</li>
         *             </ul>
         *
         * @return A table which maps from property name to value. Each of the values can have one of the following types: {@code boolean}, {@code number} or
         * {@code string}.
         * @see Converter#toMtsValue(IProperty, Comparable)
         * @see <a href="https://mobtalker.net/wiki/Script_API/Types/BlockState/GetAll">MobTalker2 wiki</a>
         * @since 0.7.2
         */
        @MtsNativeFunction
        public static MtsTable getAll( MtsVarargs args )
        {
            MtsBlockState state = checkBlockState( args, 0 );
            Stream<MtsValue> argPropertyNames;
            if ( args.get( 1 ).isTable() )
            {
                argPropertyNames = checkTable( args, 1 ).list().stream();
            }
            else
            {
                argPropertyNames = Arrays.stream( args.subArgs( 1 ).toArray() );
            }
            
            List<IProperty> propertyNames = argPropertyNames.map( state::checkPropertyName ).collect( Collectors.toList() );
            Map<IProperty, Comparable<?>> values = state.getAll( propertyNames );
            MtsTable table = new MtsTable( 0, values.size() );
            for ( Entry<IProperty, Comparable<?>> entry : values.entrySet() )
            {
                IProperty key = entry.getKey();
                table.set( key.getName(), Converter.toMtsValue( key, entry.getValue() ) );
            }
            
            return table;
        }
        
        /**
         * Sets the value of a property.
         *
         * <p>
         * Note that even though properties accept a certain value type, all properties only allow a specific range of values. If an invalid value is used,
         * calling {@code Set} will result in an error. {@link #isValid(MtsValue, MtsValue, MtsValue) IsValid} can be used to verify if one specific value is
         * valid for a given property.
         * </p>
         *
         * <p>
         * Additionally, as {@code blockstate} values are immutable, a new {@code blockstate} value with the applied changes will be returned, but the value on
         * which {@code Set} was called will not be modified. This also means that before changes on the block state are propagated to the Minecraft world, it
         * will have to be placed in the world (e. g. with {@link MtsWorld.Methods#setBlockAt(MtsVarargs) world:SetBlockAt(...)}).
         * </p>
         *
         * <p>
         * This <i>method</i> can be used in scripts by appending {@code :Set(...)} to a {@code blockstate} value.
         * </p>
         *
         * @param argState    The block state for which the changes should be applied.
         * @param argProperty The name of the property that should be modified.
         * @param argValue    The new value of the property that should be set.
         *
         * @return A new {@code blockstate} value with the given changes.
         * @see Converter#toPropertyValue(IProperty, MtsValue)
         * @see <a href="https://mobtalker.net/wiki/Script_API/Types/BlockState/Set">MobTalker2 wiki</a>
         * @since 0.7.2
         */
        @MtsNativeFunction
        public static MtsBlockState set( MtsValue argState, MtsValue argProperty, MtsValue argValue )
        {
            MtsBlockState state = checkBlockState( argState, 0 );
            IProperty key = state.checkPropertyName( argProperty, 1 );
            Comparable<?> value = Converter.toPropertyValue( key, argValue, 2 );
            return state.set( key, value );
        }
        
        /**
         * Sets the values of a set of properties.
         *
         * <p>
         * Note that even though properties accept a certain value type, all properties only allow a specific range of values. If an invalid value is used,
         * calling {@code SetAll} will result in an error. {@link #isValid(MtsValue, MtsValue, MtsValue) IsValid} can be used to verify if one specific value is
         * valid for a given property.
         * </p>
         *
         * <p>
         * Additionally, as {@code blockstate} values are immutable, a new {@code blockstate} value with the applied changes will be returned, but the value on
         * which {@code SetAll} was called will not be modified. This also means that before changes on the block state are propagated to the Minecraft world,
         * it will have to be placed in the world (e. g. with {@link MtsWorld.Methods#setBlockAt(MtsVarargs) world:SetBlockAt(...)}).
         * </p>
         *
         * <p>
         * This <i>method</i> can be used in scripts by appending {@code :SetAll(...)} to a {@code blockstate} value.
         * </p>
         *
         * @param argState      The block state for which the changes should be applied.
         * @param argProperties A table mapping from property name to property value which represents the changes to be made.
         *
         * @return A new {@code blockstate} value with the given changes.
         * @see Converter#toPropertyValue(IProperty, MtsValue)
         * @see <a href="https://mobtalker.net/wiki/Script_API/Types/BlockState/SetAll">MobTalker2 wiki</a>
         * @since 0.7.2
         */
        @MtsNativeFunction
        public static MtsBlockState setAll( MtsValue argState, MtsValue argProperties )
        {
            MtsBlockState state = checkBlockState( argState, 0 );
            MtsTable table = checkTable( argProperties, 1 );
            MtsTableMap map = table.map();
            Map<IProperty, Comparable<?>> converted = Maps.newHashMapWithExpectedSize( map.size() );
            
            for ( Entry<MtsValue, MtsValue> entry : map.entrySet() )
            {
                IProperty key = state.checkPropertyName( entry.getKey() );
                MtsValue value = entry.getValue();
                converted.put( key, Converter.toPropertyValue( key, value ) );
            }
            
            return state.setAll( converted );
        }
        
        /**
         * Cycles the value of a property to the next value in sequence. If the value is at the end of the sequence, it will be set to the first valid value.
         *
         * <p>
         * As {@code blockstate} values are immutable, a new {@code blockstate} with the cycled property value will be returned, but the value on which {@code
         * Cycle} was called will not be modified. This also means that before changes to the block state are propagated to the Minecraft world, it will have to
         * be placed in the world (e. g. with {@link MtsWorld.Methods#setBlockAt(MtsVarargs) world:SetBlockAt(...)}).
         * </p>
         *
         * <p>
         * This <i>method</i> can be used in scripts by appending {@code :Cycle(...)} to a {@code blockstate} value.
         * </p>
         *
         * @param argState    The block state of which the property should be cycled to its next value.
         * @param argProperty The name of the property that should be cycled.
         *
         * @return A new {@code blockstate} value with the given property cycled to its next value.
         * @see <a href="https://mobtalker.net/wiki/Script_API/Types/BlockState/Cycle">MobTalker2 wiki</a>
         * @since 0.7.2
         */
        @MtsNativeFunction
        public static MtsBlockState cycle( MtsValue argState, MtsValue argProperty )
        {
            MtsBlockState state = checkBlockState( argState, 0 );
            IProperty key = state.checkPropertyName( argProperty, 1 );
            return state.cycle( key );
        }
        
        /**
         * Cycles the values of a set of properties to the next value in sequence. If any value is at the end of the sequence, it will be set to the first valid
         * value.
         *
         * <p>
         * As {@code blockstate} values are immutable, a new {@code blockstate} with all properties cycled to their next value will be returned, but the value
         * on which {@code CycleAll} was called will not be modified. This also means that before any changes to the block state are propagated to the Minecraft
         * world, it will have to be manually placed (e. g. with {@link MtsWorld.Methods#setBlockAt(MtsVarargs) world:SetBlockAt(...)}.
         * </p>
         *
         * <p>
         * This <i>method</i> can be used in scripts by appending {@code :CycleAll(...)} to a {@code blockstate} value.
         * </p>
         *
         * @param args A varargs parameter with the following restrictions:
         *             <ul>
         *             <li>The first parameter must be a {@code blockstate} and describes the block state for which the property values should be
         *             returned.</li>
         *             <li>After that, either a {@link MtsTable table} or at least one {@code string} must be given and is used as a list of all properties
         *             that should be cycled</li>
         *             </ul>
         *
         * @return A new {@code blockstate} value with the given properties cycled to their respective next values.
         * @see <a href="https://mobtalker.net/wiki/Script_API/Types/BlockState/CycleAll">MobTalker2 wiki</a>
         * @since 0.7.2
         */
        @MtsNativeFunction
        public static MtsBlockState cycleAll( MtsVarargs args )
        {
            MtsBlockState state = checkBlockState( args, 0 );
            Stream<MtsValue> argPropertyNames;
            if ( args.get( 1 ).isTable() )
            {
                argPropertyNames = checkTable( args, 1 ).list().stream();
            }
            else
            {
                argPropertyNames = Arrays.stream( args.subArgs( 1 ).toArray() );
            }
            
            List<IProperty> properties = argPropertyNames.map( state::checkPropertyName ).collect( Collectors.toList() );
            return state.cycleAll( properties );
        }
        
        /**
         * Checks whether a block state has a property with the given name.
         *
         * <p>
         * This <i>method</i> can be used in scripts by appending {@code :Has(...)} to a {@code blockstate} value.
         * </p>
         *
         * @param argState    The block state which should be checked for the property
         * @param argProperty The name of the property that should be checked for
         *
         * @return {@link MtsBoolean#True true} if the block state accepts or contains a property with the given name, {@link MtsBoolean#False false} otherwise.
         * @see <a href="https://mobtalker.net/wiki/Script_API/Types/BlockState/Has">MobTalker2 wiki</a>
         * @since 0.7.2
         */
        @MtsNativeFunction
        public static MtsBoolean has( MtsValue argState, MtsValue argProperty )
        {
            MtsBlockState state = checkBlockState( argState, 0 );
            String property = checkString( argProperty, 1 );
            return MtsBoolean.of( state.has( property ) );
        }
        
        /**
         * Checks whether a value is valid for a given block state and property.
         *
         * <p>
         * A value is considered as valid iff the value has the correct type (as returned by {@link #getValueType(MtsValue, MtsValue) GetValueType}) and is in
         * the range of accepted values (as returned by {@link #getAllowedValues(MtsValue, MtsValue) GetAllowedValues}).
         * </p>
         *
         * <p>
         * This <i>method</i> can be used in scripts by appending {@code :IsValid(...)} to a {@code blockstate} value.
         * </p>
         *
         * @param argState    The block state which should be used
         * @param argProperty The name of the property for which the value should be checked
         * @param argValue    The value to be checked for validity
         *
         * @return {@link MtsBoolean#True true} if the given value is valid for this combination of block state and property, {@link MtsBoolean#False false}
         * otherwise.
         * @see #getValueType(MtsValue, MtsValue) GetValueType
         * @see #getAllowedValues(MtsValue, MtsValue) GetAllowedValues
         * @see <a href="https://mobtalker.net/wiki/Script_API/Types/BlockState/IsValid">MobTalker2 wiki</a>
         * @since 0.7.2
         */
        @MtsNativeFunction
        public static MtsBoolean isValid( MtsValue argState, MtsValue argProperty, MtsValue argValue )
        {
            MtsBlockState state = checkBlockState( argState, 0 );
            IProperty property = state.checkPropertyName( argProperty, 1 );
            return MtsBoolean.of( state.isValid( property, argValue ) );
        }
        
        /**
         * Returns the type of values that can be assigned to a property. All properties will return one of the following values: {@code "boolean"}, {@code
         * "number"} or {@code "string"}.
         *
         * <p>
         * Note that checking only for the value type of a property value is not sufficient to determine whether it is valid. For validity checks, please use
         * {@link #getAllowedValues(MtsValue, MtsValue) GetAllowedValues} or {@link #isValid(MtsValue, MtsValue, MtsValue) IsValid} instead.
         * </p>
         *
         * <p>
         * This <i>method</i> can be used in scripts by appending {@code :GetValueType(...)} to a {@code blockstate} value.
         * </p>
         *
         * @param argState    The block state which should be used
         * @param argProperty The name of the property for whose the value type should be returned
         *
         * @return One of the following values, depending on which type the property accepts: {@code "boolean"}, {@code "number"} or {@code "string"}.
         * @see <a href="https://mobtalker.net/wiki/Script_API/Types/BlockState/GetValueType">MobTalker2 wiki</a>
         * @since 0.7.2
         */
        @MtsNativeFunction
        public static MtsString getValueType( MtsValue argState, MtsValue argProperty )
        {
            MtsBlockState state = checkBlockState( argState, 0 );
            IProperty property = state.checkPropertyName( argProperty, 1 );
            return MtsString.of( state.getValueTypeName( property ) );
        }
        
        /**
         * Returns a list of all allowed values for a property.
         *
         * <p>
         * This <i>method</i> can be used in scripts by appending {@code :GetAllowedValues(...)} to a {@code blockstate} value.
         * </p>
         *
         * @param argState    The block state which should be used
         * @param argProperty The name of the property for which all allowed values should be returned
         *
         * @return A table containing all valid values for the given property.
         * @see <a href="https://mobtalker.net/wiki/Script_API/Types/BlockState/GetAllowedValues">MobTalker2 wiki</a>
         * @since 0.7.2
         */
        @MtsNativeFunction
        public static MtsTable getAllowedValues( MtsValue argState, MtsValue argProperty )
        {
            MtsBlockState state = checkBlockState( argState, 0 );
            IProperty property = state.checkPropertyName( argProperty, 1 );
            Collection<Comparable<?>> allowedValues = state.getAllowedValues( property );
            MtsTable ret = new MtsTable( allowedValues.size(), 0 );
            MtsTableList list = ret.list();
            allowedValues.stream().map( e -> Converter.toMtsValue( property, e ) ).forEachOrdered( list::add );
            return ret;
        }
    }
}
