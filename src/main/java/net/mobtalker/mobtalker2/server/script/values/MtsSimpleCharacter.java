/*
 * SPDX-FileCopyrightText: 2013-2019 Chimaine, MobTalker2 contributors
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */
package net.mobtalker.mobtalker2.server.script.values;

import net.mobtalker.mobtalkerscript.v3.value.MtsTable;

public class MtsSimpleCharacter extends MtsCharacter
{
    private String _name;
    
    // ========================================
    
    public MtsSimpleCharacter( MtsTable metatable, String name, String texturePath )
    {
        super( metatable );
        
        setName( name );
        setTexturePath( texturePath );
    }
    
    // ========================================
    
    @Override
    public String getName()
    {
        return _name;
    }
    
    @Override
    public void setName( String name )
    {
        _name = name;
    }
    
    @Override
    public void clearName()
    {
        _name = "";
    }
}
