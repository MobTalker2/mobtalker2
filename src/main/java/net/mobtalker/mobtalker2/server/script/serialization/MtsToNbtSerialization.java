/*
 * SPDX-FileCopyrightText: 2013-2019 Chimaine, MobTalker2 contributors
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */
package net.mobtalker.mobtalker2.server.script.serialization;

import java.util.*;
import java.util.Map.Entry;

import net.minecraft.nbt.*;
import net.minecraft.nbt.NBTBase.NBTPrimitive;
import net.mobtalker.mobtalkerscript.v3.value.*;

public class MtsToNbtSerialization
{
    public static NBTBase serialize( MtsValue value )
    {
        if ( value.isBoolean() )
        {
            return serialize( value.asBoolean() );
        }
        else if ( value.isInteger() )
        {
            return serialize( value.asNumber() );
        }
        else if ( value.isNumber() )
        {
            return serialize( value.asNumber() );
        }
        else if ( value.isString() )
        {
            return serialize( value.asString() );
        }
        else if ( value.is( MtsType.TABLE ) )
        {
            return serialize( value.asTable() );
        }
        else
        {
            throw new IllegalArgumentException( "Unserializeable value: " + value.getType().getName() );
        }
    }
    
    // ========================================
    
    public static NBTTagByte serialize( MtsBoolean value )
    {
        return new NBTTagByte( (byte) ( value.isTrue() ? 0x1 : 0x0 ) );
    }
    
    public static NBTPrimitive serialize( MtsNumber value )
    {
        if ( value.isInteger() )
            return new NBTTagInt( value.toJavaInt() );
        else
            return new NBTTagDouble( value.toJavaDouble() );
    }
    
    public static NBTTagString serialize( MtsString value )
    {
        return new NBTTagString( value.toJava() );
    }
    
    public static NBTTagList serialize( MtsTable table )
    {
        NBTTagList result = new NBTTagList();
        
        // List
        {
            int i = 0;
            for ( MtsValue value : table.list() )
            {
                if ( !isSerializableValue( value ) )
                    continue;
                
                NBTTagCompound listEntry = new NBTTagCompound();
                listEntry.setTag( "k", new NBTTagInt( ++i ) );
                listEntry.setTag( "v", serialize( value ) );
                
                result.appendTag( listEntry );
            }
        }
        
        // Map
        for ( Entry<MtsValue, MtsValue> entry : table.map().entrySet() )
        {
            MtsValue key = entry.getKey();
            MtsValue value = entry.getValue();
            
            if ( !isSerializeableTableKey( key ) || !isSerializableValue( value ) )
                continue;
            
            NBTTagCompound listEntry = new NBTTagCompound();
            listEntry.setTag( "k", serialize( key ) );
            listEntry.setTag( "v", serialize( value ) );
            
            result.appendTag( listEntry );
        }
        
        return result;
    }
    
    public static NBTTagCompound serialize( Map<String, MtsValue> values )
    {
        NBTTagCompound result = new NBTTagCompound();
        for ( Entry<String, MtsValue> entry : values.entrySet() )
        {
            String key = entry.getKey();
            MtsValue value = entry.getValue();
            
            if ( !isSerializableValue( value ) )
                continue;
            
            result.setTag( key, serialize( value ) );
        }
        
        return result;
    }
    
    public static NBTTagCompound serialize( MtsTable table, Set<String> keys )
    {
        NBTTagCompound result = new NBTTagCompound();
        for ( String key : keys )
        {
            MtsValue value = table.get( key );
            
            if ( !isSerializableValue( value ) )
                continue;
            
            result.setTag( key, serialize( value ) );
        }
        
        return result;
    }
    
    // ========================================
    
    public static boolean isSerializableValue( MtsValue value )
    {
        return value.isNumber() || value.isString() || value.isBoolean() || value.isTable();
    }
    
    public static boolean isSerializeableTableKey( MtsValue key )
    {
        return key.isNumber() || key.isString() || key.isBoolean();
    }
    
    // ========================================
    
    public static MtsValue deserialize( NBTBase tag )
    {
        if ( tag instanceof NBTTagInt )
        {
            return deserialize( (NBTTagInt) tag );
        }
        else if ( tag instanceof NBTTagDouble )
        {
            return deserialize( (NBTTagDouble) tag );
        }
        else if ( tag instanceof NBTTagByte )
        {
            return deserialize( (NBTTagByte) tag );
        }
        else if ( tag instanceof NBTTagString )
        {
            return deserialize( (NBTTagString) tag );
        }
        else if ( tag instanceof NBTTagList )
        {
            return deserialize( (NBTTagList) tag );
        }
        else
        {
            throw new IllegalArgumentException( "Unknown tag type: " + tag.getClass() );
        }
    }
    
    public static MtsBoolean deserialize( NBTTagByte tag )
    {
        return MtsBoolean.of( tag.getByte() != 0x0 );
    }
    
    public static MtsNumber deserialize( NBTTagInt tag )
    {
        return MtsNumber.of( tag.getInt() );
    }
    
    public static MtsNumber deserialize( NBTTagDouble tag )
    {
        return MtsNumber.of( tag.getDouble() );
    }
    
    public static MtsString deserialize( NBTTagString tag )
    {
        return MtsString.of( tag.getString() );
    }
    
    public static MtsTable deserialize( NBTTagList list )
    {
        MtsTable t = new MtsTable();
        
        for ( int i = 0; i < list.tagCount(); i++ )
        {
            NBTTagCompound entry = list.getCompoundTagAt( i );
            
            MtsValue key = deserialize( entry.getTag( "k" ) );
            MtsValue value = deserialize( entry.getTag( "v" ) );
            
            t.set( key, value );
        }
        
        return t;
    }
    
    public static Map<String, MtsValue> deserialize( NBTTagCompound compound )
    {
        @SuppressWarnings( "unchecked" )
        Set<String> keys = compound.getKeySet();
        
        Map<String, MtsValue> result = new HashMap<>( keys.size() );
        for ( String key : keys )
        {
            result.put( key, deserialize( compound.getTag( key ) ) );
        }
        
        return result;
    }
    
    public static Map<String, MtsValue> deserialize( NBTTagCompound compound, Set<String> keys )
    {
        Map<String, MtsValue> result = new HashMap<>( keys.size() );
        for ( String key : keys )
        {
            NBTBase tag = compound.getTag( key );
            if ( tag == null )
                continue;
            
            result.put( key, deserialize( tag ) );
        }
        
        return result;
    }
    
    // ========================================
    
    private MtsToNbtSerialization()
    {}
    
}
