/*
 * SPDX-FileCopyrightText: 2013-2019 Chimaine, MobTalker2 contributors
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */
package net.mobtalker.mobtalker2.server.script;

import java.io.*;
import java.util.Map;
import java.util.concurrent.*;

import com.google.common.base.*;
import com.google.common.collect.Maps;
import com.google.common.util.concurrent.ThreadFactoryBuilder;

import net.mobtalker.mobtalker2.common.MobTalkerConfig;
import net.mobtalker.mobtalker2.server.resources.scriptpack.IScriptFile;
import net.mobtalker.mobtalker2.util.logging.MobTalkerLog;
import net.mobtalker.mobtalkerscript.v3.MtsFunctionPrototype;
import net.mobtalker.mobtalkerscript.v3.compiler.MtsCompiler;

import org.antlr.v4.runtime.ANTLRInputStream;

class CompilerService
{
    private static final MobTalkerLog LOG = new MobTalkerLog( "CompilerService" );
    
    // ========================================
    
    private final ThreadPoolExecutor _executor;
    private Future<?> _initializeFuture;
    private final Map<IScriptFile, Future<MtsFunctionPrototype>> _futures;
    
    // ========================================
    
    public CompilerService()
    {
        _futures = Maps.newHashMapWithExpectedSize( 50 );
        
        ThreadFactory factory = new ThreadFactoryBuilder().setNameFormat( "MobTalker2 CompilerThread #%d" )
                                                          .setPriority( Thread.NORM_PRIORITY ).setDaemon( true ).build();
        
        int nThreads = Math.max( 1, Runtime.getRuntime().availableProcessors() / 2 );
        _executor = new ThreadPoolExecutor( nThreads,
                                            nThreads,
                                            1L,
                                            TimeUnit.MINUTES,
                                            new LinkedBlockingQueue<Runnable>(),
                                            factory );
        _executor.prestartAllCoreThreads();
        _initializeFuture = _executor.submit( new InitializeCompilerTask() );
        
        LOG.debug( "created with %d thread(s)", nThreads );
    }
    
    // ========================================
    
    private void waitForInitialization()
    {
        try
        {
            _initializeFuture.get();
        }
        catch ( Exception ex )
        {
            Throwables.propagate( ex );
        }
    }
    
    public void compile( IScriptFile file )
    {
        if ( _futures.containsKey( file ) )
            return;
        
        waitForInitialization();
        
        LOG.debug( "submitting compile task for '%s'", file.getSourceName() );
        _futures.put( file, _executor.submit( new CompileTask( file ) ) );
    }
    
    public Map<IScriptFile, Future<MtsFunctionPrototype>> getFutures()
    {
        return _futures;
    }
    
    public void shutdown( long timeout, TimeUnit unit )
        throws InterruptedException
    {
        _executor.shutdown();
        _executor.awaitTermination( timeout, unit );
    }
    
    // ========================================
    
    private static final class CompileTask implements Callable<MtsFunctionPrototype>
    {
        private final IScriptFile _file;
        
        // ========================================
        
        public CompileTask( IScriptFile file )
        {
            _file = file;
        }
        
        // ========================================
        
        @Override
        public MtsFunctionPrototype call()
            throws Exception
        {
            MtsFunctionPrototype prototype;
            
            Stopwatch stopwatch = Stopwatch.createStarted();
            try (
                Reader reader = new InputStreamReader( _file.getStream(), MobTalkerConfig.CHARSET ); )
            {
                ANTLRInputStream antlrStream = new ANTLRInputStream( reader, 1 << 14 );
                antlrStream.name = _file.getSourceName();
                
                prototype = MtsCompiler.loadChunk( antlrStream );
            }
            
            LOG.debug( "compiled '%s' in %d ms", _file.getSourceName(), stopwatch.elapsed( TimeUnit.MILLISECONDS ) );
            return prototype;
        }
    }
    
    // ========================================
    
    private static final class InitializeCompilerTask implements Runnable
    {
        @Override
        public void run()
        {
            LOG.debug( "initializing script compiler" );
            
            Stopwatch stopwatch = Stopwatch.createStarted();
            try
            {
                MtsCompiler.loadChunk( "a=1+b+c[d];a[b]={c,D};a.B:c(D,e,...);", "" );
            }
            catch ( Exception ex )
            {
                LOG.fatal( ex, "exception while initializing the compiler" );
                throw Throwables.propagate( ex );
            }
            
            LOG.debug( "initialized script compiler in %d ms", stopwatch.elapsed( TimeUnit.MILLISECONDS ) );
        }
    }
}
