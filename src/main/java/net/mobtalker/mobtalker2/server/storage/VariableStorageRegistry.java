/*
 * SPDX-FileCopyrightText: 2013-2019 Chimaine, MobTalker2 contributors
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */
package net.mobtalker.mobtalker2.server.storage;

import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.world.World;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.event.entity.EntityEvent;
import net.minecraftforge.event.entity.EntityJoinWorldEvent;
import net.minecraftforge.fml.common.FMLCommonHandler;
import net.minecraftforge.fml.common.eventhandler.EventPriority;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.mobtalker.mobtalker2.common.entity.EntityType;
import net.mobtalker.mobtalker2.server.interaction.InteractionRegistry;
import net.mobtalker.mobtalker2.util.EntityUtil;
import net.mobtalker.mobtalker2.util.MinecraftUtil;
import net.mobtalker.mobtalker2.util.logging.MobTalkerLog;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import static java.lang.String.format;
import static org.apache.commons.lang3.Validate.notNull;
import static org.apache.commons.lang3.Validate.validState;

public class VariableStorageRegistry
{
    private static final MobTalkerLog LOG = new MobTalkerLog( "VariableStorageRegistry" );
    
    private static final String STORAGE_IDENTIFIER = "MobTalkerVariableStorage";
    
    // ========================================
    
    private static VariableStorageRegistry instance;
    
    public static VariableStorageRegistry instance()
    {
        validState( isLoaded(), "No VariableStorageRegistry loaded" );
        return instance;
    }
    
    public static boolean isLoaded()
    {
        return instance != null;
    }
    
    public static void load()
    {
        validState( !isLoaded(), "VariableStorageRegistry has already been loaded" );
        LOG.debug( "Loading" );
        
        instance = new VariableStorageRegistry();
        instance.registerFactory( EntityType.Player, PlayerVariableStorage.FACTORY );
    
        // Only register variable storage for entities the player can actually interact with.
        for ( EntityType type : EntityType.values() )
        {
            if ( InteractionRegistry.instance().hasAdapterInitializer( type ) )
            {
                instance.registerFactory( type, EntityVariableStorage.FACTORY );
            }
        }
        
        MinecraftForge.EVENT_BUS.register( instance );
        FMLCommonHandler.instance().bus().register( instance );
    }
    
    public static void unload()
    {
        validState( isLoaded(), "No VariableStorageRegistry loaded" );
        LOG.debug( "Unloading" );
        
        MinecraftForge.EVENT_BUS.unregister( instance );
        FMLCommonHandler.instance().bus().unregister( instance );
        
        instance = null;
    }
    
    // ========================================
    
    private final Map<EntityType, IEntityVariableStorageFactory> _factories = new HashMap<>();
    private final Set<IEntityPropertiesVariableStorage> _uninitializedStorage = new HashSet<>();
    
    // ========================================
    
    private VariableStorageRegistry()
    {
    }
    
    // ========================================
    
    /**
     * Returns a {@link IEntityVariableStorageFactory} that was registered for the given entity type.
     * <p>
     * If no factory was registered for the given entity type, this method will move up the type ancestry and return the first factory found.
     * </p>
     *
     * @param entityType Entity type for which to return the factory
     *
     * @return An {@link IEntityVariableStorageFactory} that was registered for the given entity type or one of its ancestors.
     * @throws IllegalStateException Thrown if no factories were found for the given entity type.
     */
    public IEntityVariableStorageFactory getFactory( EntityType entityType )
    {
        notNull( entityType, "entityType must not be null" );
        
        for ( EntityType t = entityType; t != null; t = t.getParent() )
        {
            if ( _factories.containsKey( t ) )
            {
                return _factories.get( t );
            }
        }
        
        throw new IllegalStateException( String.format( "No factories registered for entity type '%s'", entityType ) );
    }
    
    /**
     * Registers a {@link IEntityVariableStorageFactory} for an entity type.
     *
     * @param entityType Entity type for which the factory should be registered
     * @param factory    Factory which should be registered
     *
     * @throws NullPointerException Thrown if {@code entityType} or {@code factory} is null.
     */
    public void registerFactory( EntityType entityType, IEntityVariableStorageFactory factory )
    {
        notNull( entityType, "entityType must not be null" );
        notNull( factory, "factory must not be null" );
        
        LOG.debug( "Registering new factory of type %s for entity type '%s'", factory.getClass().getName(), entityType );
        _factories.put( entityType, factory );
    }
    
    /**
     * Unregisters the {@link IEntityVariableStorageFactory} for an entity type.
     *
     * @param entityType Entity type whose factory should be unregistered
     */
    public void unregisterFactory( EntityType entityType )
    {
        LOG.debug( "Unregistering factory for entity type '%s'", entityType );
        _factories.remove( entityType );
    }
    
    /**
     * Checks if the entity type of the given entity has a {@link IEntityVariableStorageFactory} registered.
     *
     * @param entity Entity for which the presence of a factory should be checked
     *
     * @return {@code true} if a factory was registered for the entity's type or one of its ancestors, {@code false} otherwise.
     */
    public boolean hasFactory( EntityLivingBase entity )
    {
        return hasFactory( EntityType.of( entity ) );
    }
    
    /**
     * Checks if the given entity type has a {@link IEntityVariableStorageFactory} registered.
     * <p>
     * If no factory was registered for the given entity type, this method will move up the type ancestry and return {@code true} on the first factory found.
     * </p>
     *
     * @param entityType Entity type for which the presence of a factory should be checked
     *
     * @return {@code true} if a factory was registered for the given entity type or one of its ancestory, {@code false} otherwise.
     */
    public boolean hasFactory( EntityType entityType )
    {
        for ( EntityType t = entityType; t != null; t = t.getParent() )
        {
            if ( _factories.containsKey( t ) )
            {
                return true;
            }
        }
        
        return false;
    }
    
    // ========================================
    
    /**
     * Retrieves the variable storage for a given entity.
     *
     * @param entity The entity to retrieve the variable storage for.
     *
     * @return The variable storage or {@code null} if none is present.
     * @throws ClassCastException Thrown if extended properties were registered, but are not of the expected type.
     */
    public IEntityPropertiesVariableStorage getVariableStorage( EntityLivingBase entity )
    {
        return (IEntityPropertiesVariableStorage) entity.getExtendedProperties( STORAGE_IDENTIFIER );
    }
    
    /**
     * Retrieves the variable storage for a given entity or creates a new one if none is present.
     *
     * @param entity The entity to retrieve the variable storage for.
     *
     * @return The variable storage or {@code null} if none is present and no factory for this kind of entities has been registered.
     * @throws ClassCastException Thrown if extended properties were registered, but are not of the expected type.
     * @see #getVariableStorage(EntityLivingBase)
     * @see #createVariableStorage(EntityLivingBase)
     * @see #initializeVariableStorage(EntityLivingBase)
     */
    public IEntityPropertiesVariableStorage getOrCreateVariableStorage( EntityLivingBase entity )
    {
        IEntityPropertiesVariableStorage storage = getVariableStorage( entity );
        if ( storage != null )
        {
            return storage;
        }
        
        if ( hasFactory( entity ) )
        {
            storage = createVariableStorage( entity );
            initializeVariableStorage( entity );
            return storage;
        }
        else
        {
            LOG.debug( "Entity '%s' has no variable storage and no factory is registered for it" );
        }
        
        return null;
    }
    
    /**
     * Creates a new variable storage for a given entity.
     *
     * @param entity The entity to create the variable storage for.
     *
     * @throws UnsupportedOperationException Thrown if the entity's entity type {@linkplain #hasFactory(EntityType) does not have a registered factory}
     * @throws RuntimeException Thrown if the extended properties could not be registered properly
     */
    public IEntityPropertiesVariableStorage createVariableStorage( EntityLivingBase entity )
    {
        String entityName = EntityUtil.getDisplayName( entity );
        EntityType entityType = EntityType.of( entity );
        if ( !hasFactory( entityType ) )
        {
            throw new UnsupportedOperationException( format( "Entity type '%s' does not have a variable storage factory registered",
                                                             entityType.getDisplayName() ) );
        }
        
        IEntityVariableStorageFactory factory = getFactory( entityType );
        LOG.debug( "Creating variable storage for entity '%s' with factory of type %s", entityName, factory.getClass().getName() );
        IEntityPropertiesVariableStorage storage = factory.create();
        String identifier = entity.registerExtendedProperties( STORAGE_IDENTIFIER, storage );
        if ( !STORAGE_IDENTIFIER.equals( identifier ) )
        {
            throw new RuntimeException( format( "Failed to register variable storage properly, got '%s' instead of '%s'", identifier, STORAGE_IDENTIFIER ) );
        }
        
        _uninitializedStorage.add( storage );
        
        return storage;
    }
    
    /**
     * Initializes any uninitialized variable storage for a given entity.
     *
     * @param entity The entity to initialize the variable storage for.
     *
     * @see #initializeVariableStorage(EntityLivingBase, World)
     */
    public void initializeVariableStorage( EntityLivingBase entity )
    {
        initializeVariableStorage( entity, entity.getEntityWorld() );
    }
    
    /**
     * Initializes any uninitialized variable storage for a given entity.
     *
     * @param entity The entity to initialize the variable storage for.
     * @param world  The world for which the storage should be initialized.
     */
    public void initializeVariableStorage( EntityLivingBase entity, World world )
    {
        String entityName = EntityUtil.getDisplayName( entity );
        String worldName = world.getWorldInfo().getWorldName();
        IEntityPropertiesVariableStorage storage = getVariableStorage( entity );
        if ( storage != null )
        {
            if ( _uninitializedStorage.contains( storage ) )
            {
                LOG.debug( "Initializing variable storage for entity '%s' in world '%s'", entityName, worldName );
                storage.init( entity, world );
                _uninitializedStorage.remove( storage );
            }
            else
            {
                LOG.debug( "Not initializing variable storage for entity '%s' in world '%s': already initialized", entityName, worldName );
            }
        }
        else
        {
            LOG.debug( "Not initializing variable storage for entity '%s' in world '%s': not present", entityName, worldName );
        }
    }
    
    // ========================================
    
    @SuppressWarnings( "unused" )
    @SubscribeEvent( priority = EventPriority.LOWEST )
    public void onEntityConstructing( EntityEvent.EntityConstructing event )
    {
        boolean isServer = MinecraftUtil.isServer();
        boolean isEntityLivingBase = event.entity instanceof EntityLivingBase;
        if ( !isServer || !isEntityLivingBase )
        {
            return;
        }
        
        LOG.debug( "onEntityConstructing" );
        EntityLivingBase entity = (EntityLivingBase) event.entity;
        String entityName = EntityUtil.getDisplayName( entity );
        if ( hasFactory( entity ) )
        {
            createVariableStorage( entity );
        }
        else
        {
            LOG.debug( "Not creating variable storage for entity '%s': no factory registered", entityName );
        }
    }
    
    @SuppressWarnings( "unused" )
    @SubscribeEvent( priority = EventPriority.LOWEST )
    public void onEntityJoinWorld( EntityJoinWorldEvent event )
    {
        Entity e = event.entity;
        World world = e.worldObj;
        if ( world.isRemote || !( e instanceof EntityLivingBase ) )
        {
            return;
        }
    
        LOG.debug( "onEntityJoinWorld" );
        initializeVariableStorage( (EntityLivingBase) e, world );
    }
}
