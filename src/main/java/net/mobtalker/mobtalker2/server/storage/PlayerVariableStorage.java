/*
 * SPDX-FileCopyrightText: 2013-2019 Chimaine, MobTalker2 contributors
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */
package net.mobtalker.mobtalker2.server.storage;

public class PlayerVariableStorage extends AbstractEntityPropertiesVariableStorage implements IEntityPropertiesVariableStorage
{
    /** Name used to store the variables as NBT tags */
    private static final String NBT_TAG_NAME = PlayerVariableStorage.class.getSimpleName();
    
    public static final IEntityVariableStorageFactory FACTORY = PlayerVariableStorage::new;
    
    // ========================================
    
    public PlayerVariableStorage()
    {
        super( NBT_TAG_NAME );
    }
}
