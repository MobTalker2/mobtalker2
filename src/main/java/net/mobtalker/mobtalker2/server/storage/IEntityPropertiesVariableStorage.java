/*
 * SPDX-FileCopyrightText: 2013-2019 Chimaine, MobTalker2 contributors
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */
package net.mobtalker.mobtalker2.server.storage;

import net.minecraftforge.common.IExtendedEntityProperties;

/**
 * Extension of {@link IVariableStorage} intended for objects that are to be saved as {@link IExtendedEntityProperties}.
 *
 * @since 0.7.5
 */
public interface IEntityPropertiesVariableStorage extends IVariableStorage, IExtendedEntityProperties
{
}
