/*
 * SPDX-FileCopyrightText: 2013-2019 Chimaine, MobTalker2 contributors
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */
package net.mobtalker.mobtalker2.server.storage;

@FunctionalInterface
public interface IEntityVariableStorageFactory extends IVariableStorageFactory
{
    @Override
    IEntityPropertiesVariableStorage create();
}
