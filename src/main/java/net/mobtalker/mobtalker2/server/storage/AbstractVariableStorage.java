/*
 * SPDX-FileCopyrightText: 2013-2019 Chimaine, MobTalker2 contributors
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */
package net.mobtalker.mobtalker2.server.storage;

import net.mobtalker.mobtalkerscript.v3.value.MtsValue;

import java.util.*;
import java.util.stream.Collectors;

/**
 * General implementation of the {@link IVariableStorage} interface.
 *
 * @since 0.7.5
 */
public abstract class AbstractVariableStorage implements IVariableStorage
{
    /** Map storing the values of all saved variables by name. */
    private Map<String, Map<String, MtsValue>> _savedVariablesByScriptIdentifier = new HashMap<>();
    
    // ========================================
    
    protected void setSavedVariablesByScriptIdentifier( Map<String, Map<String, MtsValue>> savedVariablesByScriptIdentifier )
    {
        _savedVariablesByScriptIdentifier = savedVariablesByScriptIdentifier;
    }
    
    protected Map<String, Map<String, MtsValue>> getSavedVariablesByScriptIdentifier()
    {
        return _savedVariablesByScriptIdentifier;
    }
    
    // ========================================
    
    @Override
    public Map<String, Map<String, MtsValue>> getAllSavedVariables()
    {
        return getSavedVariablesByScriptIdentifier().entrySet().stream()
                                                    .map( e -> new AbstractMap.SimpleEntry<>( e.getKey(), Collections.unmodifiableMap( e.getValue() ) ) )
                                                    .collect( Collectors.toMap( Map.Entry::getKey, Map.Entry::getValue ) );
    }
    
    @Override
    public Set<String> getIdentifiers()
    {
        return Collections.unmodifiableSet( _savedVariablesByScriptIdentifier.keySet() );
    }
    
    @Override
    public Map<String, MtsValue> getSavedVariables( String identifier )
    {
        Map<String, MtsValue> savedVariables = _savedVariablesByScriptIdentifier.get( identifier );
        return savedVariables == null ? Collections.emptyMap() : Collections.unmodifiableMap( savedVariables );
    }
    
    @Override
    public void setSavedVariables( String identifier, Map<String, ? extends MtsValue> savedVariables )
    {
        Map<String, MtsValue> entry = _savedVariablesByScriptIdentifier.get( identifier );
        if ( entry == null )
        {
            entry = new HashMap<>( savedVariables.size() );
            _savedVariablesByScriptIdentifier.put( identifier, entry );
        }
        else
        {
            entry.clear();
        }
        
        entry.putAll( savedVariables );
    }
}
