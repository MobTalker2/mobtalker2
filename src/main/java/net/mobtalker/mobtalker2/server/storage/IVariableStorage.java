/*
 * SPDX-FileCopyrightText: 2013-2019 Chimaine, MobTalker2 contributors
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */
package net.mobtalker.mobtalker2.server.storage;

import net.mobtalker.mobtalkerscript.v3.value.MtsValue;

import java.util.Map;
import java.util.Set;

/**
 * Interface for classes which manage the storing of saved variables.
 * <p>
 * When saving variables to extended entity properties, the interface {@link IEntityPropertiesVariableStorage} should be implemented instead in order to provide
 * compatibility with existing code.
 * </p>
 *
 * @since 0.7.5
 */
public interface IVariableStorage
{
    /**
     * @return A map containing the saved variables mapped by their respective script's identifier.
     */
    Map<String, Map<String, MtsValue>> getAllSavedVariables();
    
    /**
     * @return A set containing identifiers of all scripts which have variables saved in this object.
     */
    Set<String> getIdentifiers();
    
    /**
     * @return A map containing the values of all saved variables mapped by the variable's name. Should be assumed to be unmodifiable.
     */
    Map<String, MtsValue> getSavedVariables( String identifier );
    
    /**
     * Clears and sets the values of all saved variables for a specific script.
     *
     * @param identifier     Identifier of the script for which the saved variables should be updated
     * @param savedVariables Set of variable values mapped to their respective names
     */
    void setSavedVariables( String identifier, Map<String, ? extends MtsValue> savedVariables );
}
