/*
 * SPDX-FileCopyrightText: 2013-2019 Chimaine, MobTalker2 contributors
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */
package net.mobtalker.mobtalker2.server.resources.scriptpack.config;

@SuppressWarnings( "serial" )
public class ScriptPackConfigException extends RuntimeException
{
    public ScriptPackConfigException( String message, Object... args )
    {
        super( String.format( message, args ) );
    }
    
    public ScriptPackConfigException( Throwable cause )
    {
        super( cause.getMessage() );
    }
}
