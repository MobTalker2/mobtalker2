/*
 * SPDX-FileCopyrightText: 2013-2019 Chimaine, MobTalker2 contributors
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */
package net.mobtalker.mobtalker2.server.resources.scriptpack;

import java.util.List;
import java.util.Set;

import static org.apache.commons.lang3.Validate.notNull;

public class ScriptPackEntry
{
    protected final IScriptPack _scriptPack;
    protected final String _identifier;
    protected final List<IScriptFile> _files;
    protected final Set<String> _savedVariablesPerEntity;
    
    // ========================================
    
    protected ScriptPackEntry( IScriptPack scriptPack,
                               String identifier,
                               List<IScriptFile> files,
                               Set<String> savedVariablesPerEntity )
    {
        _scriptPack = notNull( scriptPack, "script pack must not be null" );
        _identifier = notNull( identifier, "identifier must not be null" );
        _files = files;
        _savedVariablesPerEntity = savedVariablesPerEntity;
    }
    
    // ========================================
    
    public IScriptPack getScriptPack()
    {
        return _scriptPack;
    }
    
    public String getIdentifier()
    {
        return _identifier;
    }
    
    public List<IScriptFile> getFiles()
    {
        return _files;
    }
    
    public Set<String> getSavedVariablesPerEntity()
    {
        return _savedVariablesPerEntity;
    }
}
