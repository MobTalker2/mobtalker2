/*
 * SPDX-FileCopyrightText: 2013-2019 Chimaine, MobTalker2 contributors
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */
package net.mobtalker.mobtalker2.server.resources.scriptpack;

import java.io.*;

public interface IScriptFile
{
    public String getSourceName();
    
    public IScriptPack getScriptPack();
    
    public InputStream getStream()
        throws IOException;
}
