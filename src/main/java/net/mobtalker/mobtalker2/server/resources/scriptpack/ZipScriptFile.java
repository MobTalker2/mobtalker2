/*
 * SPDX-FileCopyrightText: 2013-2019 Chimaine, MobTalker2 contributors
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */
package net.mobtalker.mobtalker2.server.resources.scriptpack;

import java.io.*;
import java.nio.file.Path;
import java.util.zip.*;

import org.apache.commons.lang3.Validate;

public final class ZipScriptFile extends AbstractScriptFile
{
    private final Path _path;
    private final ZipFile _file;
    private final ZipEntry _entry;
    
    // ========================================
    
    public ZipScriptFile( IScriptPack pack, Path path, ZipFile file, ZipEntry entry )
    {
        super( pack );
        _path = Validate.notNull( path, "path must not be null" );
        _file = Validate.notNull( file, ".zip file must not be null" );
        _entry = Validate.notNull( entry, ".zip entry must not be null" );
    }
    
    // ========================================
    
    @Override
    public String getSourceName()
    {
        return getScriptPack().getPath().relativize( _path ).resolve( _entry.getName() ).toString();
    }
    
    @Override
    public InputStream getStream()
        throws IOException
    {
        return _file.getInputStream( _entry );
    }
}
