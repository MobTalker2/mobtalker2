/*
 * SPDX-FileCopyrightText: 2013-2019 Chimaine, MobTalker2 contributors
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */
package net.mobtalker.mobtalker2.server.resources.scriptpack;

import java.util.List;
import java.util.Set;

public class InteractionScriptEntry extends ScriptPackEntry
{
    private final String _name;
    private final Set<String> _entities;
    private final String _guiSettings;
    protected final Set<String> _savedVariablesPerPlayer;
    protected final Set<String> _savedVariablesPerPlayerAndEntity;
    
    // ========================================
    
    public InteractionScriptEntry( IScriptPack scriptPack,
                                   String identifier,
                                   String name,
                                   Set<String> entities,
                                   List<IScriptFile> files,
                                   Set<String> savedVariablesPerEntity,
                                   Set<String> savedVariablesPerPlayer,
                                   Set<String> savedVariablesPerPlayerAndEntity,
                                   String guiSettings )
    {
        super( scriptPack, identifier, files, savedVariablesPerEntity );
        _name = name;
        _entities = entities;
        _guiSettings = guiSettings;
        _savedVariablesPerPlayer = savedVariablesPerPlayer;
        _savedVariablesPerPlayerAndEntity = savedVariablesPerPlayerAndEntity;
    }
    
    // ========================================
    
    public String getName()
    {
        return _name;
    }
    
    public Set<String> getEntities()
    {
        return _entities;
    }
    
    public String getGuiSettings()
    {
        return _guiSettings;
    }
    
    public Set<String> getSavedVariablesPerPlayer()
    {
        return _savedVariablesPerPlayer;
    }
    
    public Set<String> getSavedVariablesPerPlayerAndEntity()
    {
        return _savedVariablesPerPlayerAndEntity;
    }
}
