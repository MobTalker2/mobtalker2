/*
 * SPDX-FileCopyrightText: 2013-2019 Chimaine, MobTalker2 contributors
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */
package net.mobtalker.mobtalker2.server.resources.scriptpack;

import com.google.common.base.Strings;
import net.minecraftforge.fml.common.versioning.ArtifactVersion;
import net.minecraftforge.fml.common.versioning.DefaultArtifactVersion;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;
import net.mobtalker.mobtalker2.common.entity.EntityType;
import net.mobtalker.mobtalker2.server.resources.scriptpack.config.InteractionScriptConfig;
import net.mobtalker.mobtalker2.server.resources.scriptpack.config.ScriptConfig;
import net.mobtalker.mobtalker2.server.resources.scriptpack.config.ScriptPackConfig;
import net.mobtalker.mobtalker2.server.resources.scriptpack.config.ScriptPackConfigException;
import net.mobtalker.mobtalker2.util.fp.Either;
import org.apache.commons.lang3.StringUtils;

import java.io.IOException;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

abstract class ConfigurableScriptPack extends AbstractScriptPack
{
    public static final String SCRIPT_PACK_CONFIG_NAME = "MobTalker2.pack";
    
    // ========================================
    
    private final String _name;
    private final ArtifactVersion _apiVersion;
    private final String _description;
    private final String _version;
    private final List<String> _authors;
    private final boolean _hasResources;
    
    // ========================================
    
    protected ConfigurableScriptPack( Path path )
        throws IOException
    {
        super( path );
        
        ScriptPackConfig config = readConfiguration();
        _name = config.getTitle();
        _apiVersion = new DefaultArtifactVersion( config.getApiVersion() );
        _description = config.getDescription();
        _version = config.getVersion();
        _authors = config.getAuthors();
        
        for ( ScriptConfig scriptConfig : config.getScripts() )
        {
            if ( scriptConfig instanceof InteractionScriptConfig )
            {
                loadEntry( (InteractionScriptConfig) scriptConfig );
            }
        }
        
        _hasResources = scanForResources();
    }
    
    // ========================================
    
    @Override
    public String getTitle()
    {
        return _name;
    }
    
    @Override
    public ArtifactVersion getApiVersion()
    {
        return _apiVersion;
    }
    
    @Override
    public String getDescription()
    {
        return _description;
    }
    
    @Override
    public String getVersion()
    {
        return _version;
    }
    
    @Override
    public List<String> getAuthors()
    {
        return _authors;
    }
    
    @Override
    @SideOnly( Side.CLIENT )
    public boolean hasResources()
    {
        return _hasResources;
    }
    
    // ========================================
    
    private void loadEntry( InteractionScriptConfig config )
            throws IOException
    {
        for ( String typeString : config.getEntities() )
        {
            Either<String, EntityType> type = EntityType.tryForName( typeString );
            if ( type.isLeft() )
            {
                List<String> alternatives = Arrays.stream( EntityType.values() ).map( EntityType::getKey )
                                                  .filter( name -> {
                                                      int distance = StringUtils.getLevenshteinDistance( name, typeString );
                                                      return distance >= 0 && distance <= 3;
                                                  } ).sorted().collect( Collectors.toList() );
                if ( alternatives.isEmpty() )
                {
                    throw new ScriptPackConfigException( "unknown entity type \"%s\" for interaction script \"%s\": %s",
                                                         typeString, config.getName(), type.left().get() );
                }
                
                throw new ScriptPackConfigException( "unknown entity type \"%s\" for interaction script \"%s\": %s\nPossible alternatives: %s",
                                                     typeString, config.getName(), type.left().get(), StringUtils.join( alternatives, ", " ) );
            }
        }
        
        if ( Strings.isNullOrEmpty( config.getName() ) && config.getEntities().isEmpty() )
        {
            throw new ScriptPackConfigException( "interaction script must have a name and/or at least one assigned entity" );
        }
        if ( config.getScriptPaths().isEmpty() )
        {
            throw new ScriptPackConfigException( "scripts must have at least one script file specified" );
        }
        
        List<IScriptFile> files = readFiles( config.getScriptPaths() );
        
        String guiConfig = "";
        if ( config.getGuiConfigPath() != null )
        {
            guiConfig = readGuiConfig( config.getGuiConfigPath() );
        }
        
        String identifier = config.getIdentifier();
        if ( Strings.isNullOrEmpty( identifier ) )
        {
            throw new ScriptPackConfigException( "script identifier not present" );
        }
        
        addEntry( new InteractionScriptEntry( this,
                                              identifier,
                                              config.getName(),
                                              config.getEntities(),
                                              files,
                                              config.getSavedVariablesPerEntity(),
                                              config.getSavedVariablesPerPlayer(),
                                              config.getSavedVariablesPerPlayerAndEntity(),
                                              guiConfig ) );
    }
    
    // ========================================
    
    protected abstract ScriptPackConfig readConfiguration()
        throws IOException;
    
    protected abstract List<IScriptFile> readFiles( List<String> files )
        throws IOException;
    
    // TODO read as binary?
    protected abstract String readGuiConfig( String path )
        throws IOException;
    
    protected abstract boolean scanForResources()
        throws IOException;
}
