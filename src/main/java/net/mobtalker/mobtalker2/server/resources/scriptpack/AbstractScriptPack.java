/*
 * SPDX-FileCopyrightText: 2013-2019 Chimaine, MobTalker2 contributors
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */
package net.mobtalker.mobtalker2.server.resources.scriptpack;

import java.nio.file.Path;
import java.util.*;

import org.apache.commons.lang3.Validate;

import com.google.common.collect.Lists;

import net.minecraftforge.fml.relauncher.*;
import net.mobtalker.mobtalker2.util.logging.MobTalkerLog;

abstract class AbstractScriptPack implements IScriptPack
{
    static final MobTalkerLog LOG = new MobTalkerLog( "ScriptPack" );
    
    // ========================================
    
    private final Path _path;
    private final List<ScriptPackEntry> _scripts;
    
    // ========================================
    
    protected AbstractScriptPack( Path path )
    {
        _path = Validate.notNull( path, "path must not be null" );
        _scripts = Lists.newArrayList();
    }
    
    // ========================================
    
    @Override
    public final Path getPath()
    {
        return _path;
    }
    
    @Override
    public String getVersion()
    {
        return "";
    }
    
    @Override
    public String getDescription()
    {
        return "";
    }
    
    @Override
    public List<String> getAuthors()
    {
        return Collections.emptyList();
    }
    
    protected final void addEntry( ScriptPackEntry entry )
    {
        _scripts.add( entry );
    }
    
    @Override
    public List<ScriptPackEntry> getScripts()
    {
        return _scripts;
    }
    
    @Override
    @SideOnly( Side.CLIENT )
    public boolean hasResources()
    {
        return false;
    }
    
    @Override
    public boolean hasDownloadableResources()
    {
        return false;
    }
    
    @Override
    public Path getDownloadableResourcesFile()
    {
        throw new IllegalStateException( "script pack has no downloadable resources" );
    }
    
    @Override
    public void close()
    {}
    
    // ========================================
    
    @Override
    public String toString()
    {
        return getTitle();
    }
    
    @Override
    public int hashCode()
    {
        return getPath().hashCode();
    }
    
    @Override
    public boolean equals( Object o )
    {
        if ( this == o )
        {
            return true;
        }
        if ( o == null )
        {
            return false;
        }
        if ( !( o instanceof IScriptPack ) )
        {
            return false;
        }
        return equals( (IScriptPack) o );
    }
    
    public boolean equals( IScriptPack other )
    {
        return getPath().equals( other.getPath() );
    }
}
