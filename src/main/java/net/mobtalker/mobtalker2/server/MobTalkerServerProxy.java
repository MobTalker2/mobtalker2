/*
 * SPDX-FileCopyrightText: 2013-2019 Chimaine, MobTalker2 contributors
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */
package net.mobtalker.mobtalker2.server;

import java.io.IOException;
import java.nio.file.Path;
import java.util.*;

import com.google.common.collect.ImmutableList;

import net.minecraft.server.MinecraftServer;
import net.minecraftforge.fml.common.event.FMLFingerprintViolationEvent;
import net.mobtalker.mobtalker2.MobTalker2;
import net.mobtalker.mobtalker2.common.MobTalkerCommonProxy;
import net.mobtalker.mobtalker2.util.PathUtil;

public class MobTalkerServerProxy extends MobTalkerCommonProxy
{
    @Override
    protected List<Path> getScriptPackPaths()
    {
        try
        {
            return ImmutableList.of( getWorldSavePath() );
        }
        catch ( IOException ex )
        {
            LOG.error( ex, "Unable to get script repository paths" );
            return Collections.emptyList();
        }
    }
    
    // ========================================
    
    @Override
    public void onFingerprintViolation( FMLFingerprintViolationEvent event )
    {
        throw new AssertionError( "MobTalker2 has detected an invalid fingerprint for its mod file. "
                                  + "THIS COULD MEAN THAT SOMEBODY INJECTED MALICIOUS CODE INTO IT! "
                                  + "To protect your security Minecraft has been stopped. "
                                  + "Please download MobTalker2 only from the official download location at "
                                  + "www.mobtalker.net" );
    }
    
    // ========================================
    
    @Override
    protected Path getWorldSavePath()
        throws IOException
    {
        MinecraftServer server = MinecraftServer.getServer();
        Path repoPath = PathUtil.getMinecraftPath( server.getFolderName(), MobTalker2.ID, "scripts" );
        PathUtil.ensureDirectory( repoPath );
        
        return repoPath;
    }
}
