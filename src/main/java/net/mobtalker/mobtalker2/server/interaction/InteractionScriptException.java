/*
 * SPDX-FileCopyrightText: 2013-2019 Chimaine, MobTalker2 contributors
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */
package net.mobtalker.mobtalker2.server.interaction;

public class InteractionScriptException extends Exception
{
    public InteractionScriptException( String msg )
    {
        super( msg );
    }
    
    public InteractionScriptException( Exception parent )
    {
        super( parent );
    }
    
    public InteractionScriptException( String msg, Object... args )
    {
        super( String.format( msg, args ) );
    }
    
    public InteractionScriptException( String msg, Throwable parent, Object... args )
    {
        super( String.format( msg, args ), parent );
    }
}
