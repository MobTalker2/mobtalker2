/*
 * SPDX-FileCopyrightText: 2013-2019 Chimaine, MobTalker2 contributors
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */
package net.mobtalker.mobtalker2.server.interaction;

import net.minecraft.entity.EntityLiving;

public class DefaultInteractionAdapterFactory implements IInteractionAdapterFactory
{
    @Override
    public IInteractionAdapter create( EntityLiving entity )
    {
        return new DefaultInteractionAdapter( entity );
    }
}
