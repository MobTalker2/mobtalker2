/*
 * SPDX-FileCopyrightText: 2013-2019 Chimaine, MobTalker2 contributors
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */
package net.mobtalker.mobtalker2.server.interaction;

public class InteractionSettings
{
    private double _maxDistance;
    private double _maxStartingDistance;
    private boolean _allowInWater;
    private boolean _allowInAir;
    private boolean _allowAttacking;
    
    // ========================================
    
    public InteractionSettings()
    {
        _maxDistance = 5.0 * 5.0;
        _maxStartingDistance = 3.0 * 3.0;
        _allowInWater = false;
        _allowInAir = false;
        _allowAttacking = false;
    }
    
    public InteractionSettings( double maxDistance,
                                double maxStartingDistance,
                                boolean allowInWater,
                                boolean allowInAir,
                                boolean allowAttacking )
    {
        _maxDistance = maxDistance;
        _maxStartingDistance = maxStartingDistance;
        _allowInWater = allowInWater;
        _allowInAir = allowInAir;
        _allowAttacking = allowAttacking;
    }
    
    // ========================================
    
    public double getMaxDistance()
    {
        return _maxDistance;
    }
    
    public double getMaxPlayerStartingDistance()
    {
        return _maxStartingDistance;
    }
    
    public boolean allowInWater()
    {
        return _allowInWater;
    }
    
    public boolean allowInAir()
    {
        return _allowInAir;
    }
    
    public boolean allowAttacking()
    {
        return _allowAttacking;
    }
    
    // ========================================
    
    public void setMaxDistance( double maxDistance )
    {
        _maxDistance = maxDistance;
    }
    
    public void setMaxPlayerStartingDistance( double maxDistance )
    {
        _maxStartingDistance = maxDistance;
    }
    
    public void setAllowInWater( boolean allowInWater )
    {
        _allowInWater = allowInWater;
    }
    
    public void setAllowInAir( boolean allowInAir )
    {
        _allowInAir = allowInAir;
    }
    
    public void setAllowAttacking( boolean allowAttacking )
    {
        _allowAttacking = allowAttacking;
    }
}
