/*
 * SPDX-FileCopyrightText: 2013-2019 Chimaine, MobTalker2 contributors
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */
package net.mobtalker.mobtalker2.server.interaction;

@SuppressWarnings( "serial" )
public class InteractionException extends Exception
{
    public InteractionException( String msg )
    {
        super( msg );
    }
    
    public InteractionException( Exception parent )
    {
        super( parent );
    }
    
    public InteractionException( String msg, Object... args )
    {
        super( String.format( msg, args ) );
    }
    
    public InteractionException( String msg, Exception parent, Object... args )
    {
        super( String.format( msg, args ), parent );
    }
}
