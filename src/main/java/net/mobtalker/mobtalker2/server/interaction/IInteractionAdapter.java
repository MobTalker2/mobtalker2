/*
 * SPDX-FileCopyrightText: 2013-2019 Chimaine, MobTalker2 contributors
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */
package net.mobtalker.mobtalker2.server.interaction;

import java.util.UUID;

import net.minecraft.entity.*;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.DamageSource;
import net.minecraft.world.World;
import net.minecraftforge.common.IExtendedEntityProperties;
import net.mobtalker.mobtalker2.common.entity.EntityType;
import net.mobtalker.mobtalker2.server.script.lib.EntityReaction;

/**
 * MobTalker Interaction Interface.
 * <p>
 * Attaches to entities that a player can interact with and handles their behavior.
 */
public interface IInteractionAdapter extends IExtendedEntityProperties
{
    /**
     * Returns the entity this adapter is registered for.
     */
    EntityLiving getEntity();
    
    /**
     * Returns the type of this adapter's entity.
     * 
     * @see EntityType
     */
    EntityType getEntityType();
    
    /**
     * Returns Minecraft's default name for this adapter's entity.
     */
    String getEntityName();
    
    /**
     * Returns the displayed name of the adapter's entity.
     * <p>
     * If the entity of this adapter has a somehow assigned custom name (name tag, command, NBT data, scripts) this name is
     * returned. Otherwise the result is the same as {@link #getEntityName()}.
     */
    String getDisplayedEntityName();
    
    /**
     * How (un-)forgiving the entity is to attackers.
     */
    float getUnforgivingness();
    
    // ========================================
    
    boolean allowsMovement();
    
    boolean allowsAttackers();
    
    boolean allowsInAir();
    
    boolean allowsInWater();
    
    void setAllowMovement( boolean allowMovement );
    
    void setAllowAttackers( boolean allowAttackers );
    
    void setAllowInAir( boolean allowInAir );
    
    void setAllowInWater( boolean allowInWater );
    
    // ========================================
    
    /**
     * Returns the name of the specific interaction script this adapter uses, if any.
     */
    String getScriptName();
    
    /**
     * Returns the script instance of this adapter.
     */
    InteractionScript getScript();
    
    // ========================================
    
    /**
     * Callback when the entity is about to spawn in the world.
     * <p>
     * The entity is fully initialized at this point.<br>
     * This is the place where you register your AI.
     */
    void onSpawn( World world );
    
    /**
     * Callback when a living entity attacks this adapter's entity.
     * <p>
     * Defaults to decrease the love level by the amount of damage dealt.
     */
    void onHitByEntity( EntityLivingBase attacker, DamageSource source, float amount );
    
    /**
     * Callback when the entity is about to die.
     */
    void onDeath();
    
    /**
     * Callback when the entity has changed worlds.
     */
    void onWorldChanged();
    
    // ========================================
    
    /**
     * Callback when this interaction adapter changes to active.
     */
    void onInteractionStart();
    
    /**
     * Callback when this interaction adapter changes to inactive.
     */
    void onInteractionEnd();
    
    /**
     * Callback when this interaction adapter gets canceled.
     */
    void onInteractionCanceled();
    
    // ========================================
    
    /**
     * Checks if the given player can start an interaction with this adapter's entity.
     */
    boolean canStartInteractionWith( EntityPlayerMP player, double maxDistance );
    
    /**
     * Checks if the given entity can <i>continue</i> to interact with this adapter's entity.
     * <p>
     * Use {@link #canStartInteractionWith(EntityPlayerMP, double) canStartInteractionWith} when starting an interaction.
     */
    boolean canInteractWith( EntityLivingBase entity );
    
    /**
     * Checks if this adapter's entity is currently interacting with anyone.
     */
    boolean isActive();
    
    /**
     * Returns the entity that is currently interacting with this adapter's entity, if any.
     */
    EntityPlayerMP getPartner();
    
    /**
     * Sets the interaction partner for this adapter.
     */
    void setPartner( EntityPlayerMP player );
    
    /**
     * Clears the interaction partner for this adapter.
     */
    void clearPartner();
    
    // ========================================
    
    /**
     * Returns the reaction towards a certain entity.
     */
    EntityReaction getReaction( EntityLivingBase entity );
    
    /**
     * Returns the reaction towards a certain entity.
     */
    EntityReaction getReaction( EntityType type );
    
    /**
     * Sets the reaction towards a certain entity.
     */
    void setReaction( EntityReaction reaction, EntityLivingBase entity );
    
    /**
     * Sets the reaction towards a certain entity type.
     */
    void setReaction( EntityReaction reaction, EntityType type );
    
    void resetReaction( EntityLivingBase entity );
    
    void resetReaction( EntityType type );
    
    /**
     * Returns the value of {@link SharedMonsterAttributes#followRange} for this entity, or a default value.
     */
    double getFollowRange();
    
    /**
     * Determines based on the {@link Reaction} towards the given entity if it should be attacked.
     */
    boolean shouldAttack( EntityLivingBase entity );
    
    // ========================================
    
    EntityLivingBase getFollowTarget();
    
    UUID getFollowTargetUuid();
    
    /**
     * @param target
     * @throws InteractionException If the target cannot be followed for any reason.
     */
    void setFollowedEntity( EntityLivingBase target );
    
    void clearFollowedEntity();
    
    // ========================================
    
    /**
     * Called when the {@link Entity#Entity(World) Entity(World)} constructor finishes.
     * <p>
     * In most cases, you do not have any use for this event, since all fields of subclasses aren't initialized yet (including
     * AI related fields).
     *
     * @param entity The entity that this adapter is attached to.
     * @param world The world in which the entity exists.
     */
    @Override
    void init( Entity entity, World world );
    
    /**
     * Called when the entity that this adapter is attached to is loaded.
     */
    @Override
    void loadNBTData( NBTTagCompound compound );
    
    /**
     *
     */
    void deserialize();
    
    /**
     * Called when the entity that this adapter is attached to is saved.
     * Any custom data that needs saving should be saved here.
     */
    @Override
    void saveNBTData( NBTTagCompound compound );
    
}