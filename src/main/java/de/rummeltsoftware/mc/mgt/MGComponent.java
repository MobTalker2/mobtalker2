/*
 * SPDX-FileCopyrightText: 2013-2019 Chimaine, MobTalker2 contributors
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */
package de.rummeltsoftware.mc.mgt;

import static com.google.common.base.Preconditions.*;
import static net.minecraftforge.fml.relauncher.Side.*;

import java.util.*;

import net.minecraftforge.fml.relauncher.SideOnly;

@SideOnly( CLIENT )
public abstract class MGComponent extends MGDrawable
{
    private int _id;
    private String _name;
    
    private boolean _enableMouseEvents;
    protected boolean _isMouseInside;
    
    protected MGComponent _parent;
    private final List<LayoutAnchor> _anchors;
    
    private final MutableRectangle _bounds;
    private Dimensions _dimensions;
    
    // ========================================
    
    protected final ArrayList<IMouseListener> _mouseListener;
    private final List<IMouseClickedListener> _mouseClickedListener;
    
    // ========================================
    
    {
        _mouseListener = new ArrayList<IMouseListener>( 1 );
        
        _anchors = new ArrayList<>( 1 );
        
        _bounds = new MutableRectangle();
        _dimensions = new Dimensions( 0, 0 );
        
        _mouseClickedListener = new ArrayList<IMouseClickedListener>( 1 );
    }
    
    // ========================================
    
    @Override
    protected void doLayout( Dimensions screenDim )
    {
        Rectangle screenParent;
        if ( _parent != null )
            screenParent = _parent.getBounds();
        else
            screenParent = new Rectangle( 0, screenDim.getWidth(), 0, screenDim.getHeight() );
        
        if ( _anchors.isEmpty() )
        {
            setBounds( 0, 0, _dimensions.getWidth(), _dimensions.getHeight() );
        }
        else
        {
            LayoutAnchor firstAnchor = _anchors.get( 0 );
            
            applyLayoutAnchor( firstAnchor, screenParent );
            applyDimensions( firstAnchor );
            
            for ( int i = 1, limit = _anchors.size(); i < limit; i++ )
            {
                applyLayoutAnchor( _anchors.get( i ), screenParent );
            }
        }
        
//        if ( MobTalkerConfig.Debug )
//        {
//            System.out.format( "%s: %s\n", toString(), getBounds() );
//        }
    }
    
    private void applyDimensions( LayoutAnchor point )
    {
        point.From.X.setWidth( _bounds, _dimensions.getWidth() );
        point.From.Y.setHeigth( _bounds, _dimensions.getHeight() );
    }
    
    private void applyLayoutAnchor( LayoutAnchor point, Rectangle screenBounds )
    {
        Alignment from = point.From;
        Alignment to = point.To;
        Rectangle parentBounds = ( point.Parent != null ) ? point.Parent.getBounds() : screenBounds;
        Point offset = point.Offset;
        
        from.X.setCoordinate( _bounds, to.X.getCoordinate( parentBounds ) + offset.X );
        from.Y.setCoordinate( _bounds, to.Y.getCoordinate( parentBounds ) + offset.Y );
    }
    
    List<LayoutAnchor> getPoints()
    {
        return _anchors;
    }
    
    @Override
    public void update()
    {}
    
    // ========================================
    
    @Override
    public boolean onMouseMoved( int x, int y, boolean concealed )
    {
        if ( !_enableMouseEvents )
            return false;
        
        if ( !concealed && _bounds.contains( x, y ) )
        {
            if ( !_isMouseInside )
            {
                _isMouseInside = true;
                fireMouseEntered();
            }
            
            fireMouseMoved( x, y );
            return isVisible();
        }
        else
        {
            if ( _isMouseInside )
            {
                _isMouseInside = false;
                fireMouseLeft();
            }
            
            return false;
        }
    }
    
    @Override
    public boolean onMouseClicked( int x, int y, int mouseButton )
    {
        if ( !_enableMouseEvents || !isVisible() )
            return false;
        
        if ( _bounds.contains( x, y ) )
        {
            fireMouseClicked( x, y, mouseButton );
            return true;
        }
        else
        {
            return false;
        }
    }
    
    @Override
    public boolean onMouseUp( int x, int y )
    {
        if ( !_enableMouseEvents || !isVisible() )
            return false;
        
        if ( _bounds.contains( x, y ) )
        {
            fireMouseUp( x, y );
            return true;
        }
        else
        {
            return false;
        }
    }
    
    // ========================================
    
    public boolean isMouseEventsEnabled()
    {
        return _enableMouseEvents;
    }
    
    public void enableMouseEvents( boolean flag )
    {
        _enableMouseEvents = flag;
    }
    
    // ========================================
    
    public void addMouseListener( IMouseListener l )
    {
        if ( !_mouseListener.contains( l ) )
        {
            _mouseListener.add( l );
        }
    }
    
    public void addMouseListener( IMouseClickedListener l )
    {
        if ( !_mouseClickedListener.contains( l ) )
        {
            _mouseClickedListener.add( l );
        }
    }
    
    public void removeMouseListener( IMouseListener l )
    {
        _mouseListener.remove( l );
    }
    
    public void removeMouseListener( IMouseClickedListener l )
    {
        _mouseClickedListener.remove( l );
    }
    
    // ========================================
    
    protected void fireMouseClicked( int x, int y, int mouseButton )
    {
        for ( IMouseListener l : _mouseListener )
        {
            l.mouseClicked( this, x, y, mouseButton );
        }
        for ( IMouseClickedListener l : _mouseClickedListener )
        {
            l.mouseClicked( this, x, y, mouseButton );
        }
    }
    
    protected void fireMouseUp( int x, int y )
    {
        for ( IMouseListener l : _mouseListener )
        {
            l.mouseUp( this, x, y );
        }
    }
    
    protected void fireMouseMoved( int x, int y )
    {
        for ( IMouseListener l : _mouseListener )
        {
            l.mouseMoved( this, x, y );
        }
    }
    
    protected void fireMouseEntered()
    {
        for ( IMouseListener l : _mouseListener )
        {
            l.mouseEntered( this );
        }
    }
    
    protected void fireMouseLeft()
    {
        for ( IMouseListener l : _mouseListener )
        {
            l.mouseLeft( this );
        }
    }
    
    // ========================================
    
    public int getId()
    {
        return _id;
    }
    
    public void setId( int id )
    {
        _id = id;
    }
    
    public String getName()
    {
        return _name;
    }
    
    public void setName( String name )
    {
        _name = name;
    }
    
    // ========================================
    
    public Rectangle getBounds()
    {
        return _bounds;
    }
    
    protected void setBounds( int left, int top, int right, int bottom )
    {
        _bounds.setLeft( left );
        _bounds.setTop( top );
        _bounds.setRight( right );
        _bounds.setBottom( bottom );
    }
    
    // ========================================
    
    public void setParent( MGComponent parent )
    {
        _parent = parent;
    }
    
    public MGComponent getParent()
    {
        return _parent;
    }
    
    // ========================================
    
    public void addAnchor( Alignment from, Alignment to, MGComponent parent, int offsetX, int offsetY )
    {
        addAnchor( from, to, parent, new Point( offsetX, offsetY ) );
    }
    
    public void addAnchor( Alignment from, Alignment to, MGComponent parent, Point offset )
    {
        _anchors.add( new LayoutAnchor( from, to, parent, offset ) );
        invalidate();
    }
    
    /**
     * Sets anchors to match the size and position of the given relative
     */
    public void anchorTo( MGComponent relative )
    {
        addAnchor( Alignment.TOPLEFT, Alignment.TOPLEFT, relative, Point.Zero );
        addAnchor( Alignment.BOTTOMRIGHT, Alignment.BOTTOMRIGHT, relative, Point.Zero );
    }
    
    public void clearAllAnchors()
    {
        _anchors.clear();
        invalidate();
    }
    
    public void setSize( Dimensions size )
    {
        setSize( size.getWidth(), size.getHeight() );
    }
    
    public void setSize( int width, int height )
    {
        _dimensions = new Dimensions( width, height );
    }
    
    public void setWidth( int width )
    {
        checkArgument( width >= 0 );
        
        setSize( width, _dimensions.getHeight() );
        invalidate();
    }
    
    public void setHeight( int height )
    {
        checkArgument( height >= 0 );
        
        setSize( _dimensions.getWidth(), height );
        invalidate();
    }
    
    // ========================================
    
    @Override
    public String toString()
    {
        return ( _name == null ) ? getClass().getSimpleName() : ( _name + ":" + _id );
    }
    
    // ========================================
    
    static final class LayoutAnchor
    {
        public final Alignment From;
        public final Alignment To;
        public final MGComponent Parent;
        public final Point Offset;
        
        public LayoutAnchor( Alignment from, Alignment to, MGComponent parent, Point offset )
        {
            Parent = parent;
            From = from;
            To = to;
            Offset = offset;
        }
        
        @Override
        public String toString()
        {
            return From + "," + To + "," + ( Parent == null ? "" : Parent.toString() ) + ",[" + Offset.X + "," + Offset.Y + "]";
        }
    }
}
