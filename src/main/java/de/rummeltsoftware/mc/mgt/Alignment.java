/*
 * SPDX-FileCopyrightText: 2013-2019 Chimaine, MobTalker2 contributors
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */
package de.rummeltsoftware.mc.mgt;

import java.util.HashMap;

public enum Alignment
{
 TOP( AlignmentX.CENTER, AlignmentY.TOP ),
 LEFT( AlignmentX.LEFT, AlignmentY.CENTER ),
 CENTER( AlignmentX.CENTER, AlignmentY.CENTER ),
 RIGHT( AlignmentX.RIGHT, AlignmentY.CENTER ),
 BOTTOM( AlignmentX.CENTER, AlignmentY.BOTTOM ),
 
 TOPLEFT( AlignmentX.LEFT, AlignmentY.TOP ),
 TOPRIGHT( AlignmentX.RIGHT, AlignmentY.TOP ),
 BOTTOMLEFT( AlignmentX.LEFT, AlignmentY.BOTTOM ),
 BOTTOMRIGHT( AlignmentX.RIGHT, AlignmentY.BOTTOM );
    
    // ========================================
    
    private static final HashMap<String, Alignment> _values;
    
    static
    {
        _values = new HashMap<>( values().length );
        for ( Alignment value : values() )
        {
            _values.put( value.name(), value );
        }
    }
    
    public static Alignment forName( String name )
    {
        return _values.get( name );
    }
    
    // ========================================
    
    public final AlignmentX X;
    public final AlignmentY Y;
    
    // ========================================
    
    private Alignment( AlignmentX x, AlignmentY y )
    {
        X = x;
        Y = y;
    }
}
