/*
 * SPDX-FileCopyrightText: 2013-2019 Chimaine, MobTalker2 contributors
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */
package de.rummeltsoftware.mc.mgt;

import java.math.RoundingMode;

import com.google.common.math.DoubleMath;

public enum AlignmentX
{
 LEFT
 {
     @Override
     public int getCoordinate( Rectangle r )
     {
         return r.getLeft();
     }
     
     @Override
     public void setCoordinate( MutableRectangle r, int x )
     {
         r.setLeft( x );
     }
     
     @Override
     public void setWidth( MutableRectangle r, int width )
     {
         r.setRight( r.getLeft() + width );
     }
 },
 
 CENTER
 {
     @Override
     public int getCoordinate( Rectangle r )
     {
         return r.getCenterX();
     }
     
     @Override
     public void setCoordinate( MutableRectangle r, int x )
     {
         r.setCenterX( x );
     }
     
     @Override
     public void setWidth( MutableRectangle r, int width )
     {
         int currentCenter = r.getCenterX();
         width = DoubleMath.roundToInt( width / 2d, RoundingMode.CEILING );
         r.setLeft( currentCenter - width );
         r.setRight( currentCenter + width );
     }
 },
 
 RIGHT
 {
     @Override
     public int getCoordinate( Rectangle r )
     {
         return r.getRight();
     }
     
     @Override
     public void setCoordinate( MutableRectangle r, int x )
     {
         r.setRight( x );
     }
     
     @Override
     public void setWidth( MutableRectangle r, int width )
     {
         r.setLeft( r.getRight() - width );
     }
 };
    
    // ========================================
    
    public abstract int getCoordinate( Rectangle r );
    
    public abstract void setCoordinate( MutableRectangle r, int x );
    
    public abstract void setWidth( MutableRectangle r, int width );
}
