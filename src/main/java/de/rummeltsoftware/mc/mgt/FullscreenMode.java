/*
 * SPDX-FileCopyrightText: 2013-2019 Chimaine, MobTalker2 contributors
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */
package de.rummeltsoftware.mc.mgt;

public enum FullscreenMode
{
 /**
  * Aligns the texture to the top left, keeping its original size and repeating it on both axis.
  */
 REPEAT,
 
 /**
  * Aligns the texture to the top left, keeping its original size and repeating it on the x-axis.
  */
 REPEAT_X,
 
 /**
  * Aligns the texture to the top left, keeping its original size and repeating it on the y-axis.
  */
 REPEAT_Y,
 
 /**
  * Stretches the texture to fit both horizontally and vertically.
  */
 STRETCH,
 
 /**
  * Centers the texture on the screen and resizes it to fill the screen while keeping the aspect ratio and clamping it to the
  * screen.
  */
 FIT,
 
 /**
  * Centers the texture on the screen and resizes it to fill the screen while keeping the aspect ratio.
  */
 FILL,
}
