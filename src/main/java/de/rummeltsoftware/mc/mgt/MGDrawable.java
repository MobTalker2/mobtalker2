/*
 * SPDX-FileCopyrightText: 2013-2019 Chimaine, MobTalker2 contributors
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */
package de.rummeltsoftware.mc.mgt;

import static net.minecraftforge.fml.relauncher.Side.*;
import net.minecraft.client.Minecraft;
import net.minecraft.client.audio.SoundHandler;
import net.minecraft.client.gui.FontRenderer;
import net.minecraft.client.renderer.*;
import net.minecraft.client.renderer.texture.TextureManager;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.fml.relauncher.SideOnly;
import net.mobtalker.mobtalker2.client.util.SoundHelper;

import org.lwjgl.opengl.GL11;

@SideOnly( CLIENT )
public abstract class MGDrawable
{
    public static final IMouseClickedListener clickSoundListener = ( sender, x, y, button ) -> SoundHelper.playSoundFX( "gui.button.press", 1.0F );
    
    // ========================================
    
    private boolean _isValid;
    private int _level;
    private boolean _isVisible;
    
    private Dimensions _screenDim;
    private Point _lastMousePoint;
    
    {
        _isValid = true;
        _isVisible = true;
        
        _screenDim = new Dimensions( 1, 1 );
        _lastMousePoint = new Point( -1, -1 );
    }
    
    // ========================================
    
    /**
     * Callback for one time initialization work.
     */
    public void init()
    {}
    
    /**
     * Draws this Drawable if it is visible.
     * <p>
     * If the Drawable is not valid, {@link #layout(Dimensions)} will be called before the draw.
     */
    public final void draw()
    {
        draw( false );
    }
    
    /**
     * Draws this Drawable if it is visible or <code>force</code> is set to <code>true</code>.
     * <p>
     * If the Drawable is not valid, {@link #layout(Dimensions)} will be called before the draw.
     * 
     * @param force Set to <code>true</code> if the Drawable should be drawn even if it is not visible.
     */
    public final void draw( boolean force )
    {
        if ( force || isVisible() )
        {
            if ( !isValid() )
            {
                layout( _screenDim );
            }
            
            doDraw();
        }
    }
    
    /**
     * Performs the actual drawing. <br>
     * Gets only called if this drawable is visible.
     */
    protected abstract void doDraw();
    
    public boolean isValid()
    {
        return _isValid;
    }
    
    /**
     * Invalidates this Drawable, causing a call to {@link #layout(Dimensions)} before the next draw.
     */
    public void invalidate()
    {
        _isValid = false;
    }
    
    public final void layout( Dimensions screenDim )
    {
        if ( !screenDim.equals( _screenDim ) )
        {
            _screenDim = screenDim;
        }
        
        doLayout( screenDim );
        _isValid = true;
    }
    
    /**
     * Called when the screen is initialized, resized or before a draw if the MGDrawable is invalid.
     * 
     * @param screenDim The dimensions of the screen or parent.
     */
    protected abstract void doLayout( Dimensions screenDim );
    
    /**
     * Minecraft GameLoop callback.
     */
    public abstract void update();
    
    // ========================================
    
    /**
     * Gets called before a doDraw call. <br>
     * Calls {@link #onMouseMoved(int, int, boolean)} if the mouse actually moved.
     * 
     * @param x
     * @param y
     */
    public final void onMouseUpdate( int x, int y )
    {
        if ( !_lastMousePoint.equals( x, y ) )
        {
            _lastMousePoint = new Point( x, y );
            onMouseMoved( x, y, false );
        }
    }
    
    /**
     * Event callback if the mouse pointer moved.
     * 
     * @param x
     * @param y
     * @param concealed If another drawable hides this component.
     * @return If this drawable consumed this event.
     */
    public boolean onMouseMoved( int x, int y, boolean concealed )
    {
        return false;
    }
    
    public boolean onMouseClicked( int x, int y, int mouseButton )
    {
        return false;
    }
    
    public boolean onMouseUp( int x, int y )
    {
        return false;
    }
    
    public boolean onKeyPressed( char eventChar, int eventKey )
    {
        return false;
    }
    
    public void onClosed()
    {}
    
    // ========================================
    
    /**
     * The Z level of this drawable. <br>
     * Should be a negative value if text is involved due to the font renderer always rendering at z level 0.
     */
    public int getZLevel()
    {
        return _level;
    }
    
    public void setLevel( int z )
    {
        _level = z;
    }
    
    public boolean isVisible()
    {
        return _isVisible;
    }
    
    public void setVisible( boolean isVisible )
    {
        _isVisible = isVisible;
    }
    
    // ========================================
    
    public static void drawTexture( Rectangle b, int u, int v )
    {
        Tessellator t = Tessellator.getInstance();
        WorldRenderer r = t.getWorldRenderer();
        
        double left = b.getLeft();
        double right = b.getRight();
        double top = b.getTop();
        double bottom = b.getBottom();
        
        double width = b.getWidth();
        double height = b.getHeight();
        
        double uScaling = 0.00390625F;
        double vScaling = 0.00390625F;
        
        r.startDrawingQuads();
        r.addVertexWithUV( left, bottom, 0, u * uScaling, ( v + height ) * vScaling );
        r.addVertexWithUV( right, bottom, 0, ( u + width ) * uScaling, ( v + height ) * vScaling );
        r.addVertexWithUV( right, top, 0, ( u + width ) * uScaling, v * vScaling );
        r.addVertexWithUV( left, top, 0, u * uScaling, v * vScaling );
        t.draw();
    }
    
    public static void drawTexture( int x, int y, int u, int v, int width, int height )
    {
        Tessellator t = Tessellator.getInstance();
        WorldRenderer r = t.getWorldRenderer();
        
        double left = x;
        double right = x + width;
        double top = y;
        double bottom = y + height;
        
        double uScaling = 0.00390625F;
        double vScaling = 0.00390625F;
        
        r.startDrawingQuads();
        r.addVertexWithUV( left, bottom, 0, u * uScaling, ( v + height ) * vScaling );
        r.addVertexWithUV( right, bottom, 0, ( u + width ) * uScaling, ( v + height ) * vScaling );
        r.addVertexWithUV( right, top, 0, ( u + width ) * uScaling, v * vScaling );
        r.addVertexWithUV( left, top, 0, u * uScaling, v * vScaling );
        t.draw();
    }
    
    public static void drawTextureScaled( Rectangle b, int u, int v )
    {
        Tessellator t = Tessellator.getInstance();
        WorldRenderer r = t.getWorldRenderer();
        
        double left = b.getLeft();
        double right = b.getRight();
        double top = b.getTop();
        double bottom = b.getBottom();
        
        double width = b.getWidth();
        double height = b.getHeight();
        
        double uScaling = 0.00390625F * ( 256.0F / width );
        double vScaling = 0.00390625F * ( 256.0F / height );
        
        r.startDrawingQuads();
        r.addVertexWithUV( left, bottom, 0, u * uScaling, ( v + height ) * vScaling );
        r.addVertexWithUV( right, bottom, 0, ( u + width ) * uScaling, ( v + height ) * vScaling );
        r.addVertexWithUV( right, top, 0, ( u + width ) * uScaling, v * vScaling );
        r.addVertexWithUV( left, top, 0, u * uScaling, v * vScaling );
        t.draw();
    }
    
    public static void drawTextureScaled( int x, int y, int dX, int dY, int u, int v, int dU, int dV )
    {
        Tessellator t = Tessellator.getInstance();
        WorldRenderer r = t.getWorldRenderer();
        
        double left = x;
        double right = x + dX;
        double top = y;
        double bottom = y + dY;
        
        double uScaling = 0.00390625F;
        double vScaling = 0.00390625F;
        
        r.startDrawingQuads();
        r.addVertexWithUV( left, bottom, 0, u * uScaling, ( v + dV ) * vScaling );
        r.addVertexWithUV( right, bottom, 0, ( u + dU ) * uScaling, ( v + dV ) * vScaling );
        r.addVertexWithUV( right, top, 0, ( u + dU ) * uScaling, v * vScaling );
        r.addVertexWithUV( left, top, 0, u * uScaling, v * vScaling );
        t.draw();
    }
    
    public static void drawTextureScaled( int x, int y, int dX, int dY, int u, int v, int dU, int dV, Dimensions textureDimensions )
    {
        Tessellator t = Tessellator.getInstance();
        WorldRenderer r = t.getWorldRenderer();
        
        double left = x;
        double right = dX;
        double top = y;
        double bottom = dY;
        
        double uScaling = 0.00390625F * ( 256.0F / textureDimensions.getWidth() );
        double vScaling = 0.00390625F * ( 256.0F / textureDimensions.getHeight() );
        
        r.startDrawingQuads();
        r.addVertexWithUV( left, bottom, 0, u * uScaling, ( v + dV ) * vScaling );
        r.addVertexWithUV( right, bottom, 0, ( u + dU ) * uScaling, ( v + dV ) * vScaling );
        r.addVertexWithUV( right, top, 0, ( u + dU ) * uScaling, v * vScaling );
        r.addVertexWithUV( left, top, 0, u * uScaling, v * vScaling );
        t.draw();
    }
    
    // ========================================
    
    public static void drawRectangle( Rectangle b, float cr, float cg, float cb, float ca )
    {
        Tessellator t = Tessellator.getInstance();
        WorldRenderer r = t.getWorldRenderer();
        
        double left = b.getLeft();
        double right = b.getRight();
        double top = b.getTop();
        double bottom = b.getBottom();
        
        GlStateManager.disableBlend();
        GlStateManager.enableBlend();
        GlStateManager.disableTexture2D();
        GlStateManager.blendFunc( GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA );
        GlStateManager.color( cr, cg, cb, ca );
        
        r.startDrawing( GL11.GL_QUADS );
        r.addVertex( left, bottom, 0 );
        r.addVertex( right, bottom, 0 );
        r.addVertex( right, top, 0 );
        r.addVertex( left, top, 0 );
        t.draw();
        
        GlStateManager.enableTexture2D();
        GlStateManager.disableBlend();
    }
    
    // ========================================
    
    public static void drawCenteredString( String s, int x, int y, int color )
    {
        FontRenderer r = getFontRenderer();
        r.drawStringWithShadow( s, x - ( r.getStringWidth( s ) / 2 ), y, color );
    }
    
    // ========================================
    
    public static void bindTexture( ResourceLocation resourceLocation )
    {
        getTextureManager().bindTexture( resourceLocation );
    }
    
    // ========================================
    
    public static TextureManager getTextureManager()
    {
        return Minecraft.getMinecraft().getTextureManager();
    }
    
    public static FontRenderer getFontRenderer()
    {
        return Minecraft.getMinecraft().fontRendererObj;
    }
    
    public static SoundHandler getSoundHandler()
    {
        return Minecraft.getMinecraft().getSoundHandler();
    }
    
}
