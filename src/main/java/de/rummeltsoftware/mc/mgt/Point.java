/*
 * SPDX-FileCopyrightText: 2013-2019 Chimaine, MobTalker2 contributors
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */
package de.rummeltsoftware.mc.mgt;

public class Point
{
    public static final Point Zero = new Point( 0, 0 );
    
    // ========================================
    
    public final int X;
    public final int Y;
    
    // ========================================
    
    public Point( int x, int y )
    {
        X = x;
        Y = y;
    }
    
    // ========================================
    
    public Point transition( int x, int y )
    {
        return new Point( X + x, Y + y );
    }
    
    public Point add( Point p )
    {
        return new Point( X + p.X, Y + p.Y );
    }
    
    // ========================================
    
    public boolean equals( int x, int y )
    {
        return ( X == x ) && ( Y == y );
    }
    
    // ========================================
    
    @Override
    public String toString()
    {
        return "Point [X=" + X + ", Y=" + Y + "]";
    }
}
