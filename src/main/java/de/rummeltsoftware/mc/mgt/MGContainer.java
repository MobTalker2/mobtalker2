/*
 * SPDX-FileCopyrightText: 2013-2019 Chimaine, MobTalker2 contributors
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */
package de.rummeltsoftware.mc.mgt;

import static net.minecraftforge.fml.relauncher.Side.*;

import java.util.*;

import net.minecraftforge.fml.relauncher.SideOnly;

import com.google.common.collect.Lists;

import de.rummeltsoftware.mc.mgt.util.*;
import de.rummeltsoftware.mc.mgt.util.TopologicalSort.DirectedGraph;

@SideOnly( CLIENT )
public class MGContainer extends MGComponent
{
    protected static final Comparator<MGComponent> ascZLevelComp = ( a, b ) -> a.getZLevel() - b.getZLevel();
    protected static final Comparator<MGComponent> descZLevelComp = ( a, b ) -> b.getZLevel() - a.getZLevel();
    
    // ========================================
    
    /**
     * Draw order. Z-Level order.
     */
    private final List<MGComponent> _drawOrder;
    
    /**
     * Event order. Inverse Z-Level order.
     */
    private final List<MGComponent> _eventOrder;
    
    /**
     * Layout ordering. Respects dependencies between components.
     */
    private List<MGComponent> _layoutOrder;
    
    // ========================================
    
    {
        _drawOrder = new ArrayList<MGComponent>( 1 );
        _eventOrder = Lists.reverse( _drawOrder );
        _layoutOrder = new ArrayList<MGComponent>( 1 );
    }
    
    // ========================================
    
    @Override
    protected void doDraw()
    {
        for ( MGDrawable drawable : _drawOrder )
        {
            drawable.draw();
        }
    }
    
    @Override
    protected void doLayout( Dimensions screenDim )
    {
        sortDependencies();
        
        super.doLayout( screenDim );
        for ( MGDrawable drawable : _layoutOrder )
        {
            drawable.layout( screenDim );
        }
    }
    
    private void sortDependencies()
    {
        Collections.sort( _drawOrder, ascZLevelComp );
        Collections.sort( _layoutOrder, ascZLevelComp );
        
        DirectedGraph<MGComponent> graph = new DirectedGraph<>();
        for ( MGComponent d : _layoutOrder )
        {
            graph.addNode( d );
        }
        for ( MGComponent component : _layoutOrder )
        {
            for ( MGComponent parent = component.getParent(); parent != null; component = parent, parent = component.getParent() )
            {
                addLayoutDependency( graph, parent, component );
            }
            for ( LayoutAnchor point : component.getPoints() )
            {
                addLayoutDependency( graph, point.Parent, component );
            }
        }
        
        _layoutOrder = TopologicalSort.topologicalSort( graph );
    }
    
    private static void addLayoutDependency( DirectedGraph<MGComponent> graph, MGComponent component, MGComponent parent )
    {
        if ( ( component == null ) || !graph.nodeExists( component ) || !graph.nodeExists( parent ) )
            return;
        
        graph.addEdge( component, parent );
    }
    
    @Override
    public void update()
    {
        for ( MGDrawable drawable : _layoutOrder )
        {
            drawable.update();
        }
    }
    
    // ========================================
    
    @Override
    public boolean onMouseMoved( int x, int y, boolean concealed )
    {
        boolean consumed = false;
        for ( MGDrawable drawable : _eventOrder )
        {
            consumed |= drawable.onMouseMoved( x, y, consumed );
        }
        
        return consumed;
    }
    
    @Override
    public boolean onMouseClicked( int x, int y, int button )
    {
        if ( !isVisible() )
            return false;
        
        for ( MGDrawable drawable : _eventOrder )
        {
            if ( drawable.onMouseClicked( x, y, button ) )
                return true;
        }
        
        return false;
    }
    
    @Override
    public boolean onMouseUp( int x, int y )
    {
        if ( !isVisible() )
            return false;
        
        for ( MGDrawable drawable : _eventOrder )
        {
            if ( drawable.onMouseUp( x, y ) )
                return true;
        }
        
        return false;
    }
    
    @Override
    public boolean onKeyPressed( char eventChar, int eventKey )
    {
        if ( !isVisible() )
            return false;
        
        for ( MGDrawable drawable : _eventOrder )
        {
            if ( drawable.onKeyPressed( eventChar, eventKey ) )
                return true;
        }
        
        return false;
    }
    
    @Override
    public void onClosed()
    {
        for ( MGDrawable drawable : _eventOrder )
        {
            drawable.onClosed();
        }
    }
    
    // ========================================
    
    public void add( MGComponent drawable )
    {
        if ( _drawOrder.contains( drawable ) )
            return;
        
        _drawOrder.add( drawable );
        _layoutOrder.add( drawable );
    }
    
    public void addAll( Collection<? extends MGComponent> drawables )
    {
        for ( MGComponent drawable : drawables )
        {
            add( drawable );
        }
    }
    
    public void remove( MGComponent drawables )
    {
        _drawOrder.remove( drawables );
        _layoutOrder.remove( drawables );
    }
    
    public void removeAll( Collection<? extends MGComponent> drawables )
    {
        for ( MGComponent drawable : drawables )
        {
            remove( drawable );
        }
    }
    
    public void clear()
    {
        _drawOrder.clear();
        _layoutOrder.clear();
    }
}
