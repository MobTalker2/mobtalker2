/*
 * SPDX-FileCopyrightText: 2013-2019 Chimaine, MobTalker2 contributors
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */
package de.rummeltsoftware.mc.mgt;

import static net.minecraftforge.fml.relauncher.Side.*;
import net.minecraftforge.fml.relauncher.SideOnly;

@SideOnly( CLIENT )
public class MGFullscreenTexture extends MGTexture
{
    protected FullscreenMode _mode;
    
    // ========================================
    
    public MGFullscreenTexture()
    {
        addAnchor( Alignment.CENTER, Alignment.CENTER, null, Point.Zero );
        setMode( FullscreenMode.FIT );
    }
    
    // ========================================
    
    public FullscreenMode getMode()
    {
        return _mode;
    }
    
    public void setMode( FullscreenMode mode )
    {
        _mode = mode;
    }
    
    // ========================================
    
    @Override
    public void doLayout( Dimensions screenDim )
    {
        Dimensions textureDim = this._texture.getSize();
        
        float scaleW = (float) screenDim.getWidth() / textureDim.getWidth();
        float scaleH = (float) screenDim.getHeight() / textureDim.getHeight();
        
        int height = (int) ( textureDim.getHeight() * scaleW );
        int top = ( screenDim.getHeight() - height ) / 2;
        
        int width = (int) ( textureDim.getWidth() * scaleH );
        int left = ( screenDim.getWidth() - width ) / 2;
        
        switch ( _mode )
        {
            case FIT:
            {
                if ( scaleW < scaleH )
                    setBounds( 0, top, screenDim.getWidth(), top + height );
                else
                    setBounds( left, 0, left + width, screenDim.getHeight() );
                
                break;
            }
            case FILL:
            {
                if ( scaleW > scaleH )
                    setBounds( 0, top, screenDim.getWidth(), top + height );
                else
                    setBounds( left, 0, left + width, screenDim.getHeight() );
                
                break;
            }
            case REPEAT:
                break;
            case REPEAT_X:
                break;
            case REPEAT_Y:
                break;
            case STRETCH:
                setBounds( 0, 0, screenDim.getWidth(), screenDim.getHeight() );
                break;
            default:
                break;
        }
        
        // super.doLayout( screenDim );
    }
}
