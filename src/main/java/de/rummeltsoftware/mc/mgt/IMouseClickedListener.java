/*
 * SPDX-FileCopyrightText: 2013-2019 Chimaine, MobTalker2 contributors
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */
package de.rummeltsoftware.mc.mgt;

@FunctionalInterface
public interface IMouseClickedListener
{
    void mouseClicked( MGComponent sender, int x, int y, int mouseButton );
}
