/*
 * SPDX-FileCopyrightText: 2013-2019 Chimaine, MobTalker2 contributors
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */
package de.rummeltsoftware.mc.mgt;

import static net.minecraftforge.fml.relauncher.Side.*;
import net.minecraftforge.fml.relauncher.SideOnly;

@SideOnly( CLIENT )
public interface IMouseListener
{
    void mouseClicked( MGComponent sender, int x, int y, int mouseButton );
    
    void mouseUp( MGComponent sender, int x, int y );
    
    void mouseMoved( MGComponent sender, int x, int y );
    
    void mouseEntered( MGComponent sender );
    
    void mouseLeft( MGComponent sender );
}
