/*
 * SPDX-FileCopyrightText: 2013-2019 Chimaine, MobTalker2 contributors
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */
package de.rummeltsoftware.mc.mgt;

import static com.google.common.base.Preconditions.*;

import java.math.RoundingMode;
import java.util.*;

import net.minecraft.client.renderer.*;
import net.minecraftforge.fml.relauncher.*;

import org.lwjgl.opengl.GL11;

import com.google.common.math.DoubleMath;

@SideOnly( Side.CLIENT )
public class MGPolygon extends MGComponent
{
    private final List<PathPoint> _path;
    private Color _color;
    
    // ========================================
    
    {
        _path = new ArrayList<>();
    }
    
    // ========================================
    
    @Override
    public void doLayout( Dimensions screenDim )
    {
        super.doLayout( screenDim );
        
        int width = getBounds().getWidth();
        int height = getBounds().getHeight();
        
        for ( PathPoint point : _path )
        {
            point.AbsX = DoubleMath.roundToInt( width * point.RelX, RoundingMode.CEILING );
            point.AbsY = DoubleMath.roundToInt( height * point.RelY, RoundingMode.CEILING );
        }
    }
    
    @Override
    protected void doDraw()
    {
        Tessellator t = Tessellator.getInstance();
        WorldRenderer r = t.getWorldRenderer();
        
        GlStateManager.enableBlend();
        GlStateManager.disableTexture2D();
        GlStateManager.blendFunc( GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA );
        GlStateManager.color( _color.getRf(), _color.getGf(), _color.getBf(), _color.getAf() );
        
        GlStateManager.pushMatrix();
        GlStateManager.translate( getBounds().getLeft(), getBounds().getTop(), 0.0D );
        
        r.startDrawing( GL11.GL_POLYGON );
        
        for ( PathPoint point : _path )
        {
            r.addVertex( point.AbsX, point.AbsY, 0.0D );
        }
        
        t.draw();
        
        GlStateManager.popMatrix();
        
        GlStateManager.enableTexture2D();
        GlStateManager.disableBlend();
    }
    
    // ========================================
    
    public Color getColor()
    {
        return _color;
    }
    
    public void setColor( Color c )
    {
        _color = c;
    }
    
    // ========================================
    
    public void addPoint( double x, double y )
    {
        checkArgument( ( 0.0D <= x ) && ( x <= 1.0D ), "x must be between 0.0 and 1.0" );
        checkArgument( ( 0.0D <= y ) && ( y <= 1.0D ), "y must be between 0.0 and 1.0" );
        
        _path.add( new PathPoint( x, y ) );
        invalidate();
    }
    
    public void clearPath()
    {
        _path.clear();
        invalidate();
    }
    
    // ========================================
    
    private static final class PathPoint
    {
        public final double RelX;
        public final double RelY;
        
        public double AbsX;
        public double AbsY;
        
        public PathPoint( double x, double y )
        {
            RelX = x;
            RelY = y;
        }
        
        @Override
        public String toString()
        {
            return RelX + "," + RelY;
        }
    }
}
