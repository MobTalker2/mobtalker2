/*
 * SPDX-FileCopyrightText: 2013-2019 Chimaine, MobTalker2 contributors
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */
package de.rummeltsoftware.mc.mgt;

import java.math.RoundingMode;

import com.google.common.math.DoubleMath;

/**
 * Mutable version of a rectangle
 */
class MutableRectangle extends Rectangle
{
    public MutableRectangle()
    {
        super( 0, 0, 0, 0 );
    }
    
    public MutableRectangle( int left, int right, int top, int bottom )
    {
        super( left, right, top, bottom );
    }
    
    // ========================================
    
    public void setLeft( int x )
    {
        _left = x;
    }
    
    public void setTop( int y )
    {
        _top = y;
    }
    
    public void setCenterX( int x )
    {
        int shift = DoubleMath.roundToInt( getWidth() / 2d, RoundingMode.CEILING );
        _left = x - shift;
        _right = x + shift;
    }
    
    public void setCenterY( int y )
    {
        int shift = DoubleMath.roundToInt( getHeight() / 2d, RoundingMode.CEILING );
        _top = y - shift;
        _bottom = y + shift;
    }
    
    public void setBottom( int y )
    {
        _bottom = y;
    }
    
    public void setRight( int x )
    {
        _right = x;
    }
    
    // ========================================
    
    public void translate( int x, int y )
    {
        setLeft( _left + x );
        setTop( _top + y );
    }
    
    public void copy( Rectangle parent )
    {
        setLeft( parent.getLeft() );
        setTop( parent.getTop() );
        setRight( parent.getRight() );
        setBottom( parent.getBottom() );
    }
}
