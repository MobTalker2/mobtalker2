/*
 * SPDX-FileCopyrightText: 2013-2019 Chimaine, MobTalker2 contributors
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */
/**
 * The MGT (Minecraft GUI Toolkit) aims to provide Java-AWT'esque GUI elements for modders to use.
 * 
 * @author Chimaine
 */
package de.rummeltsoftware.mc.mgt;