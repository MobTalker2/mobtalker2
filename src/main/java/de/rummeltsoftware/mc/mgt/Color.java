/*
 * SPDX-FileCopyrightText: 2013-2019 Chimaine, MobTalker2 contributors
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */
package de.rummeltsoftware.mc.mgt;

public class Color
{
    public static final Color White = new Color( 1.0f, 1.0f, 1.0f, 1.0f );
    
    // ========================================
    
    private final float _r;
    private final float _g;
    private final float _b;
    private final float _a;
    
    // ========================================
    
    public Color( float r, float g, float b, float a )
    {
        _r = r;
        _g = g;
        _b = b;
        _a = a;
    }
    
    public static Color fromHexString( String s )
    {
        return new Color( Integer.valueOf( s.substring( 3, 5 ), 16 ) / 255f,
                          Integer.valueOf( s.substring( 5, 7 ), 16 ) / 255f,
                          Integer.valueOf( s.substring( 7, 9 ), 16 ) / 255f,
                          Integer.valueOf( s.substring( 1, 3 ), 16 ) / 255f );
    }
    
    // ========================================
    
    public float getRf()
    {
        return _r;
    }
    
    public float getGf()
    {
        return _g;
    }
    
    public float getBf()
    {
        return _b;
    }
    
    public float getAf()
    {
        return _a;
    }
    
    // ========================================
    
    public int getRi()
    {
        return Math.round( _r * 255 );
    }
    
    public int getGi()
    {
        return Math.round( _g * 255 );
    }
    
    public int getBi()
    {
        return Math.round( _b * 255 );
    }
    
    public int getAi()
    {
        return Math.round( _a * 255 );
    }
    
    // ========================================
    
    public String toHexString()
    {
        return String.format( "#%x2%x2%x2%x2", getAi(), getRi(), getGi(), getBi() );
    }
    
}
