# SPDX-FileCopyrightText: 2013-2019 Chimaine, MobTalker2 contributors
#
# SPDX-License-Identifier: AGPL-3.0-or-later

-target 1.8

-dontobfuscate

#-dontoptimize
-optimizations !code/allocation/variable
-optimizationpasses 5

-dontwarn javax.annotation.**
-dontwarn javax.inject.**
-dontwarn sun.misc.Unsafe

# Preserve enum members
-keepclassmembers
 public enum *
{    
    **[] $VALUES;
    public *;
}

# Main mod class and classes referenced in @Mod annotations
-keep
 public class net.mobtalker.mobtalker2.MobTalker2,
              net.mobtalker.mobtalker2.client.config.gui.MobTalkerGuiConfigFactory
{
    public *;
}

# Keep proguard from over-optimizing the config screen >_> (#91)
-keepclassmembers public class net.mobtalker.mobtalker2.client.config.gui.MobTalkerGuiConfig { <init>(...); }

# Proxy classes
-keep
 public class net.mobtalker.mobtalker2.** implements net.mobtalker.mobtalker2.IMobTalkerProxy
{
    public *;
}

# FML/Forge event handlers
-keepclassmembers
 class net.mobtalker.mobtalker2.** 
{
    @net.minecraftforge.fml.common.eventhandler.SubscribeEvent *;
}

# Message Handler classes
-keep
 public class net.mobtalker.mobtalker2.** implements net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler
{
    public *;
}

# GUI XML stuff
-keep
 public class net.mobtalker.mobtalker2.client.gui.settings.**
{
    public *;
    @org.simpleframework.xml.** *;
}

# MTS native libraries
-keep
 class net.mobtalker.**
{
    @net.mobtalker.mobtalkerscript.v3.value.userdata.* *;
}

# SimpleXML
-keepclassmembers
 class org.simpleframework.xml.core.AttributeLabel,
       org.simpleframework.xml.core.ElementLabel,
       org.simpleframework.xml.core.ElementListLabel
{
    <init>( ... );
}

# If this isn't kept, we've done shrinking of ANTLR right
-whyareyoukeeping 
 class org.abego.treelayout.TreeLayout